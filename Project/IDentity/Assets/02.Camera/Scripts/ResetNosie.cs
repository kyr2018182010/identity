using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ResetNosie : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponentInChildren<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0f;
        GetComponentInChildren<CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 0f;
    }
}
