using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ShakingCamera : MonoBehaviour
{
    float timer = 0f;
    [SerializeField] float shakingTime;
    [SerializeField] float amplitudeGain;
    [SerializeField] float frequencyGain;

    public void Shake()
    {
        StartCoroutine(Shaking());
    }
    private IEnumerator Shaking()
    {
        var cam = GetComponent<CinemachineBrain>().ActiveVirtualCamera.VirtualCameraGameObject;
        if (cam == null) Debug.Log("Active Cam GameObject is NULL");
            
        var stateDriven = cam.GetComponent<CinemachineStateDrivenCamera>();
        if (stateDriven == null) Debug.Log("VirCam is NULL");
        if (stateDriven.ChildCameras[0] == null) Debug.Log("Child Cam is NULl");
        var perlins = stateDriven.gameObject.GetComponentsInChildren<CinemachineBasicMultiChannelPerlin>();
        foreach (var perlin in perlins)
        {

            perlin.m_AmplitudeGain = amplitudeGain;
            perlin.m_FrequencyGain = frequencyGain;
        }

        yield return new WaitForSeconds(shakingTime);

        foreach (var perlin in perlins)
        {

            perlin.m_AmplitudeGain = 0f;
            perlin.m_FrequencyGain = 0f;
        }
        StopCoroutine(Shaking());
    }
}
