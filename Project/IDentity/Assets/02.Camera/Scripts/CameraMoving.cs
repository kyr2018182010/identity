using Photon.Pun;
using UnityEngine;

public class CameraMoving : MonoBehaviourPunCallbacks
{
    [Header("======Camera Controller Value=======")]
    [SerializeField]private float mouseXSensitivity = 100f;
    static public float MouseXSensitivity = 100f;
    [SerializeField]private float mouseYSensiticity = 10f;
    [SerializeField]private float thirdMinY = 0.5f;
    [SerializeField]private float thirdMaxY = 4f;
    [SerializeField]private float firstMinY = 0.3f;
    [SerializeField] private float firstMaxY = 3f;
    
    [Header("======Camera Anim & Follow=========")]
    private Transform thirdCameraFollow;
    public Transform ThirdCameraFollow
    {
        set => thirdCameraFollow = value;
    }
    private Transform firstCameraLookAt;
    public Transform FirstCameraAim
    {
        set => firstCameraLookAt = value;
    }

    [Header("========Target=====================")]
    private PlayerAction player;
    public PlayerAction Player
    {
        set => player = value;
    }
    private SkillManager _skillManager;
    public SkillManager SkillManager
    {
        set => _skillManager = value;
    }

    private float _defualt3rdHeight;
    private float _defualt1stHeight;
    private float _thirdDeltaY = 0f;
    private float _firstDeltaY = 0f;

    private Vector3 defualt3rdPosition;
    private Vector3 defualt1stPosition;

    private Vector2 inputData;
    public Vector2 InputData
    {
        set => inputData = value;
    }

    private void Start()
    {
        if (_skillManager == null)
            Debug.LogError("Camera Moving : Skill Manager is NULL");
        if (player == null)
            Debug.LogError("Camera Moving : player is NULL");
        if (firstCameraLookAt == null)
            Debug.LogError("Camera Moving : 1st Camera Aim is NULL");
        if (thirdCameraFollow == null)
            Debug.LogError("Camera Moving : 3rd Camera Aim is NULL");

        defualt3rdPosition = thirdCameraFollow.localPosition;
        defualt1stPosition = firstCameraLookAt.localPosition;
        //_defualt1stHeight = firstCameraLookAt.position.y - player.transform.position.y;
        //_defualt3rdHeight = thirdCameraFollow.position.y - player.transform.position.y;

        MouseXSensitivity = mouseXSensitivity;
    }



    void FixedUpdate()
    {
        if (player.State == PlayerState.Talk)
            return;

        // GameInputManager에서 set된 값을 가져온다.
        float rotY;
        rotY = GameInputManager.inputData.rotData.y;

        // 카메라의 이동량을 계산해 이동시킨다.
        float deltaY = rotY * Time.deltaTime * mouseYSensiticity;
        if (_skillManager.isKeyHolding[player.ID])
        {
            // 1st
            // min / max ???
            float checkY = (firstCameraLookAt.position.y + deltaY) - player.transform.position.y;
            if (checkY < firstMinY)
            {
                rotY = 0f;
            }
            else if (checkY > firstMaxY)
            {
                rotY = 0f;
            }
            firstCameraLookAt.Translate(Vector3.up * rotY * Time.deltaTime * mouseYSensiticity);
        }
        else
        {
            // 3rd          
            // ?浹 ???
            if(Physics.BoxCast(thirdCameraFollow.position,new Vector3(0.2f, 0.3f, 0.2f), Vector3.down, Quaternion.identity,0f))
            {

                rotY = 0f;
            }

            // min / max ???
            float checkY = (thirdCameraFollow.position.y - deltaY) - player.transform.position.y;
            if (checkY < thirdMinY)
            {
                rotY = 0f;
            }
            else if (checkY  > thirdMaxY)
            {
                rotY = 0f;
            }


            // ???? ???? ???
            thirdCameraFollow.Translate(Vector3.up * (-rotY * Time.deltaTime * mouseYSensiticity));
        }
    }

    public void ChangeView(bool buttonHold) 
    {
        if (buttonHold)
        {
            // 1st view
            // initialize 3rd camera follow position
            thirdCameraFollow.SetPositionAndRotation(player.transform.position + defualt3rdPosition, player.transform.rotation);
            thirdCameraFollow.localPosition = defualt3rdPosition;

            _thirdDeltaY = 0f;
        }
        else
        {
            // 3rd view
            firstCameraLookAt.SetPositionAndRotation(player.transform.position + defualt1stPosition, player.transform.rotation);
            firstCameraLookAt.localPosition = defualt1stPosition;

            _firstDeltaY = 0f;
        }
    }

}
