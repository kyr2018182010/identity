using System;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering;

public class ReflectionPlan : MonoBehaviour
{
    public enum ResolutionMulltiplier
    {
        Full,
        Half,
        Third,
        Quarter
    }

    [Serializable]
    public class PlanarReflectionSettings
    {
        public float m_ClipPlaneOffset = 0f;
        public LayerMask m_ReflectLayers = -1;
    }

    public PlanarReflectionSettings m_settings = new PlanarReflectionSettings();

    // Cameras
    static private Camera _reflectionCamera;
    [SerializeField] private RenderTexture outputTexture;        // reflection Texture

    [SerializeField]private Transform reflectionPlan;        // target plane

    public static event Action<ScriptableRenderContext, Camera> BeginPlanarReflections;

    private void OnEnable()
    {
        RenderPipelineManager.beginCameraRendering += ExecutePlanarReflections;     // add reflection rendering
    }

    private void OnDisable()
    {
        RenderPipelineManager.beginCameraRendering -= ExecutePlanarReflections;     // delete reflection rendering
        //if (_reflectionCamera)
        //{
        //    //if (Application.isEditor)
        //    //    DestroyImmediate(_reflectionCamera);
        //    //else
        //        //Destroy(_reflectionCamera);
        //}
    }

    private void OnDestroy()
    {
        //RenderPipelineManager.beginCameraRendering -= ExecutePlanarReflections;
        if (_reflectionCamera)
        {
            //if (Application.isEditor)
                DestroyImmediate(_reflectionCamera.gameObject);
            //else
                //Destroy(_reflectionCamera.gameObject);
        }
    }


    private void ExecutePlanarReflections(ScriptableRenderContext context, Camera mainCamera)
    {
        // only reflect main camera
        if (mainCamera.cameraType == CameraType.Reflection || mainCamera.cameraType == CameraType.Preview)
            return;

        Reflection(mainCamera);        // adjust reflection camera
        RenderReflection();             // assign render texture

        if (!_reflectionCamera)
            return;

        var data = new ReflectionSettingData();
        data.Set();      // set render quality settings

        BeginPlanarReflections?.Invoke(context, _reflectionCamera); // callback Action for PlanarReflection
        UniversalRenderPipeline.RenderSingleCamera(context, _reflectionCamera); // render planar reflections

        data.Restore();  // restore render quality settings
    }

    private void Reflection(Camera mainCamera)
    {
        // create reflection camera
        if (_reflectionCamera == null)
            _reflectionCamera = CreateReflectionCamera();


        _reflectionCamera.CopyFrom(mainCamera);     // sync reflection camera with main camera
        _reflectionCamera.nearClipPlane = 80f;      // near range will be refracted not reflected

        // calculate reflection's new transform
        var pos = Vector3.zero;
        var normal = Vector3.up;

        if (reflectionPlan != null)
        {
            pos = reflectionPlan.position;
            normal = reflectionPlan.up;
        }

        var d = -Vector3.Dot(normal, pos) - m_settings.m_ClipPlaneOffset;
        var newReflectionPlan = new Vector4(normal.x, normal.y, normal.z, d);
        var reflectionMatrix = Matrix4x4.identity;
        reflectionMatrix *= Matrix4x4.Scale(new Vector3(1, -1, 1));

        CalculateReflectionMatrix(ref reflectionMatrix, newReflectionPlan);
        var oldPosition = mainCamera.transform.position - new Vector3(0, pos.y * 2, 0);
        var newPosition = new Vector3(oldPosition.x, -oldPosition.y, pos.z);
        _reflectionCamera.transform.forward = Vector3.Scale(mainCamera.transform.forward, new Vector3(1, -1, 1));
        _reflectionCamera.worldToCameraMatrix = mainCamera.worldToCameraMatrix * reflectionMatrix;

        var clipPlane = CameraSpacePlane(_reflectionCamera, pos - Vector3.up * 0.1f, normal, 1.0f);
        var projection = mainCamera.CalculateObliqueMatrix(clipPlane);
        _reflectionCamera.projectionMatrix = projection;
        _reflectionCamera.transform.position = newPosition;
        _reflectionCamera.cullingMask = m_settings.m_ReflectLayers;
    }

    private Camera CreateReflectionCamera()
    {
        var go = new GameObject("Reflection Camera", typeof(Camera));
        var cameraData = go.AddComponent(typeof(UniversalAdditionalCameraData)) as UniversalAdditionalCameraData;

        cameraData.requiresColorOption = CameraOverrideOption.Off;
        cameraData.requiresDepthOption = CameraOverrideOption.Off;
        cameraData.SetRenderer(0);

        var t = transform;
        var reflectionCamera = go.GetComponent<Camera>();
        reflectionCamera.transform.SetPositionAndRotation(t.position, t.rotation);
        reflectionCamera.depth = -10;
        reflectionCamera.enabled = false;
        go.hideFlags = HideFlags.HideAndDontSave;

        return reflectionCamera;
    }

    private void CalculateReflectionMatrix(ref Matrix4x4 reflectionMat, Vector4 plane)
    {
        reflectionMat.m00 = (1F - 2F * plane[0] * plane[0]);
        reflectionMat.m01 = (-2F * plane[0] * plane[1]);
        reflectionMat.m02 = (-2F * plane[0] * plane[2]);
        reflectionMat.m03 = (-2F * plane[3] * plane[0]);

        reflectionMat.m10 = (-2F * plane[1] * plane[0]);
        reflectionMat.m11 = (1F - 2F * plane[1] * plane[1]);
        reflectionMat.m12 = (-2F * plane[1] * plane[2]);
        reflectionMat.m13 = (-2F * plane[3] * plane[1]);

        reflectionMat.m20 = (-2F * plane[2] * plane[0]);
        reflectionMat.m21 = (-2F * plane[2] * plane[1]);
        reflectionMat.m22 = (1F - 2F * plane[2] * plane[2]);
        reflectionMat.m23 = (-2F * plane[3] * plane[2]);

        reflectionMat.m30 = 0F;
        reflectionMat.m31 = 0F;
        reflectionMat.m32 = 0F;
        reflectionMat.m33 = 1F;
    }

    private void RenderReflection()
    {
        _reflectionCamera.targetTexture = outputTexture;
    }


    private Vector4 CameraSpacePlane(Camera cam, Vector3 pos, Vector3 normal, float sideSign)
    {
        var offsetPos = pos + normal * m_settings.m_ClipPlaneOffset;
        var m = cam.worldToCameraMatrix;
        var cameraPosition = m.MultiplyPoint(offsetPos);
        var cameraNormal = m.MultiplyVector(normal).normalized * sideSign;
        return new Vector4(cameraNormal.x, cameraNormal.y, cameraNormal.z, -Vector3.Dot(cameraPosition, cameraNormal));
    }



    class ReflectionSettingData
    {
        // Original Quality Settings
        private bool _fog;
        private int _maxLod;
        private float _lodBias;

        public ReflectionSettingData()
        {
            // set original quality settings
            _fog = RenderSettings.fog;
            _maxLod = QualitySettings.maximumLODLevel;
            _lodBias = QualitySettings.lodBias;
        }
        public void Set()
        {
            GL.invertCulling = true;
            RenderSettings.fog = false; // disable fog for now as it's incorrect with projection
            QualitySettings.maximumLODLevel = 1;
            QualitySettings.lodBias = 0.5f;
        }

        public void Restore()
        {
            GL.invertCulling = false;
            RenderSettings.fog = _fog;
            QualitySettings.maximumLODLevel = _maxLod;
            QualitySettings.lodBias = _lodBias;
        }
    }


}
