using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Floater : MonoBehaviour
{
    [SerializeField]
    private Transform waterRenderer;

    private MeshFilter meshFilter;

    private MeshRenderer meshRenderer;

    // key = floatingObj's index in childeren, value = vertex's index in mesh that is same position x and z with floating object
    private Dictionary<int,int> vertexIndexs = new Dictionary<int, int>();

    List<Transform> flotingObjs;

    private void Awake()
    {
        meshFilter = waterRenderer.GetComponent<MeshFilter>();
        Vector3[] vertices = meshFilter.mesh.vertices;
        flotingObjs = GetComponentsInChildren<Transform>().ToList();
        flotingObjs.RemoveAt(0);

        // 인덱스 check시 사용
        List<Transform> flotingObjsTmp = GetComponentsInChildren<Transform>().ToList();
        // remove myself
        flotingObjsTmp.RemoveAt(0);


        for (int i = 0; i < vertices.Length; ++i)
        {
            for(int j = 0; j < flotingObjsTmp.Count; ++j)
            {
                Debug.Log("버텍스 y : " + vertices[i].y);

                if (Mathf.Abs((vertices[i].x + waterRenderer.position.x) - flotingObjsTmp[j].position.x) < 1.5f 
                    && Mathf.Abs((vertices[i].z + waterRenderer.position.z) - flotingObjsTmp[j].position.z) <1.5f)
                {
                    Debug.Log(flotingObjsTmp[j].name + " " + i);
                    Debug.Log("버텍스x : " + vertices[i].x + ", 플로팅 오브젝트x" + flotingObjsTmp[j].position.x);
                    Debug.Log("버텍스x : " + vertices[i].z + ", 플로팅 오브젝트x" + flotingObjsTmp[j].position.z);
                    vertexIndexs.Add(j, i);
                    flotingObjsTmp.RemoveAt(j);
                    break;
                }
            }
            if (flotingObjsTmp.Count == 0)
                break;
        }
    }

    private void FixedUpdate()
    {
        Vector3[] vertices = meshFilter.mesh.vertices;

        for (int i = 0; i < flotingObjs.Count; ++i) 
        {
            Debug.Log(vertices[vertexIndexs[i]] + waterRenderer.position);
            flotingObjs[i].position = vertices[vertexIndexs[i]] + waterRenderer.position;
        }
    }
}
