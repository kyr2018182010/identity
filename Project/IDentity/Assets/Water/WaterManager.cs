using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ProBuilder;

[Serializable]
public class WaterData
{
    public int tag;
    public Transform tr;
}

public class WaterManager : MonoBehaviour
{
    // 지금 로드되어있는 water
    [SerializeField] List<WaterData> waterDatas;
    [SerializeField] GameObject water;
    private Camera cam;
    private List<WaterData> cnaSeeWater = new List<WaterData>();
    private WaterData nowWater;

    private Coroutine canSeeCoroutine = null;  //코루틴 변수. 1개의 루틴만 돌리기 위해 저장한다.
    private Coroutine outEyesightCoroutine = null;  //코루틴 변수. 1개의 루틴만 돌리기 위해 저장한다.

    // caustics shader
    [SerializeField] private Material causticsMat;

    private void Start()
    {
        cam = Camera.main;
        water.SetActive(false);
        StartCoroutine("WaterUpdate");
    }

    private void FixedUpdate()
    {
        if (water.activeSelf)
        {
            //water.transform.eulerAngles = new Vector3(0, cam.transform.eulerAngles.y, 0);
            water.transform.position =
                new Vector3(cam.transform.position.x,
                nowWater.tr.position.y,
                cam.transform.position.z);

            if (nowWater.tr.GetComponent<Renderer>().bounds.extents.x + 50 < Mathf.Abs(nowWater.tr.position.x - cam.transform.position.x) ||
                    nowWater.tr.GetComponent<Renderer>().bounds.extents.z + 50 < Mathf.Abs(nowWater.tr.position.z - cam.transform.position.z))
            {
                water.SetActive(false);
            }
        }
    }

    IEnumerator WaterUpdate()
    {
        while (true)
        {
            for (int i = 0; i < waterDatas.Count; ++i)
            {
                if (waterDatas[i].tr.GetComponent<Renderer>().bounds.extents.x + 700 >= Mathf.Abs(waterDatas[i].tr.position.x - cam.transform.position.x) &&
                    waterDatas[i].tr.GetComponent<Renderer>().bounds.extents.z + 700 >= Mathf.Abs(waterDatas[i].tr.position.z - cam.transform.position.z))
                {
                    waterDatas[i].tr.gameObject.SetActive(true);
                    cnaSeeWater.Add(waterDatas[i]);
                    waterDatas.Remove(waterDatas[i]);
                    if (canSeeCoroutine == null)
                        canSeeCoroutine = StartCoroutine("CheckPlayerInWater");
                    if (outEyesightCoroutine == null)
                        outEyesightCoroutine = StartCoroutine("CheckOutEyesightCoroutine");
                }
            }
            yield return new WaitForSeconds(0.5f);
        }
    }

    IEnumerator CheckPlayerInWater()
    {
        while (true)
        {
            for (int i = 0; i < cnaSeeWater.Count; ++i)
            {
                if (cnaSeeWater[i].tr.GetComponent<Renderer>().bounds.extents.x +50 >= Mathf.Abs(cnaSeeWater[i].tr.position.x - cam.transform.position.x) &&
                    cnaSeeWater[i].tr.GetComponent<Renderer>().bounds.extents.z +50 >= Mathf.Abs(cnaSeeWater[i].tr.position.z - cam.transform.position.z))
                {
                    water.SetActive(true);
                    nowWater = cnaSeeWater[i];
                }
            }

            if (cnaSeeWater.Count == 0)
            {
                canSeeCoroutine = null;
                break;
            }
            yield return new WaitForSeconds(0.5f);
        }
    }

    IEnumerator CheckOutEyesightCoroutine()
    {
        while (true)
        {

            for (int i = 0; i < cnaSeeWater.Count; ++i)
            {
                if (cnaSeeWater[i].tr.GetComponent<Renderer>().bounds.extents.x + 700 < Mathf.Abs(cnaSeeWater[i].tr.position.x - cam.transform.position.x) ||
                    cnaSeeWater[i].tr.GetComponent<Renderer>().bounds.extents.z + 700 < Mathf.Abs(cnaSeeWater[i].tr.position.z - cam.transform.position.z))
                {
                    cnaSeeWater[i].tr.gameObject.SetActive(false);
                    waterDatas.Add(cnaSeeWater[i]);
                    cnaSeeWater.Remove(cnaSeeWater[i]);
                }
            }

            if(cnaSeeWater.Count == 0)
            {
                outEyesightCoroutine = null;
                break;
            }

            yield return new WaitForSeconds(0.5f);
        }
    }
}
