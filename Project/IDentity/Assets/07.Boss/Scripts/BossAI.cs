using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class BossAI : MonoBehaviour
{
    [SerializeField] private RuntimeAnimatorController controller;
    [SerializeField] private Avatar avatar;
    [SerializeField] private float idleTimerValue = 3f;
    [SerializeField] private float attackTimerValue = 15f;
    [SerializeField] private float attackTime = 4.3f;
    [SerializeField] private List<Transform> walkTrackPoints;
    [SerializeField] private float damping = 5f;
    [SerializeField] private GameObject deadAnimation;
    [SerializeField] private GameObject bloodEffect;

    // animatator hash
    readonly private int _stateHash = Animator.StringToHash("Speed");
    readonly private int _attackHash = Animator.StringToHash("Attack");
    readonly private int _dieHash = Animator.StringToHash("Die");

    private Animator animator;
    private NavMeshAgent agent;
    private int currTrackPointIdx = 0;
    private float attackTimer;
    [HideInInspector] public int attackCount;

    private Coroutine moveConroutine;
    private Coroutine attackCoroutine;

    private void Awake()
    {
        // set animator
        animator = GetComponent<Animator>();
        if (!animator)
        {
            animator = gameObject.AddComponent<Animator>();
            animator.avatar = avatar;
        }
        if (!controller)
            Debug.LogError("Boss AI : Animator isn't set");
        else        
            animator.runtimeAnimatorController = controller;

        // set Navmesh Agent
        agent = GetComponent<NavMeshAgent>();
        if (!agent)
            Debug.LogError("Boss AI : agent isn't set");
        else
            agent.destination = walkTrackPoints[currTrackPointIdx].position;

    }

    private void OnEnable()
    {
        animator.SetFloat(_stateHash, 0);
        agent.isStopped = true;
        attackTimer = attackTimerValue;
    }

    private void OnDisable()
    {
    }

    public bool IsArrived(Vector3 destination)
    {
        var vec1 = destination;
        var vec2 = transform.position;
        vec1.y = 0f;
        vec2.y = 0f;
 
        if(Vector3.Distance(vec1, vec2)<0.3f)
        {
            return true;
        }
        return false;
    }

    IEnumerator MoveAndAnimationController()
    {
        yield return new WaitForSeconds(3f);
        while(isActiveAndEnabled)
        {
            if (IsArrived(walkTrackPoints[currTrackPointIdx].position))            // idle state
            {
                agent.isStopped = true;
                animator.SetFloat(_stateHash, 0f);
                currTrackPointIdx = (currTrackPointIdx + 1) % walkTrackPoints.Count;
                yield return new WaitForSeconds(idleTimerValue);
            }
            else if (attackTimer <= 0f)          // attack state
            {
                agent.isStopped = true;
                attackTimer = attackTimerValue;
                attackCoroutine = StartCoroutine(AttackDelay(1f));
                Roar(false,1f);
                bloodEffect.GetComponent<Animator>().Play("Blood");
                yield return new WaitForSeconds(attackTime);
            }

            // moving state
            if (agent.isStopped)            
            {
                agent.isStopped = false;
                agent.destination = walkTrackPoints[currTrackPointIdx].position;
                animator.SetFloat(_stateHash, 1f);
            }
            // turn
            Quaternion rot = Quaternion.LookRotation(agent.desiredVelocity);
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * damping);
            attackTimer -= Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
    }

    public void QuestStart()
    {
        animator.SetFloat(_stateHash, 1);
        if(moveConroutine == null)
            moveConroutine = StartCoroutine(MoveAndAnimationController());
    }

    public void Dead()
    {
        agent.isStopped = true;
        StopCoroutine(attackCoroutine);
        StopCoroutine(moveConroutine);
        moveConroutine = null;
        attackCoroutine = null;
        GetComponentInChildren<Renderer>().enabled = false;
        var sound = GetComponent<EffectSoundPlayer>();
        sound.PlayEffectSound(BGMEnum.BossDead, true, 1f);
        deadAnimation.SetActive(true);
        deadAnimation.GetComponent<Animation>().Play();
    }

    public void Retry()
    {
        attackCount = 0;
        attackTimer = attackTime;
        animator.SetFloat(_stateHash, 0);
        agent.isStopped = true;
        attackTimer = attackTimerValue;
        GetComponentInChildren<Renderer>().enabled = true;
        deadAnimation.SetActive(false);
    }

    public void Roar(bool bgmMute,float delay)
    {
        animator.SetTrigger(_attackHash);
        var sound = GetComponent<EffectSoundPlayer>();
        sound.PlayEffectSound(BGMEnum.BossRoar,bgmMute,delay);
    }

    IEnumerator AttackDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        ++attackCount;
        Camera.main.GetComponent<ShakingCamera>().Shake();
    }
}
