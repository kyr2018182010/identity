using UnityEngine;
using UnityEditor;

[CustomEditor(typeof (SeamlessSaver))]
public class SeamlessSaverEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        SeamlessSaver objectSeamless = (SeamlessSaver)target;
        if(GUILayout.Button("Bake Seamless Objects"))
        {
            objectSeamless.Bake();
        }

        if (GUILayout.Button("Active Static Objects"))
        {
            objectSeamless.onOffStaticObjects(true);
        }

        if (GUILayout.Button("Inactive Static Objects"))
        {
            objectSeamless.onOffStaticObjects(false);
        }
        if (objectSeamless.showDestroyButton)
        {
            if (GUILayout.Button("Destroy All Objects"))
            {
                objectSeamless.DestroyAllStaticObjects();
            }
            if (GUILayout.Button("Restore All Objects"))
            {
                objectSeamless.RestoreAllStaticObjects();
            }
        }
    }
}
