using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SeamlessTargetSetter))]
public class SeamlessTargetSetterEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        SeamlessTargetSetter setter = (SeamlessTargetSetter)target;
        if (GUILayout.Button("Set seamless target at prefabs"))
        {
            setter.SetSeamlessTarget();
        }
        if (GUILayout.Button("Set ids"))
        {
            setter.SetSemalessTargetId();
        }
    }
}
