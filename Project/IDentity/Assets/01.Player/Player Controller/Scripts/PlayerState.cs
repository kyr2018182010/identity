public enum PlayerState
{
    Idle,
    Run,
    JumpStart,
    Jumping,
    Landing,
    Talk,
    Fall,
    Dead,
}
