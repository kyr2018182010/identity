﻿using UnityEngine;
using Photon.Pun;
using System.Collections;

public class PlayerAction : MonoBehaviourPunCallbacks
{
    private CharacterController _characterController;
    private Animator _animator;
    private Rigidbody rigidbody;
    [HideInInspector] public GravityLevel GravityLevel = GravityLevel.Light;

    // for test
    [SerializeField] private PlayerState _state = PlayerState.Idle;
    public PlayerState State
    {
        get => _state;
        set => _state = value;
    }

    // animation
    private readonly int _hashJumpStart = Animator.StringToHash("JumpStart");
    private readonly int _hashJumping = Animator.StringToHash("Jumping");
    private readonly int _hashRunJump = Animator.StringToHash("Run Jump");
    private readonly int _hashEndRunJump = Animator.StringToHash("End Run Jump");
    private readonly int _hashLand = Animator.StringToHash("Land");
    private readonly int _hashFall = Animator.StringToHash("Fall");
    private readonly int _hashInput = Animator.StringToHash("Input");
    private readonly int _hashHor = Animator.StringToHash("InputHor");
    private readonly int _hashVer = Animator.StringToHash("InputVer");
    private readonly int _hashPush = Animator.StringToHash("Push");
    private readonly int _hashSkill = Animator.StringToHash("Skill");
    private readonly int _hashTalk = Animator.StringToHash("Talk");

    // Move
    [Header("Move")]
    [SerializeField] private float runSpeed = 5f;
    [SerializeField] private float walkSpeed = 3f;
    [SerializeField] private float sideWalkSpeed = 2f;
    private bool onTile;
    private Vector3 moveVelocity = Vector3.zero;

    //Jump
    [Header("Jump")]
    [SerializeField] private float jumpPower = 300f;
    [SerializeField] private float jumpDuration = 0.28f;
    [SerializeField] private float landMinDist = 1f;
    private float _jumpDuration;
    [SerializeField] private float moveSpeedDiminution = 0.1f;
    private float _moveSpeedDiminution = 1f;

    // Fall
    [Header("Fall")]
    [SerializeField] private float mass = 10f;
    [SerializeField] private float minGroundedDist = 0.02f;
    [SerializeField] private LayerMask notGroundedLayer;
    [SerializeField] private float fallDistance = 5f;
    private int layerMask;
    private Vector3 _fallVelocity = Vector3.zero;
    public Vector3 onTileVelocity = Vector3.zero;
    private float _fallingDistance;
    [SerializeField] private float deadFallingDistance;
    private float groundDist = 10f;

    [SerializeField] private Collider[] footColliders;

    [HideInInspector] public Vector3 externalVelocity = Vector3.zero;

    // Push
    private bool isCloseObject;
    private GravityObject pushObject;

    private bool isPushing = false;
    public bool IsPushing
    {
        get => isPushing;
    }

    public bool pushButtonDown = false;

    // multiPlay    
    [SerializeField]
    private int id = (int)PlayerSkillType.Gravity;
    public int ID
    {
        get => id;
        set => id = value;
    }


    // test
    private Vector3 hitPoint = Vector3.zero;
    private Coroutine ontTileCoroutine;
    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();
        _jumpDuration = jumpDuration;
        layerMask = 1 << LayerMask.NameToLayer("Not Grounded") | 1 << LayerMask.NameToLayer("Player");
        layerMask = ~layerMask;

        // find foot colliders
        var colliders = GetComponentsInChildren<Collider>();
        foreach (var col in colliders)
        {
            if (col.name == "Bip001 L Foot")
                footColliders[0] = col;
            else if (col.name == "Bip001 R Foot")
                footColliders[1] = col;
        }

        // add rigidbody
        if (!rigidbody)
        {
            rigidbody = gameObject.AddComponent<Rigidbody>();
        }
        rigidbody.useGravity = false;
        rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        rigidbody.mass = 0.5f;
    }

    private void Update()
    {

        if (!photonView.IsMine)
            return;

        if (_state == PlayerState.Dead)
            return;

        if (onTile)
        {
            footColliders[0].isTrigger = true;
            footColliders[1].isTrigger = true;
        }

        Push();
    }

    private void FixedUpdate()
    {

        if (!photonView.IsMine)
            return;
        if (_state == PlayerState.Talk)
            return;

        moveVelocity = Vector3.zero;
        Fall();

        if (_state == PlayerState.Dead)
            return;

        Jumping();
        Move();
        CheckisGrounded();

        var vec = (moveVelocity + _fallVelocity + externalVelocity) * Time.deltaTime;
        _characterController.Move(vec);
    }

    private void Move()
    {
        // �÷��̾ x�� �������� ȸ����Ų��.

        if (_state != PlayerState.Jumping)
            transform.Rotate(Vector3.up * (GameInputManager.inputData.rotData.x * Time.deltaTime * CameraMoving.MouseXSensitivity));

        Vector2 runData = GameInputManager.inputData.runData;
        Vector2 speed = Vector2.zero;

        if (_state != PlayerState.Jumping && _state != PlayerState.JumpStart)
        {
            if (runData == Vector2.zero)
            {
                if (_state == PlayerState.Run)
                    _state = PlayerState.Idle;
            }
            else
            {
                if (_state == PlayerState.Fall)
                {
                    speed = new Vector2(runSpeed, runSpeed);
                }
                else
                {
                    if (runData.y > 0)
                    {
                        // forward run
                        speed = new Vector2(runSpeed, runSpeed);

                    }
                    else if (runData.y < 0)
                    {
                        // back walk
                        speed = new Vector2(walkSpeed, walkSpeed);

                    }
                    else if (runData.y == 0)
                    {
                        // side walk
                        speed.x = sideWalkSpeed;
                        speed.y = 0f;
                    }
                    _state = PlayerState.Run;
                }
            }

            // set animation
            if (_state != PlayerState.Fall)
            {
                _animator.SetFloat(_hashInput, runData.magnitude);
                _animator.SetFloat(_hashHor, runData.x);
                _animator.SetFloat(_hashVer, runData.y);
            }
            else
            {
                _animator.SetFloat(_hashInput, 0f);
            }
        }
        else
            speed.y = runSpeed;

        runData = runData.normalized;
        moveVelocity = (transform.right * runData.x * speed.x + transform.forward * runData.y * speed.y) * _moveSpeedDiminution;
    }

    void CheckisGrounded()
    {
        //Debug.DrawRay(transform.position + Vector3.up * 0.3f, -transform.up * fallDistance, Color.red);
        if (Physics.Raycast(transform.position + Vector3.up * 0.3f, -transform.up, out var hit, fallDistance, layerMask))
        {
            //hitPoint = hit.point;
            _fallingDistance = 0f;
            groundDist = transform.position.y - hit.point.y;

            // Land Check

            // Land Finished Check
            if (_state != PlayerState.JumpStart && _fallVelocity.y <= 0)
            {
                if (groundDist <= 0.03f)
                {
                    moveVelocity += groundDist * Vector3.down;
                    _fallVelocity = Vector3.zero;
                }

                if (onTile && _state != PlayerState.Jumping && externalVelocity.y == 0 && groundDist > 0.03f)
                {
                    moveVelocity += groundDist * Vector3.down;
                    _fallVelocity = Vector3.zero;
                }

                if (_state == PlayerState.Jumping || _state == PlayerState.Fall)
                {
                    if (groundDist < landMinDist)
                    {
                        if (GameInputManager.inputData.runData == Vector2.zero)
                            _animator.SetTrigger(_hashLand);
                        else
                            _animator.SetTrigger(_hashEndRunJump);
                        _animator.SetBool(_hashFall, false);
                        _state = PlayerState.Landing;
                    }
                }

                if (_state == PlayerState.Landing && groundDist < minGroundedDist)
                {
                    _state = PlayerState.Idle;
                    _moveSpeedDiminution = 1f;
                }
            }
        }
        else
        {
            // Fall Check
            if (_state != PlayerState.Fall && _state != PlayerState.Jumping && _state != PlayerState.JumpStart)
            {
                _animator.SetBool(_hashFall, true);
                _state = PlayerState.Fall;
                _fallingDistance = 0f;
                _fallVelocity = Vector3.zero;
            }
        }
    }

    private void Fall()
    {
        if (!onTile || _state == PlayerState.Jumping || externalVelocity.y <= 0)
        {
            float accY = -9.8f;
            _fallVelocity.y += accY * Time.deltaTime;
        }

        // check State            
        _fallingDistance -= _fallVelocity.y * Time.deltaTime;

        // Dead Check
        if (_fallingDistance >= deadFallingDistance && _state != PlayerState.Dead)
            _state = PlayerState.Dead;


        if (Physics.Raycast(transform.position, transform.up, out var hit, _fallingDistance, layerMask))
        {
            // adjust player feet position
            groundDist = transform.position.y - hit.point.y;
            hitPoint = hit.point;
            if (groundDist <= 0)
            {
                _characterController.Move(groundDist * Vector3.down);
                _fallVelocity = Vector3.zero;
                _fallingDistance = 0f;
            }
        }
    }

    public void JumpStart()
    {
        if (_state != PlayerState.Jumping && _state != PlayerState.JumpStart && _state != PlayerState.Landing)
        {
            if (_state == PlayerState.Run)
            {
                _jumpDuration = 0f;
            }
            else
                _animator.SetTrigger(_hashJumpStart);
            _state = PlayerState.JumpStart;
            _moveSpeedDiminution = moveSpeedDiminution;
        }
    }

    private void Jumping()
    {
        if (_state != PlayerState.JumpStart) return;
        if (_jumpDuration <= 0)
        {
            // Start Jump
            _animator.SetTrigger(_hashJumping);
            _fallVelocity = Vector3.zero;
            float acc = (jumpPower / mass) * 10;
            _fallVelocity.y += acc * Time.deltaTime;
            _state = PlayerState.Jumping;
            _jumpDuration = jumpDuration;
        }
        else
        {
            _jumpDuration -= Time.deltaTime;
        }
    }

    private void Push()
    {
        if (_state == PlayerState.Jumping)
            return;

        isCloseObject = false;
        if (Physics.Raycast(transform.position + Vector3.up * 1f, transform.forward, out var hit, 5f))
        {
            var obj = hit.transform.GetComponent<GravityObject>();
            var trauma = hit.transform.GetComponent<Trauma>();
            
            if (obj != null)
            {
                isCloseObject = true;
                pushObject = obj;
            }

            if (trauma)
            {
                isCloseObject = true;
            }
        }

        if (GameInputManager.inputData.pushHold && isCloseObject)
        {
            isPushing = true;
            footColliders[0].isTrigger = true;
            footColliders[1].isTrigger = true;

        }
        else if (!GameInputManager.inputData.pushHold || !isCloseObject)
        {
            isPushing = false;
            footColliders[0].isTrigger = false;
            footColliders[1].isTrigger = false;
        }

        _animator.SetBool(_hashPush, isPushing);

        if (pushObject != null)
        {
            pushObject.IsPushed = isPushing;

            if (!isPushing)
                pushObject = null;
        }
    }

    public void SetSkillAnimation(bool skill)
    {
        _animator.SetBool(_hashSkill, skill);
    }

    public void SetTalkState(bool talk, Transform npc)
    {
        if (talk)
        {
            _state = PlayerState.Talk;
            var pos = npc.position;
            pos.y = transform.position.y;
            transform.LookAt(pos, transform.up);
            _animator.SetFloat(_hashInput, 0);      // change animation to idle
            if (npc.TryGetComponent<ItemTarget>(out var target))
            {
                GetComponent<InventoryOnwer>().giveTarget = target;
            }

            //CursorController.LockGameCursor(false);
        }
        else
        {
            _state = PlayerState.Idle;
            if (GetComponent<InventoryOnwer>().giveTarget)
            {
                GetComponent<InventoryOnwer>().giveTarget = null;
            }
            //CursorController.LockGameCursor(true);
        }

        _animator.SetFloat(_hashInput, 0f);
        _animator.SetBool(_hashTalk, talk);
    }

    public void SetNewTransform(Vector3 pos)
    {
        _characterController.enabled = false;
        transform.position = pos;
        _characterController.enabled = true;
        OnTile(false);
        StopCoroutine(OnTileUpdate());
        externalVelocity = Vector3.zero;
    }

    public void OnTile(bool isOnTile)
    {
        footColliders[0].isTrigger = isOnTile;
        footColliders[1].isTrigger = isOnTile;
        var originOnTile = onTile;          // prevent to start same coroutine twice
        onTile = isOnTile;
        if (isOnTile && !originOnTile)
        {
            externalVelocity = Vector3.zero;
            rigidbody.mass = 0.1f;
            StartCoroutine(OnTileUpdate());
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(hitPoint, 0.01f);
    }

    IEnumerator OnTileUpdate()
    {
        while (onTile)
        {
            if (!onTile)
            {
                StopCoroutine(OnTileUpdate());
                externalVelocity = Vector3.zero;
                rigidbody.mass = 1f;
                break;
            }
            if (onTile && externalVelocity.y > 0)
            {
                rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX
                    | RigidbodyConstraints.FreezePositionZ;
            }
            else
                rigidbody.constraints = RigidbodyConstraints.FreezeAll;

            yield return new WaitForEndOfFrame();
        }

        StopCoroutine(OnTileUpdate());
        externalVelocity = Vector3.zero;
    }

}
