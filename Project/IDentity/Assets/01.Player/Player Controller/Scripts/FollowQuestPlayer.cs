using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using Protocol;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowQuestPlayer : MonoBehaviourPunCallbacks
{
    public FollowQuestNPCAI questNPC;
    private int viewId;

    public override void OnEnable()
    {
        // 네트워크 이벤트 추가
        PhotonNetwork.NetworkingClient.EventReceived += EventReceived;
        viewId = GetComponent<PhotonView>().ViewID;
    }
    public override void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= EventReceived;
    }

    public void CallOrStopNPC()
    {
        if (!FollowQuestManager.doingQuest) return; 

        if (questNPC.Moving)
        {
            object[] content = new object[] { viewId };
            SendRaiseEvent(EVENTCODE.NPC_STOP, content, SEND_OPTION.OTHER);
            questNPC.Stop(viewId);
        }
        else
        {
            object[] content = new object[] { viewId };
            SendRaiseEvent(EVENTCODE.NPC_CALL, content, SEND_OPTION.OTHER);
            questNPC.Call(transform, viewId);
        }
    }



    public void SendRaiseEvent(EVENTCODE eventcode, object[] content, SEND_OPTION sendoption)
    {
        // debug
        string DebugStr = string.Empty;
        DebugStr = "[SEND__" + eventcode.ToString() + "]";
        for (int i = 0; i < content.Length; ++i)
        {
            DebugStr += "_" + content[i];
        }
        Debug.Log(DebugStr);

        RaiseEventOptions raiseEventOption = new RaiseEventOptions
        {
            Receivers = (ReceiverGroup)sendoption,
        };
        PhotonNetwork.RaiseEvent((byte)eventcode, content, raiseEventOption, SendOptions.SendReliable);
    }

    private void EventReceived(EventData photonEvent)
    {
        int code = photonEvent.Code;

        if (code == (int)EVENTCODE.NPC_CALL)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("RECV__EVENTCODE.INSTANCIATE_ENEMY__" + datas[0] );

            var players = PhotonView.FindObjectsOfType<PlayerAction>();
            for (int i = 0; i < players.Length; ++i)
            {
                if (!players[i].GetComponent<PhotonView>().IsMine)
                {
                    questNPC.Call(players[i].transform, (int)datas[0]);
                    break;
                }
            }
        }
        else if (code == (int)EVENTCODE.NPC_STOP)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("RECV__EVENTCODE.INSTANCIATE_ENEMY__" + datas[0] );

            questNPC.Stop((int)datas[0]);
        }
    }
}
