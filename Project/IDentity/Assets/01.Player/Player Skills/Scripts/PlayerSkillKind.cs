using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerSkillType
{
    Gravity = 0,
    CreateObject = 1,
    None = 2
}
