public enum GravityLevel
{
    Heavy = -5,
    Common = 0,
    Light = 5
}
