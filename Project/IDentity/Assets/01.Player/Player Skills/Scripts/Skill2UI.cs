using Photon.Pun.UtilityScripts;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;

public class Skill2UI : MonoBehaviour
{
    public bool creatable = true;
    private Collider collider;
    public Material material;
    public Color deualtColor;
    private bool gravityHeavy = false;
    private bool staticObject = false;
    private bool hitOnArea = false;

    private void Start()
    {
        collider = GetComponent<Collider>();
        collider.isTrigger = true;
        gameObject.layer = 2;
    }

    private void OnEnable()
    {
        creatable = true;
        gravityHeavy = false;
        staticObject = false;
        material.SetColor("_EmissionColor", deualtColor);
        GetComponentInChildren<Renderer>().material = material;
    }

    private void Update()
    {
        // if collide with gravity Heavy Object or staticObject skillObject can't be created
        if (gravityHeavy || staticObject)
        {
            creatable = false;
            GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", Color.red);
        }
        else
        {
            creatable = true;
            GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", deualtColor);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponentInParent<PlayerAction>();
        var gravityObj = other.GetComponent<GravityObject>();
        var npc = other.GetComponent<NPCAI>();

        if (gravityObj != null && gravityObj.Level <= (int)GravityLevel.Heavy)
        {
            // if gravity Heavy
            gravityHeavy = true;
            Debug.Log("Collide with Gravity Object");
        }
        else
        {
            // if static Object
            if (player == null && npc == null && gravityObj == null && !other.CompareTag("Creation Area"))
            {
                staticObject = true;
                Debug.Log("Collide with Static Object");
            }
        }

        //if (Physics.Raycast(transform.position - Vector3.up * 0.1f, Vector3.down, out var hit, 0.3f))
        //{
        //    hitOnArea = true;
        //}
        //else
        //{
        //    hitOnArea = false;
        //}
    }

    private void OnTriggerExit(Collider other)
    {
        var player = other.GetComponentInParent<PlayerAction>();
        var gravityObj = other.GetComponent<GravityObject>();
        var npc = other.GetComponent<NPCAI>();

        if (gravityObj != null && gravityObj.Level <= (int)GravityLevel.Light)
        {
            // if gravity Heavy
            gravityHeavy = false;
        }
        else
        {
            // if static Object
            if (player == null && npc == null && gravityObj == null && !other.CompareTag("Creation Area"))
            {
                staticObject = false;
            }
        }

        //if (Physics.Raycast(transform.position - Vector3.up * 0.1f, Vector3.down, out var hit, 0.3f))
        //{
        //    hitOnArea = true;
        //}
        //else
        //{
        //    hitOnArea = false;
        //}
    }
}
