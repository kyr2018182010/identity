using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill2Object : MonoBehaviour
{
    private float duration = 0f;
    public float Duration
    {
        get => duration;
    }
    private bool creating;

    private const float defualtRestCreatedHeight = 2.5f;
    private float restCreatedHeight;
    private const float createdDetal = 1.5f;

    private int id;
    public int Id
    {
        get => id;
        set => id = value;
    }

    private Coroutine coroutine;

    private void Start()
    {
        gameObject.AddComponent<QuestTile>();
    }

    private void FixedUpdate()
    {
        if (creating)
            Creating();
        if (gameObject.activeSelf && !creating)
            duration += Time.deltaTime;
    }

    public void Removed()
    {
        coroutine = null;
        Debug.Log("Skill2 Object Removed");
        gameObject.SetActive(false);
        duration = 0;
        var gravity = GetComponent<GravityObject>();
        gravity.ResetGravity();
        gravity.ChagneSelected(null, false);
        var tile = GetComponent<QuestTile>();
        tile.AllGetDownTile();
        tile.StopAllCoroutines();
        StopAllCoroutines();
    }

    public void Created(Vector3 CreatePoint)
    {
        gameObject.SetActive(false);
        GetComponent<Rigidbody>().isKinematic = true;
        transform.position = CreatePoint + Vector3.down * defualtRestCreatedHeight;
        restCreatedHeight = defualtRestCreatedHeight;
        GetComponent<GravityObject>().enabled = false;
        if (TryGetComponent<QuestTile>(out var tile))
        {
            tile.enabled = false;
        }
        Debug.Log($"Skill2Object - {id} Created!");
        gameObject.layer = 7;       // set layer object selector
        if (!creating)
            creating = true;
        gameObject.SetActive(true);

    }

    private void Creating()
    {
        transform.Translate(Vector3.up * createdDetal * Time.deltaTime);
        restCreatedHeight -= createdDetal * Time.deltaTime;
        if (restCreatedHeight <= 0)
        {
            GetComponent<GravityObject>().enabled = true;
            if (TryGetComponent<QuestTile>(out var tile))
            {
                tile.enabled = true;
            }
            GetComponent<Rigidbody>().isKinematic = false;
            creating = false;
            duration = 0;
        }
    }
}
