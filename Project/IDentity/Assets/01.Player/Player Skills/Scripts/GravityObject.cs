using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Protocol;

public class GravityObject : OutlineObject
{
    private Rigidbody _rigidbody;
    [SerializeField] private GravityLevel defualtLevel = GravityLevel.Common;
    [SerializeField] private float defualtDuration = 5f;
    [Tooltip("발판으로 사용합니다.")] public bool isTile = true;

    [HideInInspector] public float addedGravity;
    private GravityLevel currLevel;
    private GravityLevel tempLevel;
    private GravityLevel prevLevel;

    private float duration;
    ///* [HideInInspector]*/ public Vector3 velocity;
    ///
    private Vector3 velocity = Vector3.zero;

    private Vector3 resetPosition;
    public int Level
    {
        get => (int)currLevel;
    }

    private bool isSelected = false;        // 중력을 바꿀 수 있는 상태임을 나타내는 flag
    public bool IsSelected
    {
        get => isSelected;
    }
    private bool isAdapted = false;      // 변경된 중력이 적용되어 있는지 나타내는 flag

    private Renderer _renderer;
    private Material _defualtMaterial;
    private Color[] uiColors;
    public Color[] UIColors
    {
        set => uiColors = value;
    }

    private bool isPushed;
    public bool IsPushed
    {
        set => isPushed = value;
    }

    bool isOnGround = false;
    private bool isReseting = false;
    private PlayerAction player;


    private void Start()
    {
        _renderer = GetComponentInChildren<Renderer>();
        _defualtMaterial = _renderer.material;
        // 이후 oulineObject의 원본이 될 모델오브젝트를 찾아 변수에 저장한다.
        MakeOutlineModel();
    }

    private void FixedUpdate()
    {
        if (player == null)
        {
            var players = GameObject.FindObjectsOfType<PlayerAction>();

            if (players.Length < 2)
                return;

            for (int i = 0; i < players.Length; ++i)
            {
                if (players[i].photonView.IsMine)
                    player = players[i];
            }

            if (player.ID != 0)
            {
                _rigidbody.isKinematic = true;
            }
        }
        else
        {
            if (player.ID == 0)
            {
                if (currLevel == GravityLevel.Light)
                {
                    isOnGround = false;
                }

                // 선택이 된 경우 중력을 바꾸고 바뀐 중력에 따라 ui를 적용한다.
                if (isSelected)
                {
                    ChangeGravity();
                    if (prevLevel != tempLevel)
                    {
                        object[] content = new object[] { photonView.ViewID, (int)tempLevel };
                        SendRaiseEvent(EVENTCODE.GRAVITY_OBJ_SET_UI, content, SEND_OPTION.OTHER);
                    }

                    prevLevel = tempLevel;
                    SetUI(uiColors[((int)tempLevel + 5) / 5]);
                }

                if (isAdapted)
                {
                    CheckDuration();
                }

                MoveUpAndDown();
            }
        }
    }

    public override void OnEnable()
    {
        // 네트워크 이벤트 추가
        PhotonNetwork.NetworkingClient.EventReceived += EventReceived;

        // ui 적용을 위해 타겟이되는 렌더러를 하위 오브젝트에서 불러온다.
        _renderer = GetComponentInChildren<Renderer>();
        _defualtMaterial = _renderer.material;

        _rigidbody = GetComponent<Rigidbody>();

        if (!_rigidbody)
        {
            _rigidbody = gameObject.AddComponent<Rigidbody>();
        }
        _rigidbody.useGravity = false;
        _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        // _rigidbody.mass = Mathf.Clamp(0.5f - (int)currLevel, 0.5f, 16f);
        ResetGravity();

        uiColors = new Color[3];
        uiColors = GravityUISetting._colors;
        gameObject.layer = 7;       // set layer object selector


        resetPosition = transform.position;

        var players = GameObject.FindObjectsOfType<PlayerAction>();

        for (int i = 0; i < players.Length; ++i)
        {
            if (players[i].photonView.IsMine)
                player = players[i];
        }

        if(player.ID != 0)
        {
            _rigidbody.isKinematic = true;
        }
    }

    public override void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= EventReceived;
        if(_defualtMaterial)
            _renderer.material = _defualtMaterial;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Floor") || collision.transform.CompareTag("Creation Area"))
        {
            if(_rigidbody.velocity.y < 0 && currLevel != GravityLevel.Light)
            {
                _rigidbody.velocity = Vector3.zero;
                velocity = Vector3.zero;
            }
        }
    }



    private void ChangeGravity()
    {
        addedGravity += GameInputManager.inputData.gravityInputData * Time.deltaTime * 1000;

        if((int)currLevel + addedGravity <= (int)GravityLevel.Heavy)
        {
            tempLevel = GravityLevel.Heavy;
        }
        else if ((int)currLevel + addedGravity <= (int)GravityLevel.Common)
        {
            tempLevel = GravityLevel.Common;
        }
        else if ((int)currLevel + addedGravity <= (int)GravityLevel.Light)
        {
            tempLevel = GravityLevel.Light;
        }
    }

    public void CheckDuration()
    {
        duration -= Time.deltaTime;
        if(duration < 0)
        {
            ResetGravity();
            isAdapted = false;

            //object[] content = new object[] { photonView.ViewID ,currLevel };
            //SendRaiseEvent(EVENTCODE.GRAVITY_OBJ_STATE, content, SEND_OPTION.OTHER);
        }
    }

    public void AdaptGravity()
    {
        //object[] content = new object[] { photonView.ViewID, tempLevel };
        //SendRaiseEvent(EVENTCODE.GRAVITY_OBJ_STATE, content, SEND_OPTION.OTHER);

        isAdapted = true;
        currLevel = tempLevel;
        duration = defualtDuration;
        addedGravity = 0f;
        velocity = Vector3.zero;

        if (currLevel == GravityLevel.Common || currLevel == GravityLevel.Light)
        {
            _rigidbody.isKinematic = true;
            _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        }
        else
        {
            _rigidbody.isKinematic = false;
            _rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        }
    }

    public void ResetGravity()
    {
        tempLevel = defualtLevel;
        currLevel = defualtLevel;
        addedGravity = 0f;
        velocity = Vector3.zero;

        if (currLevel == GravityLevel.Common || currLevel == GravityLevel.Light )
        {
            _rigidbody.isKinematic = true;
            _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        }
        else
        {
            _rigidbody.isKinematic = false;
            _rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        }
    }

    public void MoveUpAndDown()
    {
        velocity += Vector3.up * (int)currLevel * Time.deltaTime;
        if (currLevel == GravityLevel.Light)
        {
            transform.position += velocity * Time.deltaTime;
        }
        else
            _rigidbody.velocity = velocity;

    }

    private void SetUI(Color color)
    {
        _renderer.material.SetColor("_EmissionColor", color);
    }

    public void ChagneSelected(Material uiMaterial, bool selected)
    {
        if (uiMaterial == null)
            _renderer.material = _defualtMaterial;
        else
            _renderer.material = uiMaterial;
        isSelected = selected;
    }

    public void SendRaiseEvent(EVENTCODE eventcode, object[] content, SEND_OPTION sendoption)
    {
        // debug
        string DebugStr = string.Empty;
        DebugStr = "[SEND__" + eventcode.ToString() + "]";
        for (int i = 0; i < content.Length; ++i)
        {
            DebugStr += "_" + content[i];
        }
        Debug.Log(DebugStr);

        RaiseEventOptions raiseEventOption = new RaiseEventOptions
        {
            Receivers = (ReceiverGroup)sendoption,
        };
        PhotonNetwork.RaiseEvent((byte)eventcode, content, raiseEventOption, SendOptions.SendReliable);
    }

    public Vector3 GetRigidbodyVelocity()
    {
        return velocity;
    }

    private void EventReceived(EventData photonEvent)
    {
        int code = photonEvent.Code;

        if (code == (int)EVENTCODE.GRAVITY_OBJ_SET_UI)
        {
            object[] datas = (object[])photonEvent.CustomData;

            if (photonView.ViewID == (int)datas[0])
            {
                SetUI(uiColors[((int)datas[1] + 5) / 5]);
            }
        }
    }
}
