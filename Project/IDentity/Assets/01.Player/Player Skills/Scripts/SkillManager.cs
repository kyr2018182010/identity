using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using Protocol;
using UnityEngine;
using UnityEngine.UI;

public class SkillManager : MonoBehaviourPunCallbacks
{
    private Text focus;
    public Text Focus
    {
        set => focus = value;
    }

    [HideInInspector] public bool[] isKeyHolding = new bool[2];

    [Header("Gravity")]
    [HideInInspector] public GravityObject[] _selection = new GravityObject[2];
    private GravityObject _gravityTarget;
    [SerializeField] private Material gravityUIMaterial;
    [SerializeField] private Color[] gravityUIColors = new Color[5];
    public Color[] GravityColors
    {
        get => gravityUIColors;
    }

    [Header("Skill2")]
    private Vector3 _creationPoint;
    public Vector3 CreationPoint
    {
        set => _creationPoint = value;
    }
    private bool _creatable;
    public bool Creatable
    {
        set => _creatable = value;
    }

    [SerializeField] GameObject skill2ObjectPrefab;
    [SerializeField] Material skill2UIMateiral;
    [SerializeField] private Color defualtUIColor;
    private Skill2Object[] skill2Objects;
    private GameObject skill2UIObject;
    private const int maxObjectCount = 5;
    private const float maxDuration = 15f;
    private int objectCount = 0;
    private bool prevColor = false; // 0: defalut, 1: red

    private PlayerAction player;
    public PlayerAction Player
    {
        set => player = value;
    }

    public override void OnEnable()
    {
        // 네트워크 이벤트 추가
        PhotonNetwork.NetworkingClient.EventReceived += EventReceived;

        // set Focus UI
        //if (focus == null)
        //    Debug.LogError("SKill Manager focus is NULL");

        // skill2의 보석 오브젝트를 하이러키에서 찾는다.
        skill2Objects = FindObjectsOfType<Skill2Object>(true);
        for (int i = 0; i<skill2Objects.Length;++i)
        {
            skill2Objects[i].Id = i;
            skill2Objects[i].gameObject.SetActive(false);
        }

        // skill2의 UI로 사용할 오브젝트를 프리팹을 복사해 생성한다.
        skill2UIObject = Instantiate(skill2ObjectPrefab) as GameObject;
        skill2UIObject.GetComponent<Collider>().isTrigger = true;
        skill2UIObject.GetComponent<Rigidbody>().isKinematic = true;
        var gravityComp = skill2UIObject.GetComponent<GravityObject>();
        if(gravityComp)
            Destroy(gravityComp);
        var skill2Comp = skill2UIObject.GetComponent<Skill2Object>();
        if (skill2Comp)
            Destroy(skill2Comp);
        var skillUI = skill2UIObject.AddComponent<Skill2UI>();
        skillUI.material = skill2UIMateiral;
        skillUI.deualtColor = defualtUIColor;
        skill2UIObject.SetActive(false);
    }

 
    public override void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= EventReceived;
    }

    private void FixedUpdate()
    {
        if (player.ID == 1)
        {
            if (isKeyHolding[1] && _creatable && _selection[1] == null)
            {
                // Skill2 UI
                if (objectCount >= maxObjectCount)
                {
                    if (prevColor == false) // if prev color is default => send packet 
                    {
                        object[] content = new object[] { true };
                        SendRaiseEvent(EVENTCODE.CREATE_OBJ_SET_UI_COLOR, content, SEND_OPTION.OTHER);
                    }

                    prevColor = true;
                    skill2UIMateiral.SetColor("_EmissionColor", Color.red);
                }
                else
                {
                    if (prevColor == true)
                    {
                        object[] content = new object[] { false };
                        SendRaiseEvent(EVENTCODE.CREATE_OBJ_SET_UI_COLOR, content, SEND_OPTION.OTHER);
                    }
                    prevColor = false;
                    skill2UIMateiral.SetColor("_EmissionColor", defualtUIColor);
                }


                if (!skill2UIObject.activeSelf)
                {
                    object[] content = new object[] { true };
                    SendRaiseEvent(EVENTCODE.CREATE_OBJ_SET_UI, content, SEND_OPTION.OTHER);
                    Debug.Log("Skill Manager - Show Skill 2 UI");
                    skill2UIObject.SetActive(true);
                }

                object[] contents = new object[] { _creationPoint.x, _creationPoint.y, _creationPoint.z };
                SendRaiseEvent(EVENTCODE.CREATE_OBJ_UI_TRANSFORM, contents, SEND_OPTION.OTHER);

                skill2UIObject.transform.position = _creationPoint;
            }
            else if (!_creatable || !isKeyHolding[1] || _selection[1] != null)
            {
                if (skill2UIObject.activeSelf)
                {
                    object[] content = new object[] { false };
                    SendRaiseEvent(EVENTCODE.CREATE_OBJ_SET_UI, content, SEND_OPTION.OTHER);

                    skill2UIObject.SetActive(false);
                }
            }

            // 지속시간이 지난 오브젝트를 검사해 삭제한다.
            CheckDurationOverObject();

            if (_selection[1] != null && !isKeyHolding[1])
                _selection[1] = null; if (isKeyHolding[1] && _creatable && _selection[1] == null)
            {
                if (objectCount >= maxObjectCount || !skill2UIObject.GetComponent<Skill2UI>().creatable)
                {
                    if (prevColor == false) // if prev color is default => send packet 
                    {
                        object[] content = new object[] { true };
                        SendRaiseEvent(EVENTCODE.CREATE_OBJ_SET_UI_COLOR, content, SEND_OPTION.OTHER);
                    }

                    prevColor = true;
                    skill2UIMateiral.SetColor("_EmissionColor", Color.red);
                }
                else
                {
                    if (prevColor == true)
                    {
                        object[] content = new object[] { false };
                        SendRaiseEvent(EVENTCODE.CREATE_OBJ_SET_UI_COLOR, content, SEND_OPTION.OTHER);
                    }

                    prevColor = true;
                    skill2UIMateiral.SetColor("_EmissionColor", defualtUIColor);
                }
                if (!skill2UIObject.activeSelf)
                {
                    object[] content = new object[] { true };
                    SendRaiseEvent(EVENTCODE.CREATE_OBJ_SET_UI, content, SEND_OPTION.OTHER);

                    skill2UIObject.SetActive(true);
                }

                object[] contents = new object[] { _creationPoint.x, _creationPoint.y, _creationPoint.z };
                SendRaiseEvent(EVENTCODE.CREATE_OBJ_UI_TRANSFORM, contents, SEND_OPTION.OTHER);

                skill2UIObject.transform.position = _creationPoint;
            }
            else if (!_creatable || !isKeyHolding[1] || _selection[1] != null)
            {
                if (skill2UIObject.activeSelf)
                {
                    object[] content = new object[] { false };
                    SendRaiseEvent(EVENTCODE.CREATE_OBJ_SET_UI, content, SEND_OPTION.OTHER);

                    skill2UIObject.SetActive(false);
                }
            }

            // 지속시간이 지난 오브젝트를 검사해 삭제한다.
            CheckDurationOverObject();

            if (_selection[1] != null && !isKeyHolding[1])
                _selection[1] = null;

        }

    }

    int FindCreatableObject()
    {
        for (int i = 0; i < maxObjectCount; ++i)
        {
            if (!skill2Objects[i].gameObject.activeSelf)
            {
                Debug.Log("Find : " + i);
                return i;
            }
        }
        Debug.Log("Can't Find");
        return -1;
    }

    void CreateObject(int idx)
    { 
        //skill2Objects[idx].transform.position = _creationPoint;
        skill2Objects[idx].Created(_creationPoint);
        objectCount++;

        object[] content = new object[] { idx, _creationPoint };
        SendRaiseEvent(EVENTCODE.CREATE_OBJECT, content, SEND_OPTION.OTHER);
    }

    void DeleteObject(GravityObject selection)
    {
        var obj = selection.GetComponent<Skill2Object>();
        if (obj == null)
            return;
        var tile = selection.GetComponent<QuestTile>();
        if(tile != null)
            tile.AllGetDownTile();
        var idx = obj.Id;
        skill2Objects[idx].Removed();
        objectCount--;
        Debug.Log("Skill Manager - Delte Object");

        object[] content = new object[] { idx };
        SendRaiseEvent(EVENTCODE.DELETE_OBJECT, content, SEND_OPTION.OTHER);
    }

    void CheckDurationOverObject()
    {
        foreach (var obj in skill2Objects)
        {
            if (obj.gameObject.activeSelf && obj.Duration >= maxDuration)
            {
                object[] content = new object[] { obj.Id };
                SendRaiseEvent(EVENTCODE.DELETE_OBJECT, content, SEND_OPTION.OTHER);

                obj.GetComponent<QuestTile>().AllGetDownTile();
                Debug.Log("Skill Manater - Delete Object by duration");
                obj.Removed();
                objectCount--;
            }
        }
    }

    public void SkillButton(bool hold, PlayerSkillType playerID)
    {
        if (hold)
        {
            // button down
            if (_gravityTarget != null)
            {
                _gravityTarget.ChagneSelected(null, false);
                _gravityTarget.ResetGravity();
            }
        }
        isKeyHolding[(int)playerID] = hold;
        focus.enabled = hold;
    }


    public bool SelectButton(PlayerSkillType playerID)
    {
        bool returnVal = false;
        if (playerID == PlayerSkillType.Gravity)
        {
            if (_selection[(int)playerID] != null)
            {
                // 새로운 오브젝트 선택 
                _gravityTarget = _selection[(int)playerID];

                object[] content = new object[] { _gravityTarget.photonView.ViewID, true };
                SendRaiseEvent(EVENTCODE.GRAVITY_OBJ_SET_MATERIAL, content, SEND_OPTION.OTHER);

                _selection[(int)playerID].ChagneSelected(gravityUIMaterial, true);
                _selection[(int)playerID] = null;
                returnVal=  true;
            }
            else if (_gravityTarget != null)
            {
                // 선택했던 오브젝트에 대해 변경된 중력을 적용하고 원래의 메터리얼로 돌려놓는다.
                object[] content = new object[] { _gravityTarget.photonView.ViewID, false };
                SendRaiseEvent(EVENTCODE.GRAVITY_OBJ_SET_MATERIAL, content, SEND_OPTION.OTHER);

                _gravityTarget.ChagneSelected(null, false);
                _gravityTarget.AdaptGravity();
                _gravityTarget = null;
                returnVal = true;
            }

        }
        else if (playerID == PlayerSkillType.CreateObject)
        {
            if (_selection[(int)playerID] != null)
            {
                DeleteObject(_selection[(int)playerID]);
                returnVal = true;
            }
            else
            {
                if (_creatable && objectCount < maxObjectCount && skill2UIObject.GetComponent<Skill2UI>().creatable && isKeyHolding[(int)playerID])
                {
                    var idx = FindCreatableObject();
                    if(idx >=0)
                        CreateObject(idx);
                    returnVal = true;
                }
            }
        }

        isKeyHolding[(int)playerID] = false;
        focus.enabled = false;

        return returnVal;
    }


    public void SendRaiseEvent(EVENTCODE eventcode, object[] content, SEND_OPTION sendoption)
    {
        // debug
        string DebugStr = string.Empty;
        DebugStr = "[SEND__" + eventcode.ToString() + "]";
        for (int i = 0; i < content.Length; ++i)
        {
            DebugStr += "_" + content[i];
        }
        Debug.Log(DebugStr);

        RaiseEventOptions raiseEventOption = new RaiseEventOptions
        {
            Receivers = (ReceiverGroup)sendoption,
        };
        PhotonNetwork.RaiseEvent((byte)eventcode, content, raiseEventOption, SendOptions.SendReliable);
    }

    private void EventReceived(EventData photonEvent)
    {
        int code = photonEvent.Code;

        if (code == (int)EVENTCODE.CREATE_OBJECT)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("RECV__EVENTCODE.INSTANCIATE_ENEMY__" + datas[0] + "_" + datas[1]);

            int idx = (int)datas[0];
            skill2Objects[idx].transform.position = (Vector3)datas[1];
            skill2Objects[idx].gameObject.SetActive(true);
            objectCount++;
        }
        else if( code == (int)EVENTCODE.DELETE_OBJECT)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("RECV__EVENTCODE.INSTANCIATE_ENEMY__" + datas[0]);

            int idx = (int)datas[0];

            if (skill2Objects[idx] == null)
                return;

            var tile = skill2Objects[idx].gameObject.GetComponent<QuestTile>();
            if (tile != null)
                tile.AllGetDownTile();

            skill2Objects[idx].gameObject.SetActive(false);
            objectCount--;
        }
        else if (code == (int)EVENTCODE.CREATE_OBJ_SET_UI_COLOR)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("RECV__skill color" + datas[0]);

            if ((bool)datas[0])
            {
                skill2UIMateiral.SetColor("_EmissionColor", Color.red);
            }
            else
            {
                skill2UIMateiral.SetColor("_EmissionColor", defualtUIColor);

            }
        }
        else if (code == (int)EVENTCODE.CREATE_OBJ_SET_UI)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("RECV__skill set active" + datas[0]);

            if ((bool)datas[0])
            {
                skill2UIObject.SetActive(true);
            }
            else
            {
                skill2UIObject.SetActive(false);
            }
        }
        else if (code == (int)EVENTCODE.CREATE_OBJ_UI_TRANSFORM)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("RECV__skill position" + datas[0]);

            skill2UIObject.transform.position = new Vector3((float)datas[0], (float)datas[1], (float)datas[2]);
        }
        else if (code == (int)EVENTCODE.GRAVITY_OBJ_SET_MATERIAL)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("RECV__skill position" + datas[0]);

            if ((bool)datas[1])
                PhotonView.Find((int)datas[0]).GetComponent<GravityObject>().ChagneSelected(gravityUIMaterial, true);
            else
                PhotonView.Find((int)datas[0]).GetComponent<GravityObject>().ChagneSelected(null, false);
        }
    }
}
