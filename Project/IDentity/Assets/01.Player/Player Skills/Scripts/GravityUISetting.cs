using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityUISetting : MonoBehaviour {
    [SerializeField] Material GravityMaterial;
    [SerializeField] Color[] Colors = new Color[3];

    static public Material _material;
    static public Color[] _colors = new Color[3];

    // Start is called before the first frame update
    void Awake()
    {
        _material = GravityMaterial;
        _colors = Colors;
    }

}
