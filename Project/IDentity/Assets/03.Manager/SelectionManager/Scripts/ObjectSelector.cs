using UnityEngine;

public class ObjectSelector : MonoBehaviour
{
    private SkillManager _skillManager;
    public SkillManager SkillManager
    {
        set => _skillManager = value;
    }

    [HideInInspector] public PlayerAction player;

    private IHilightSelectionResponse _highlihgtSelectionResponse;

    private GravityObject _selection;
    private Vector3 hitPoint;
    private bool creatable;

    [SerializeField] float range;
    [SerializeField] string CreationAreaTag = "Creation Area";
    [SerializeField] string ObjectSelectorLayer = "Object Selector";
    int layerMask = -1;

    private Transform eyePosition;
    public Transform EyePostion
    {
        set => eyePosition = value;
    }
    private Transform firstCameraLookAtPostion;
    public Transform FirstCameraLookAtPosition
    {
        set => firstCameraLookAtPostion = value;
    }

    private void Start()
    {
        if (eyePosition == null)
            Debug.LogError("Object Selector EyePositon : " + eyePosition);
        if (firstCameraLookAtPostion == null)
            Debug.LogError("Object Selector firstCameraLookAtPostion : " + firstCameraLookAtPostion);
        if (_skillManager == null)
            Debug.LogError("Object Selector SkillManager : " + _skillManager);

        layerMask = 1 << LayerMask.NameToLayer(ObjectSelectorLayer);

        _highlihgtSelectionResponse = GetComponent<IHilightSelectionResponse>();
    }

    private void FixedUpdate()
    {
        if (_selection != null)
        {
            _highlihgtSelectionResponse.OnDeselect(_selection.transform);
            _selection = null;

        }

        if (_skillManager.isKeyHolding[(int)player.ID])
        {
            Check();
        }

        if (_selection != null)
        {
            if (player.ID == (int)PlayerSkillType.Gravity)
            {
                _skillManager._selection[(int)player.ID] = _selection;
                _highlihgtSelectionResponse.OnSelect(_selection.transform);
            }
            else
            {
                if(_selection.GetComponent<Skill2Object>() != null)
                {
                    // if Skill2 Object is selected 
                    _skillManager._selection[(int)player.ID] = _selection;
                    _highlihgtSelectionResponse.OnSelect(_selection.transform);
                }
            }
        }
        if (player.ID == (int)PlayerSkillType.CreateObject)
        {
            _skillManager.CreationPoint = hitPoint;
            _skillManager.Creatable = creatable;
        }        
    }

    private void Check()
    {      
        Vector3 look= firstCameraLookAtPostion.position - eyePosition.position;
        Debug.DrawRay(eyePosition.position, look, Color.blue,range);
        if (Physics.Raycast(eyePosition.position, look, out var hit, range, layerMask))
        {
            var gravityObj = hit.transform.GetComponent<GravityObject>();
            if (gravityObj != null)
            {
                _selection = gravityObj;
            }
            else if (hit.transform.CompareTag(CreationAreaTag) && player.ID == (int)PlayerSkillType.CreateObject)
            {
                hitPoint = hit.point;
                creatable = true;
            }
            else if (player.ID == (int)PlayerSkillType.CreateObject)
            {
                hitPoint = new Vector3(-100000, -100000, -100000);
                creatable = false;
            }
        }
    }
}
