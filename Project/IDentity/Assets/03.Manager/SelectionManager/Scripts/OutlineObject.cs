using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class OutlineObject : MonoBehaviourPunCallbacks
{
    protected Transform _outlineModelTransfrom;
    public Transform OutlineModelTransform
    {
        get => _outlineModelTransfrom;
        set => _outlineModelTransfrom = value;
    }

    virtual public GameObject GetModel()
    {
        if(_outlineModelTransfrom!=null)
            return _outlineModelTransfrom.gameObject;
        return null;
    }
    
    virtual protected void SetModelTransform()
    {
        // 이후 oulineObject의 원본이 될 모델오브젝트를 찾아 변수에 저장한다.
        _outlineModelTransfrom = GetComponentInChildren<Renderer>().transform;
       // _outlineModelTransfrom.gameObject.layer = 5;        // set outline model layer to "UI"
    }

    protected void MakeOutlineModel()
    {
        if (_outlineModelTransfrom ) return;
        var parents = GetComponentsInParent<OutlineObject>();
        foreach(var obj in parents)
        {
            if (obj._outlineModelTransfrom) return;
        }
        SetModelTransform();

        GameObject originModel = GetModel();
        GameObject outlineModel;
        if (originModel != null)
        {
            // 새로운 오브젝트를 생성한다.
            outlineModel = Instantiate(originModel, originModel.transform.position, originModel.transform.rotation, originModel.transform);
            outlineModel.transform.localScale = Vector3.one;

            // Smooth Normal Mesh로 oulineModel을 변경해줘야하는 경우 변경해준다.
            SmoothNormal smoothNormal = originModel.GetComponent<SmoothNormal>();
            if (smoothNormal != null)
            {
                var meshFilter = outlineModel.GetComponent<MeshFilter>();
                if (meshFilter != null)
                {
                    meshFilter.mesh = smoothNormal.SmoothMesh;
                }
                Destroy(outlineModel.GetComponent<SmoothNormal>());
            }

            // outline 모델의 렌더러를 설정한다.
            Renderer renderer = outlineModel.GetComponent<Renderer>();
            renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            renderer.enabled = false;

            // 모델의 종류를 식별해 메터리얼을 설정한다.
            if (GetComponent<NPC>() != null)
            {
                if(OutlineObjectManager._npcOutlineMaterial == null)
                    Debug.Log("NPC Material is NULL");
                renderer.material = OutlineObjectManager._npcOutlineMaterial;
            }
            else
            {
                if (OutlineObjectManager._outlineMaterial == null)
                    Debug.Log("Object Material is NULL");
                renderer.material = OutlineObjectManager._outlineMaterial;
            }

            // 불필요한 컴포넌트를 찾아 삭제한다.
            Collider collider = outlineModel.GetComponent<Collider>();
            Destroy(collider);
            Rigidbody rigidbody = outlineModel.GetComponent<Rigidbody>();
            if (rigidbody != null)
                rigidbody.isKinematic = true;
            if (originModel.GetComponent<Skill2Object>() != null)
            {
                Destroy(outlineModel.GetComponent<Skill2Object>());
                gameObject.SetActive(false);
                outlineModel.transform.position = transform.position;
            }
            if (originModel.TryGetComponent<Collection>(out var collection))
            {
                Destroy(outlineModel.GetComponent<Collection>());
            }
            if (originModel.TryGetComponent<PhotonView>(out var view))
            {
                Debug.Log($"Outline Object -{gameObject.name}");
                Destroy(outlineModel.GetComponent<PhotonView>());
            }
            if (originModel.TryGetComponent<PhotonRigidbodyView>(out var photonRigidbodyView))
            {
                Debug.Log($"Outline Object -{gameObject.name}");
                Destroy(outlineModel.GetComponent<PhotonRigidbodyView>());
            }
            if (originModel.TryGetComponent<PhotonTransformView>(out var trasnformView))
            {
                Debug.Log($"Outline Object -{gameObject.name}");
                Destroy(outlineModel.GetComponent<PhotonTransformView>());
            }

            // outline 모델을 오브젝트의 하위로 설정한다.
            outlineModel.transform.SetParent(transform);
            OutlineModelTransform = outlineModel.transform;
        }
    }
}
