using Subtegral.DialogueSystem.Runtime;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Photon.Pun;
using ExitGames.Client.Photon;
using Protocol;
using Photon.Realtime;

public class NPCSelector : MonoBehaviour
{
    class angleData
    {
        public int id;
        public float angle;

        public angleData(int i, float a)
        {
            i = id;
            a = angle;
        }
    }

    private Transform _playerTransform;
    private Transform[] _npcTransforms;

    [SerializeField] private float range;
    [SerializeField] private float fov;

    private Transform _selection = null;

    // UI
    private IHilightSelectionResponse _hilightSelectionResponse;


    // Quest Parser
    private QuestParser questParser;

    private PlayerAction playerAction;

    // for cursor
    private GameObject dialogueUi;
    private GameObject questUi;


    public PlayerAction Player
    {
        set => playerAction = value;
    }
    private CinemachineVirtualCamera talkVirtualCam;
    public CinemachineVirtualCamera TalkCam
    {
        set => talkVirtualCam = value;
    }

    private void Start() 
    {
        // Check Error
        if (playerAction == null)
            Debug.LogError("NPC Selector Player: " + playerAction);
        if (talkVirtualCam == null)
            Debug.LogError("NPC Selector talkCam: " + talkVirtualCam);

        NPC[] npcs = FindObjectsOfType<NPC>(true);
        _npcTransforms = new Transform[npcs.Length];
        for(int i = 0; i < npcs.Length; ++i)
        {
            _npcTransforms[i] = npcs[i].GetComponent<Transform>();
        }
        _hilightSelectionResponse = GetComponent<IHilightSelectionResponse>();

        questParser = FindObjectOfType<QuestParser>();
        _playerTransform = playerAction.transform;

        RectTransform[] uis = GameObject.FindObjectsOfType<RectTransform>(true);

        for(int i =0;i< uis.Length; ++i)
        {
            if (uis[i].name == "QuestText2")
                questUi = uis[i].gameObject;
            else if (uis[i].name == "Dialogue2")
                dialogueUi = uis[i].gameObject;
        }
    }

    private void FixedUpdate()
    {
        if (playerAction.State == PlayerState.Talk)
            return;
        if (dialogueUi.activeSelf || questUi.activeSelf)
            return;

        // UI를 지운다.
        if(_selection != null)
        {
            _hilightSelectionResponse.OnDeselect(_selection);
            _selection.GetComponent<NPCAI>().SetTalkState(false,null);
        }

        // 시야내에 존재하는지 검사한다.
        _selection = null;
        Check();
        
        // UI를 띄운다.
        if (_selection != null) {
            _hilightSelectionResponse.OnSelect(_selection);
        }
    }

    private void Check()
    {
        // 모든 NPC들의 거리를 계산해 range에 포함되는 NPC만 남긴다.
        List<int> inRangeNpcIdxs = new List<int>();
        for(var  i = 0; i < _npcTransforms.Length; ++i)
        {
            if (!_npcTransforms[i].GetComponent<NPC>().NonSelect)
            {
                var dist = (_npcTransforms[i].transform.position - _playerTransform.position).magnitude;
                if (dist <= range)
                {
                    inRangeNpcIdxs.Add(i);
                }
            }
        }

        // fov 값을 벗어난 뱡향에 있는경우 제외시킨다.
        List<int> inFov = new List<int>();
        for (int i = 0; i < inRangeNpcIdxs.Count; ++i)
        {
            var dir = (_npcTransforms[inRangeNpcIdxs[i]].transform.position - _playerTransform.position).normalized;
            if (Vector3.Angle(_playerTransform.forward, dir) <= fov * 0.5)
            {
                inFov.Add(inRangeNpcIdxs[i]);
            }
        }

        if (inFov.Count > 0)
        {
            var selectedNPC = inFov[0];            // 선택되는 npc가 한개인 경우
            // 선택되는 npc가 여러개인 경우에 대해 처리한다. -> 최소 거리에 있는 npc를 선택하도록 한다.
            if (inFov.Count > 1)
            {
                var minDist = (_npcTransforms[inFov[0]].transform.position - _playerTransform.position).magnitude;
                for (var i = 1; i < inFov.Count; ++i)
                {
                    var dist = (_npcTransforms[inFov[i]].transform.position - _playerTransform.position).magnitude;
                    if (minDist > dist)
                    {
                        selectedNPC = inFov[i];
                        minDist = dist;
                    }
                }
            }
            _selection = _npcTransforms[selectedNPC];
        }
    }

    public void SelectionButton()
    {
        if ( playerAction.State != PlayerState.Talk && _selection != null)
        {
            if (questParser.IsQuestGiver(_selection.gameObject.name, false))
            {
                object[] content = new object[] { _selection.gameObject.name };
                SendRaiseEvent(EVENTCODE.QUEST_START, content, SEND_OPTION.OTHER);

                playerAction.SetTalkState(true, _selection);
                _selection.GetComponent<NPCAI>().SetTalkState(true, playerAction.transform);
                talkVirtualCam.LookAt = _selection.GetComponent<NPC>().Face;
                _hilightSelectionResponse.OnDeselect(_selection);
            }

            else if (questParser.CheckTalkPartner(_selection.gameObject.name, false))
            {
                Debug.Log("CheckTalkPartner OK");
                object[] content = new object[] { _selection.gameObject.name };
                SendRaiseEvent(EVENTCODE.QUEST_TALK, content, SEND_OPTION.OTHER);

                playerAction.SetTalkState(true, _selection);
                if (playerAction.gameObject.TryGetComponent<InventoryOnwer>(out var owner))
                {
                    Debug.Log("TryGetComponent<InventoryOnwer> OK");

                    if (owner.giveTarget)
                    {
                        Debug.Log("giveTarget OK");

                        owner.GiveItems(owner.giveTarget);
                    }
                }
                _selection.GetComponent<NPCAI>().SetTalkState(true, playerAction.transform);
                talkVirtualCam.LookAt = _selection.GetComponent<NPC>().Face;
                _hilightSelectionResponse.OnDeselect(_selection);
            }
            else if (_selection.GetComponent<NPC>().dialogue != null)
            {
                playerAction.SetTalkState(true, _selection);
                _selection.GetComponent<NPCAI>().SetTalkState(true, playerAction.transform);
                talkVirtualCam.LookAt = _selection.GetComponent<NPC>().Face;
                _hilightSelectionResponse.OnDeselect(_selection);

                DialogueParser dialogueParser = Camera.main.GetComponent<DialogueParser>();
                dialogueParser.dialogue = _selection.GetComponent<NPC>().dialogue;
                dialogueParser.StartTalk("talk default", false);
            }
        }
    }

    public void SendRaiseEvent(EVENTCODE eventcode, object[] content, SEND_OPTION sendoption)
    {
        // debug
        string DebugStr = string.Empty;
        DebugStr = "[SEND__" + eventcode.ToString() + "]";
        for (int i = 0; i < content.Length; ++i)
        {
            DebugStr += "_" + content[i];
        }
        Debug.Log(DebugStr);

        RaiseEventOptions raiseEventOption = new RaiseEventOptions
        {
            Receivers = (ReceiverGroup)sendoption,
        };
        PhotonNetwork.RaiseEvent((byte)eventcode, content, raiseEventOption, SendOptions.SendReliable);
    }
}
