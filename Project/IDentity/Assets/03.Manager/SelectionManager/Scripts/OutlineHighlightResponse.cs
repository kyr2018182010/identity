using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineHighlightResponse : MonoBehaviour,IHilightSelectionResponse
{
    public void OnDeselect(Transform selection)
    {
        selection.GetComponent<OutlineObject>().OutlineModelTransform.GetComponent<Renderer>().enabled = false;
    }

    public void OnSelect(Transform selection)
    {
        if(selection.TryGetComponent<GravityObject>(out var gObj))
        {
            var modelRenderer = selection.GetComponent<OutlineObject>().OutlineModelTransform.GetComponent<Renderer>();
            modelRenderer.material.SetFloat("_Scale", OutlineObjectManager.OutlineScale / modelRenderer.transform.lossyScale.magnitude);
        }       
        selection.GetComponent<OutlineObject>().OutlineModelTransform.GetComponent<Renderer>().enabled = true;
    }
}
