using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;
using Photon.Pun;

public class AutoSetting : MonoBehaviourPunCallbacks
{
    [SerializeField] bool auto = false;
    [SerializeField] private PlayerAction player;

    [SerializeField] private GameObject inputManagerPrefab;
    [SerializeField] GameObject skillManagerPrefab;
    [SerializeField] GameObject selectionManagerPrefab;
    [SerializeField] GameObject stateDrivenPrefab;
    [SerializeField] GameObject skillFocusPrefab;
    private FogController fogController;

    private GameObject skillManagerObj = null;
    private GameObject selectionManagerObj = null;
    private GameObject stateDrivenPrefabObj = null;
    private GameObject inputManager = null;
    private GameObject reflection = null;

    void Awake()
    {
        if (!photonView.IsMine)
            return;


        if (auto)
        {
            // add Prefabs to Hierachy
            skillManagerObj = Instantiate(skillManagerPrefab, Vector3.zero, transform.rotation, transform);
            selectionManagerObj = Instantiate(selectionManagerPrefab, Vector3.zero, transform.rotation, transform);
            stateDrivenPrefabObj = Instantiate(stateDrivenPrefab, Vector3.zero, transform.rotation, transform);
            inputManager = Instantiate(inputManagerPrefab, Vector3.zero, transform.rotation, transform);
        }

        if (player == null)
            Debug.LogError("Auto Setting Player is NULL");
        Transform eyeTransform = null;
        Transform firstAimTransform = null;
        Transform thirdFollowTransform = null;
        var trs = player.GetComponentsInChildren<Transform>();
        foreach (var tr in trs)
        {
            if (tr.name == "Eye Postion")
                eyeTransform = tr;
            else if (tr.name == "1st Camera Aim")
                firstAimTransform = tr;
            else if (tr.name == "3rd Camera Follow")
                thirdFollowTransform = tr;
        }

        if (eyeTransform == null)
            Debug.LogError("Auto Setting: eyeTransform is NULL");
        if (firstAimTransform == null)
            Debug.LogError("Auto Setting: firstAimTransform is NULL");
        if (thirdFollowTransform == null)
            Debug.LogError("Auto Setting: thirdFollowTransform is NULL");

        // set StateDriven Camera
        CinemachineVirtualCamera talkCam = null;
        CinemachineVirtualCamera thirdPersonCam = null;
        CinemachineVirtualCamera firstPersonCam = null;
        var vCams = stateDrivenPrefabObj.GetComponentsInChildren<CinemachineVirtualCamera>();
        foreach (var vCam in vCams)
        {
            if (vCam.name == "3rdCamera")
                thirdPersonCam = vCam;
            else if (vCam.name == "1st Camera")
                firstPersonCam = vCam;
            else if (vCam.name == "Talk Cam")
                talkCam = vCam;
        }

        thirdPersonCam.Follow = thirdFollowTransform;
        thirdPersonCam.LookAt = player.transform;
        firstPersonCam.Follow = eyeTransform;
        firstPersonCam.LookAt = firstAimTransform;
        talkCam.Follow = player.transform;
        talkCam.LookAt = firstAimTransform;

        var stateDriven = stateDrivenPrefabObj.GetComponent<CinemachineStateDrivenCamera>();
        stateDriven.m_AnimatedTarget = player.GetComponent<Animator>();
        stateDriven.m_LayerIndex = 1;

        // add skillFocus
        GameObject canvas = GameObject.Find("Canvas(2)");
        var skillFocus = Instantiate(skillFocusPrefab, canvas.transform);

        // set SkillManager
        var skillManager = skillManagerObj.GetComponent<SkillManager>();
        skillManager.Focus = skillFocus.GetComponent<Text>();
        skillManager.Player = player;

        // set SelectorManager
        var objSelector = selectionManagerObj.GetComponent<ObjectSelector>();
        objSelector.SkillManager = skillManager;
        objSelector.EyePostion = eyeTransform;
        objSelector.FirstCameraLookAtPosition = firstAimTransform;
        objSelector.player = player;

        var npcSelector = selectionManagerObj.GetComponent<NPCSelector>();
        npcSelector.Player = player;
        npcSelector.TalkCam = talkCam;

        // Set Main Camera
        Camera camera = Camera.main;

        if (camera == null)
            Debug.LogError("Auto Setting : Main Camera is NULL");

        if (camera.GetComponent<CinemachineBrain>() == null)
            camera.gameObject.AddComponent<CinemachineBrain>();
        if (camera.GetComponent<CameraMoving>() == null)
        {
            camera.gameObject.AddComponent<CameraMoving>();

        }
        var camMoving = camera.GetComponent<CameraMoving>();
        camMoving.Player = player;
        camMoving.SkillManager = skillManager;
        camMoving.ThirdCameraFollow = thirdFollowTransform;
        camMoving.FirstCameraAim = firstAimTransform;
        camera.gameObject.AddComponent<MassiveCloudsUniversalRPCameraTarget>();
        camMoving.enabled = true;


        // Set inputManager
        var gameInputManager = inputManager.GetComponent<GameInputManager>();
        if (player != null)
            gameInputManager.Player = player;

        if (skillManager == null)
            Debug.LogError("AutoSetting : InpuManager.skillManager can't be set(skillManager is NULL)");
        else
            gameInputManager.SkillManager = skillManager;

        if (camMoving == null)
            Debug.LogError("AutoSetting : InpuManager.camMoving can't be set(camMoving is NULL)");
        else
            gameInputManager.MainCamera = camMoving;

        if (npcSelector == null)
            Debug.LogError("AutoSetting : InpuManager.camMoving can't be set(camMoving is NULL)");
        else
            gameInputManager.NPCSelector = npcSelector;

        // follow Quest Player setting
        player.gameObject.AddComponent<FollowQuestPlayer>(); 

        //// seamless
        //var seamlessTerrain = FindObjectOfType<TerrainLoader>();
        //seamlessTerrain.ReSetTerrains(player.transform);

        Debug.Log("Seamless setting");
    }
}
