using UnityEngine;
using UnityEngine.UI;

public struct InputData
{
    public Vector2 runData;
    public Vector2 rotData;
    public bool pushHold;
    public float gravityInputData;
}

public class GameInputManager : MonoBehaviour
{
    private PlayerAction player;
    public PlayerAction Player
    {
        set => player = value;
    }

    private FollowQuestPlayer questPlayer;

    private CameraMoving _mainCamera1;
    public CameraMoving MainCamera
    {
        set => _mainCamera1 = value;
    }

    private SkillManager skillManager;
    public SkillManager SkillManager
    {
        set => skillManager = value;
    }

    private NPCSelector npcSelector;
    public NPCSelector NPCSelector
    {
        set => npcSelector = value;
    }

    //static public float gravityInputData;
    //private float _gravityInputData;

    static public InputData inputData;
    [SerializeField] private KeyCode pauseKey = KeyCode.Escape;
    [SerializeField] private KeyCode invincibleKey = KeyCode.I;


    private GameObject dialogueUi;
    private GameObject questUi;


    private void Start()
    {
        if (player == null)
            Debug.LogError("GameInputManager : No Player");
        else
        {
            questPlayer = player.GetComponent<FollowQuestPlayer>();
        }

        if (_mainCamera1 == null)
            Debug.LogError("Game Input Manager : No Main Camera 1");

        if (skillManager == null)
            Debug.LogError("Game Input Manager : No Skill Manager");

        if (npcSelector == null)
            Debug.LogError("Game Input Manager : No NPC Selector");

        RectTransform[] uis = GameObject.FindObjectsOfType<RectTransform>(true);

        for (int i = 0; i < uis.Length; ++i)
        {
            if (uis[i].name == "QuestText2")
                questUi = uis[i].gameObject;
            else if (uis[i].name == "Dialogue2")
                dialogueUi = uis[i].gameObject;
        }

    }
    private void Update()
    {

        if (player)
            PlayerInput();
        if(_mainCamera1)
            CameraInput();
        if(player)
            SkillInput();
        if (dialogueUi.activeSelf || questUi.activeSelf)
            return;
        if(skillManager)
            LeftClickInput();

        Pause();
        InvincibleMode();
    }

    private void PlayerInput()
    {
        inputData.runData.x = Input.GetAxis("Horizontal");
        inputData.runData.y = Input.GetAxis("Vertical");
        //player.SetRunDate(x, y);

        if (Input.GetButtonDown("Jump"))
            player.JumpStart();

        if (Input.GetButtonDown("Push"))
        {
            inputData.pushHold = true;
        }
        else if (Input.GetButtonUp("Push"))
        {
            inputData.pushHold = false;
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            questPlayer.CallOrStopNPC();
        }


    }

    private void CameraInput()
    {
        if (_mainCamera1 == null)
            return;
        inputData.rotData.x = Input.GetAxis("Mouse X");
        inputData.rotData.y = Input.GetAxis("Mouse Y");
        //_mainCamera1.InputData = new Vector2(rotX, rotY);
    }

    private void SkillInput()
    {
        if (skillManager == null)
            return;

        if (player.ID == (int)PlayerSkillType.Gravity) {
            inputData.gravityInputData = Input.GetAxis("Mouse ScrollWheel");
        }

        if (Input.GetButtonDown("Skill"))
        {
            skillManager.SkillButton(true, (PlayerSkillType)player.ID);
            player.SetSkillAnimation(true);
            _mainCamera1.ChangeView(true);
        }

        if (Input.GetButtonUp("Skill"))
        {
            skillManager.SkillButton(false, (PlayerSkillType)player.ID);
            player.SetSkillAnimation(false);
            _mainCamera1.ChangeView(false);
        }
    }

    private void LeftClickInput()
    {
        if (Input.GetButtonDown("LeftClick"))
        {
            var exeuted = skillManager.SelectButton((PlayerSkillType)player.ID);
            if (exeuted)
                player.SetSkillAnimation(false);
            else
                npcSelector.SelectionButton();
        }
    }

    private void Pause()
    {
        if (Input.GetKeyDown(pauseKey))
        {
            if (GameStateManager.stop)
            {
                GameStateManager.Continue();
            }
            else
            {
                GameStateManager.Pause();
            }
        }
    }

    private void InvincibleMode()
    {
        if (Input.GetKeyDown(invincibleKey))
        {
            GameStateManager.invincibleMode = !GameStateManager.invincibleMode;
        }
    }
}
