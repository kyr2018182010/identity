using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEndPoint : MonoBehaviour
{
    public bool end;
    private InventoryUI inventoryUI;
    private GameStateManager gameManager;
    [Range(0f,5f)][SerializeField] private float delaySeconds;
    Coroutine coroutine;

    private void Awake()
    {
        inventoryUI = FindObjectOfType<InventoryUI>();
        gameManager = FindObjectOfType<GameStateManager>();
    }

    private void OnDisable()
    {
        coroutine = null;
        end = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<PlayerAction>(out var player))
        {
            if (end && coroutine == null)
                coroutine = StartCoroutine(EndEffectAndEnd());
        }
    }

    public void End()
    {
        if (end)
            StartCoroutine(EndEffectAndEnd());
    }

    private IEnumerator EndEffectAndEnd()
    {
        yield return new WaitForSeconds(delaySeconds);
        inventoryUI.ShowPanelEffect(Color.black);
        yield return new WaitForSeconds(1f);
        gameManager.GameEnd();
    }
}
