using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using Protocol;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStateManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject pauseUI;
    [SerializeField] private GameObject gameOverUI;
    [SerializeField] private GameObject PanelsRoot;

    static private SaveAndLoad saveAndLoad;
    static private TimerManager timerManager;
    static public GameObject gameOverPanel;
    static public GameObject pausePanel;
    static private InventoryUI uiManager;
    static private SoundManager sound;

    static public bool stop;
    static public bool invincibleMode = false;

    private void Awake()
    {
        saveAndLoad = GetComponent<SaveAndLoad>();
        timerManager = GetComponent<TimerManager>();
        gameOverPanel = GameObject.Find("GameOver Panel");
        pausePanel = GameObject.Find("Pause Panel");
        stop = false;

        gameOverPanel = gameOverUI;
        pausePanel = pauseUI;
        uiManager = PanelsRoot.GetComponent<InventoryUI>();
        sound = FindObjectOfType<SoundManager>();

    }

    public override void OnEnable()
    {
        // 네트워크 이벤트 추가
        PhotonNetwork.NetworkingClient.EventReceived += EventReceived;
    }


    public override void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= EventReceived;
    }

    static public void Continue()
    {
        uiManager.Close(pausePanel.name);
        stop = uiManager.isOpend(pausePanel.name);
    }

    static public void Pause()
    {
        uiManager.OnOffWindow(pausePanel.name);
        stop = uiManager.isOpend(pausePanel.name);
    }

    static public void GameOver()
    {
        if (invincibleMode) return;
        gameOverPanel.SetActive(true);
        sound.DelayStop();
    }

    public void Restart()
    {
        saveAndLoad.Load();
        sound.Retry();
        FindObjectOfType<BossQuest>().Retry();
        gameOverPanel.SetActive(false);
    }

    public void GameEnd()
    {
        Debug.Log("Game End");

        object[] content = new object[] { FindObjectOfType<EndingScoreCalc>(true).endingScore};
        SendRaiseEvent(EVENTCODE.ENDING_SCORE, content, SEND_OPTION.OTHER);
    }

    static public void Exit()
    {

    }

    public void SendRaiseEvent(EVENTCODE eventcode, object[] content, SEND_OPTION sendoption)
    {
        // debug
        string DebugStr = string.Empty;
        DebugStr = "[SEND__" + eventcode.ToString() + "]";
        for (int i = 0; i < content.Length; ++i)
        {
            DebugStr += "_" + content[i];
        }
        Debug.Log(DebugStr);

        RaiseEventOptions raiseEventOption = new RaiseEventOptions
        {
            Receivers = (ReceiverGroup)sendoption,
        };
        PhotonNetwork.RaiseEvent((byte)eventcode, content, raiseEventOption, SendOptions.SendReliable);
    }

    private void EventReceived(EventData photonEvent)
    {
        int code = photonEvent.Code;
        
        if (code == (int)EVENTCODE.ENDING_SCORE)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("ending score - " + datas[0]);

            int endingScore = FindObjectOfType<EndingScoreCalc>(true).endingScore + (int)datas[0];
            Debug.Log(endingScore);

            if (endingScore < 10 && endingScore > -10)
            {
                SceneManager.LoadScene("EndingCutScene3");
            }
            else if (endingScore >= 10)
            {
                SceneManager.LoadScene("EndingCutScene2");
            }
            else if (endingScore <= -10)
            {
                SceneManager.LoadScene("EndingCutScene1");
            }
        }
    }
}
