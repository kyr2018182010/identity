using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowQuestManager : MonoBehaviour
{
    //static private List<FollowQuestPlayer> players = new List<FollowQuestPlayer>();
    [SerializeField] private FollowQuestNPCAI[] NPCs;

    static private FollowQuestNPCAI[] npcs;

    static public bool doingQuest = false;

    private void Awake()
    {
        if(NPCs != null && NPCs.Length > 0)
            npcs = NPCs;        
    }


    public void StartQuest(string questNodeName,string guid)
    {
        if (npcs == null)
            return;
        foreach(var npc in npcs)
        {
            if(!npc.IsEndQuest && npc.questName == questNodeName)
            {
                doingQuest = true;

                var players = PhotonView.FindObjectsOfType<FollowQuestPlayer>();
                for (int i = 0; i < players.Length; ++i)
                {
                    if (players[i].GetComponent<PhotonView>().IsMine)
                    {
                        players[i].questNPC = npc;
                    }
                }

                var ai = npc.GetComponent<NPCAI>();
                ai.SetTalkState(false, null);
                ai.SetFollowQuestState();
                npc.questGUID = guid;
                npc.StartQuest();
            }
        }
    }
}
