using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class NPCAI : MonoBehaviour
{
    // animation
    [Header("====Animation====")]
    [SerializeField] RuntimeAnimatorController controller;
    [SerializeField] Avatar avatar;
    private Animator _animator;
    readonly private int _hashIdleNum = Animator.StringToHash("NPC Idle Num");
    readonly private int _hashWalk = Animator.StringToHash("NPC Walk");
    readonly private int _hashWalkNum = Animator.StringToHash("NPC Walk Num");
    readonly private int _hashRun = Animator.StringToHash("NPC Run");
    readonly private int _hashTalk = Animator.StringToHash("NPC Talk");
    readonly private int _hashTalkNum = Animator.StringToHash("NPC Talk Num");
    readonly private int _hashSync = Animator.StringToHash("Sync");
    readonly private int _hashAnimStart = Animator.StringToHash("Anim Start");

    // state
    [Header("====State=====")]
    [SerializeField] private NPCState[] states;
    [SerializeField] private NPCStateChangeCondition[] conditions;
    [SerializeField]private int stateIdx = 0;
    [SerializeField] bool roop = false;
    [SerializeField] private float changeTimer;
    private float restTimer = 0f;
    private bool talkNPC = false;
    private bool followQuestNPC = false;
    private bool talking = false;
    private float animStartTimer = 0f;
    private bool animation = false;

    // move
    private NavMeshAgent _agent;
    [SerializeField] float walkingSpeed = 0f;
    [SerializeField] float runningSpeed = 0f;
    [SerializeField] float damping = 3f;
    private bool TileQuest = false;
    private float sync = 1f;

    // target
    [Header("====Destination====")]
    [SerializeField] Transform[] targetTransforms;
    [SerializeField] private int currTargetIdx = 0;
    [SerializeField] float range = 0.1f;

    private FollowQuestNPCAI followNPC;

    void Awake()
    {
        // set Animator
        _animator = gameObject.AddComponent<Animator>();
        _animator.runtimeAnimatorController = controller;
        if (avatar != null)
        {
            _animator.avatar = avatar;
        }

        // sync
        sync = Random.Range(0.7f, 1.1f);
        _animator.SetFloat(_hashSync, sync);
        animStartTimer = Random.Range(0f, 0.8f);

        //state
        if(conditions.Length >0 && conditions[0].condition == NPCStateChange.Time)
        {
            restTimer = conditions[0].value;
        }

        // set NavmeshAgent
        _agent = GetComponent<NavMeshAgent>();
        ChangeSpeed();         
        SetAnimation(states[stateIdx]);

        // set Destination
        if (targetTransforms.Length != 0)
        {
            _agent.SetDestination(targetTransforms[currTargetIdx].position);
        }

        // talkable check
        if (GetComponent<NPC>() != null)
        {
            talkNPC = true;
        }
        followNPC = GetComponent<FollowQuestNPCAI>();
        if (followNPC != null)
            followQuestNPC = true;
    }

    private void OnEnable()
    {
        animStartTimer = Random.Range(0f, 0.8f);
        ChangeSpeed();
        SetAnimation(states[stateIdx]);
        _animator.SetFloat(_hashSync, sync);

        if (targetTransforms.Length != 0)
        {
            _agent.SetDestination(targetTransforms[currTargetIdx].position);

            // talkable check
            if (GetComponent<NPC>() != null)
            {
                talkNPC = true;
            }
            followNPC = GetComponent<FollowQuestNPCAI>();
            if (followNPC != null)
                followQuestNPC = true;
        }

        talking = false;
    }

    private void OnDisable()
    {
        animation = false;   
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (followNPC != null && followNPC.followQuest)
            return;

        // check anim start
        animStartTimer -= Time.deltaTime;
        if (animStartTimer <= 0f && !animation)
        {
            _animator.SetTrigger(_hashAnimStart);
            animation = true;
        }

        if (talkNPC && talking)
            return;

        if (!_agent.enabled)
            return;


        if (animation)
            Move();

        // State Change Check
        if (CheckTimeCondition())
            ChangeState();

        CheckOnTile();
    }

    private bool CheckTimeCondition()
    {
        if (conditions.Length > stateIdx)
        {
            if (conditions[stateIdx].condition == NPCStateChange.Time)
            {
                restTimer -= Time.deltaTime;
                if (restTimer < 0f)
                {
                   // ChangeState();
                    restTimer = conditions[stateIdx].value;
                    return true;
                }
            }
        }
        return false;
    }

    private bool IsDestination(float range,Vector3 vec1, Vector3 vec2)
    {       
        float dist = (vec1 - vec2).magnitude;
        if ( dist <= range)
        {
            return true;
        }
        return false;
    }

    private void CheckDestination()
    {
        var vec1 = new Vector2(targetTransforms[currTargetIdx].position.x, targetTransforms[currTargetIdx].position.z);
        var vec2 = new Vector2(transform.position.x, transform.position.z);
        // if Dist condition exsist check Dist and Change State
        if (conditions.Length > 0 && conditions[stateIdx].condition == NPCStateChange.Dist)
        {
            if (IsDestination(conditions[stateIdx].value, transform.position, targetTransforms[currTargetIdx].position))
            {
                currTargetIdx = (currTargetIdx + 1) % targetTransforms.Length;      // set next target transform
                ChangeState();
                restTimer = conditions[stateIdx].value;
            }
        }
        else // if Dist condition does't exist only check Dist
        {
            if (IsDestination(0.8f, vec1, vec2))
            {
                currTargetIdx = (currTargetIdx + 1) % targetTransforms.Length;      // set next target transform
            }
        }
        _agent.SetDestination(targetTransforms[currTargetIdx].position);
    }

    private void Turn()
    {
        Quaternion rot = Quaternion.LookRotation(_agent.desiredVelocity);
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * damping);
    }

    private void Move()
    {
        // ????? ????????? ??? ??? agent?? ?????��?.
        if (targetTransforms.Length == 0)
            return;

        if (NPCState.Walk1 <= states[stateIdx] && states[stateIdx] <= NPCState.Run)
        {
            // NacMeshAgent?? ???? ?? ???? ????? ?????? ????? ?????? ???
            Turn();
        }

        // ???????? ?????? ???? ???????? ?????????? ???
        CheckDestination();
    }

    private void ChangeState()
    {
        // roop?? true?? ?????? ???? ?????? ??? ????????? ??????, ???? ?????? state?? ????��?.
        if (roop)
            stateIdx = (stateIdx + 1) % states.Length;
        else
        {
            if (stateIdx < states.Length - 1)
                stateIdx++;
        }

        // ????????????? ????????? ????.
        SetAnimation(states[stateIdx]);

        // ??????? ???? speed?? ???????.
        ChangeSpeed();
    }

    private void ChangeSpeed()
    {
        if (NPCState.Idle1 <= states[stateIdx] && states[stateIdx] <= NPCState.Idle3)
            _agent.speed = 0f;
        else if (states[stateIdx] == NPCState.Walk1 || states[stateIdx] == NPCState.Walk2)
            _agent.speed = walkingSpeed * sync;
        else if (states[stateIdx] == NPCState.Run)
            _agent.speed = runningSpeed * sync;
    }

    private void SetAnimation(NPCState state)
    {
        if (followNPC != null && followNPC.followQuest)
            return;

        if (NPCState.Idle1 <= state && state <= NPCState.Idle3)
        {
            _animator.SetBool(_hashWalk, false);
            _animator.SetBool(_hashRun, false);
            _animator.SetBool(_hashTalk, false);
            _animator.SetFloat(_hashIdleNum, (float)state / 2);
            
            if(_agent.enabled)
                _agent.isStopped = true;

        }
        else if (state == NPCState.Walk1 || state == NPCState.Walk2)
        {
            _animator.SetBool(_hashWalk, true);
            _animator.SetBool(_hashRun, false);
            _animator.SetBool(_hashTalk, false);

            _animator.SetFloat(_hashWalkNum, state - NPCState.Walk1);

            if (_agent.enabled)
                _agent.isStopped = false;
        }
        else if (state == NPCState.Run)
        {
            _animator.SetBool(_hashWalk, false);
            _animator.SetBool(_hashRun, true);
            _animator.SetBool(_hashTalk, false);

            if (_agent.enabled)
                _agent.isStopped = false;
        }
        else if (state == NPCState.Talk1 || state == NPCState.Talk2)
        {
            _animator.SetBool(_hashWalk, false);
            _animator.SetBool(_hashRun, false);
            _animator.SetBool(_hashTalk, true);

            _animator.SetFloat(_hashTalkNum, state - NPCState.Talk1 - 0.5f);

            if (_agent.enabled)
                _agent.isStopped = true;
        }
    }

    public void SetTalkState(bool talkState, Transform player)
    {
        if (!talkNPC)
            return;

        if (followNPC != null && followNPC.followQuest)
            return;

        if (!_agent.enabled)
            return;

        if (!talkState && !talking)     // prevent setting wrong talk animation
            return;

        if (talkState)
        {
            SetAnimation(NPCState.Talk1);

            var pos = player.position;
            pos.y = transform.position.y;
            _agent.updateRotation = false;
            _agent.SetDestination(transform.position);
            _agent.isStopped = true;

            transform.LookAt(pos);

        }
        else
        {
            _agent.updateRotation = true;
            SetAnimation(states[currTargetIdx]);
            if (targetTransforms.Length > 0)
            {
                _agent.isStopped = false;
                _agent.SetDestination(targetTransforms[currTargetIdx].position);
            }
        }

        talking = talkState;
    }

    public void SetFollowQuestState()
    {
        _agent.speed = runningSpeed * sync;
        _agent.updateRotation = true;
        SetAnimation(NPCState.Idle3);
        talking = false;
    }


    private bool CheckOnTile()
    {
        if (TileQuest)
        {
            if (Physics.Raycast(transform.position, Vector3.down, out var hit, 1f))
            {
                if (hit.transform.GetComponent<QuestTile>() != null)
                {
                    transform.parent = hit.transform;
                    transform.Translate(Vector3.down * (transform.position.y - hit.point.y));
                    _agent.enabled = false;
                    SetAnimation(states[stateIdx]);
                    StartCoroutine(OnTile());
                    return true;
                }
            }
        }
        return false;
    }
    IEnumerator OnTile()
    {
        // not arrived at tile's center point
        var vec1 = transform.localPosition;
        vec1.y = 0;
        while(!IsDestination(0.1f, vec1,Vector3.zero))
        {
            vec1 = transform.localPosition;
            vec1.y = 0;
            Vector3 dir = (vec1 - Vector3.zero).normalized;
            //transform.Translate(dir * walkingSpeed * sync * Time.deltaTime,Space.Self);
            transform.localPosition = Vector3.LerpUnclamped(transform.localPosition, new Vector3(0.0f,transform.localPosition.y,0.0f),Time.deltaTime);
            yield return new WaitForFixedUpdate();
        }

      
        // moving on Tile
        SetAnimation(NPCState.Idle3);
        yield return CheckTileArrived();
    }

    IEnumerator CheckTileArrived()
    {
        while(!Physics.Raycast(transform.parent.position, Vector3.down, out var hit, 0.1f))
        {
            yield return new WaitForFixedUpdate();
        }

        _agent.enabled = true;
        transform.parent = null;
        SetAnimation(states[stateIdx]);
        yield break;
    }


}
