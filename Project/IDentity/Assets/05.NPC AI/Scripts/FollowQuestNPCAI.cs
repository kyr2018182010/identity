using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class FollowQuestNPCAI : MonoBehaviour
{
    private NavMeshAgent _agent;
    private Animator _animator;
    private Collider _collider;

    readonly private int _hashIdleNum = Animator.StringToHash("NPC Idle Num");
    readonly private int _hashRun = Animator.StringToHash("NPC Run");

    public Vector3 fallVelocity;

    public string questName;
    [HideInInspector] public string questGUID;
    [HideInInspector]public bool followQuest = false;       // 현재 퀘스트를 수행 중인지

    private bool endQuest = false;
    public bool IsEndQuest
    {
        get => endQuest;
    }
    [SerializeField] private Transform destination;

    private bool moving = false;            // player의 호출을 받아 이동중인지
    public bool Moving
    {
        get => moving;
    }
    private float damping = 3f;

    [HideInInspector]public Transform targetPlayer = null;      // 현재의 target 플레이어 = NavAgent의 target Transform

    private int targetID = -1;        // 현재 target 플레이어의 id;
    private Text uiText;
    [SerializeField] private Image NPCStateUI;
    [SerializeField] private Image KeyAnnounceUI;
    private float keyUIRestTime = 5f;

    // on tile state
    [HideInInspector] public QuestTile onTile;
    [HideInInspector] public bool jumpingToFloor;
    [HideInInspector] public Vector3 externalVelocity = Vector3.zero;

    private void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        if (_agent == null)
            Debug.LogError("NPCAI is NULL");

        _animator = GetComponent<Animator>();
        if (_animator == null)
            Debug.LogError("Animator is NULL");

        _collider = GetComponent<Collider>();
        if (_collider == null)
            Debug.LogError("FollowNPC : Collier is NULL");
        _collider.isTrigger = true;
    }

    private void FixedUpdate()
    {
        if (!followQuest)
            return;

        // tile -> ground
        if (jumpingToFloor) {
            transform.position = Vector3.Slerp(transform.position, targetPlayer.position  - targetPlayer.forward.normalized * 0.2f, 0.05f);
            Debug.Log("Jumping to Player");
            if (Physics.Raycast(transform.position + Vector3.up * 0.1f, Vector3.down, out var hit, 1f))     // check Land to Ground
            {
                if (hit.transform.GetComponent<QuestTile>() != null)    return;

                Debug.Log("Land on Floor");
                jumpingToFloor = false;     // stop jumping
                _agent.enabled = true;
                Call(targetPlayer, targetID);        // set agent's target and set animation
            }
            return;
        }

        // Call Key AnnounceUI turn off
        if (keyUIRestTime > 0f)
            keyUIRestTime -= Time.deltaTime;
        else
        {
            if (KeyAnnounceUI.gameObject.activeSelf)
                KeyAnnounceUI.gameObject.SetActive(false);
        }

        if (moving)
            Move();

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (jumpingToFloor)
        {
            if (collision.transform.GetComponent<QuestTile>() != null)
                return;
            Debug.Log("Land on Floor On Collision Enter");
            jumpingToFloor = false;     // stop jumping
            _collider.isTrigger = true;
            _agent.enabled = true;    
            Call(targetPlayer,targetID);        // set agent's target and set animation
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.Equals(destination.gameObject)) EndQuest();
    }

    private void Move()
    {
        if((targetPlayer.position - transform.position).magnitude <= 1.5f)
        {
            Stop(targetID);
            return;
        }

        _agent.SetDestination(targetPlayer.position);
        // NavMeshAgent가 가야 할 방향 벡터를 쿼터니언 타입의 각도로 변환
        Quaternion rot = Quaternion.LookRotation(_agent.desiredVelocity);
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * damping);
    }

    public void OnTile(QuestTile tile)        // only use to set onTile state
    {
        onTile = tile;
        Stop(targetID);
        _agent.enabled = false;
    }

    private bool CheckArrived(float range)
    {
        float dist = (destination.position - transform.position).magnitude;
        if (dist < 1f)
            return true;
        return false;
    }


    public void Call(Transform transform, int playerId)
    {
        if (!followQuest)
            return;

        if (jumpingToFloor)
            return;

        // if on tile, once check over the end position
        if (onTile)
        {
            int id = transform.GetComponent<PlayerAction>().ID;
            if (!onTile.FindPlayer(id))      // if false, change state to jump2floor 
            {
                targetPlayer = transform;           // To follow player, set target transform
                targetID = playerId;
                jumpingToFloor = true;              // Change NPC's state to jumping to Floor
                onTile = null;

                Debug.Log("Start Jump to Player");

                // set UI
                if (uiText != null)
                    uiText.text = "Moving";
            }
        }
        else
        {

            moving = true;
            targetPlayer = transform;
            targetID = playerId;    // photon ID
            _agent.SetDestination(transform.position);
            _agent.isStopped = false;

            // set Animation
            _animator.SetBool(_hashRun, true);

            // set UI Text
            if (uiText != null)
                uiText.text = "Moving";
        }
    }

    public void Stop(int playerID) {
        if (!followQuest || jumpingToFloor)
            return;

        if (targetID == playerID)
        {
            _agent.isStopped = true;
            moving = false;
            targetPlayer = null;
            targetID = -1;

            // set Animation
            _animator.SetBool(_hashRun, false);
            _animator.SetFloat(_hashIdleNum, 1f);

            // set UI Text
            uiText.text = "Stop";
        }
    }

    public void StartQuest()
    {
        Debug.Log("Start Quest");
        followQuest = true;
        NPCStateUI.gameObject.SetActive(true);
        KeyAnnounceUI.gameObject.SetActive(true);
        uiText = NPCStateUI.GetComponentInChildren<Text>();
        if (uiText == null)
            Debug.LogError("Follow Button Text is NULL");
        uiText.text = "Call";
        var ai = GetComponent<NPCAI>();
        ai.enabled = false;
        GetComponent<NPC>().NonSelect = true;
        Stop(targetID);
    }

    public void EndQuest()
    {
        Stop(targetID);
        followQuest = false;
        NPCStateUI.gameObject.SetActive(false);
        GetComponent<NPCAI>().enabled = true;
        GetComponent<NPC>().NonSelect = false;
        enabled = false;
        endQuest = true;
        Debug.Log("End Quest");
    }
}
