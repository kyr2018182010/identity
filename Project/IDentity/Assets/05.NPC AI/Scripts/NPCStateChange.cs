using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum NPCStateChange
{
    Time,
    Dist
}

[Serializable]
public struct NPCStateChangeCondition
{
    public NPCStateChange condition;
    [Range(1,10)]
    public float value;
}
