using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TraumaSlot : MonoBehaviour
{
    private Image normal;
    private Color originNormalColor;
    private Image pressed;
    private Color originPressedColor;
    private Button button;
    private Image content;
    [SerializeField] public GameObject targetPanel;
    [SerializeField] private InventoryUI inventoryUI;

    [HideInInspector] public bool haveTrauma = false;

    private void Awake()
    {
        var objects = GetComponentsInChildren<CanvasGroup>();
        foreach(var obj in objects)
        {
            if(obj.name == "Normal")
            {
                normal = obj.GetComponentInChildren<Image>();
                originNormalColor = normal.color;
            }

            if(obj.name == "Pressed")
            {
                pressed = obj.GetComponentInChildren<Image>();
                originPressedColor = pressed.color;
            }
        }
        normal.color = Color.clear;
        pressed.color = Color.clear;

        button = GetComponent<Button>();
        button.interactable = haveTrauma;

        var imgs = targetPanel.GetComponentsInChildren<Image>();
        foreach (var img in imgs)
        {
            if (img.name == "content") content = img;
        }
    }

    public void PushTrauma(TraumaData data, bool effect)
    {
        normal.sprite = data.icon;
        pressed.sprite = data.icon;
        content.sprite = data.content;
        button.interactable = true;
        normal.color = originNormalColor;
        pressed.color = originPressedColor;

        if(effect) inventoryUI.OpenPanelWithEffect(targetPanel.name, Color.white);
    }

    public void PopTrauma()
    {
        button.interactable = false;
        normal.color = Color.clear;
        pressed.color = Color.clear;
        haveTrauma = false;
    }
}
