using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct Window_Button
{
    public string name;
    public GameObject window;
    public GameObject button;
    public bool on;
    public bool startOpen;
}

public class InventoryUI : MonoBehaviour
{
    [SerializeField] private Window_Button[] window_Buttons;
    [SerializeField] private GameObject effectPanel;
    public string windowFadeIn = "Demo Window In";
    public string windowFadeOut = "Demo Window Out";
    public string buttonFadeIn = "Normal to Pressed";
    public string buttonFadeOut = "Pressed to Normal";

    private int currIdx = -1;

    private void Awake()
    {
        //foreach(var win in window_Buttons)
        //{
        //    if (!win.startOpen)
        //    {
        //        Close(win.name);
        //    }
        //}
    }

    private void Open(int idx)
    {
        var windowAnimator = window_Buttons[idx].window.GetComponent<Animator>();
        windowAnimator.Play(windowFadeIn);

        if (window_Buttons[idx].button)
        {
            var buttonAnimator = window_Buttons[idx].button.GetComponent<Animator>();
            buttonAnimator.Play(buttonFadeIn);
        }
        window_Buttons[idx].on = true;
        currIdx = idx;
    }

    private void Close(int idx)
    {
        var windowAnimator = window_Buttons[idx].window.GetComponent<Animator>();
        windowAnimator.Play(windowFadeOut);

        if (window_Buttons[idx].button)
        {
            var buttonAnimator = window_Buttons[idx].button.GetComponent<Animator>();
            buttonAnimator.Play(buttonFadeOut);
        }
        window_Buttons[idx].on =false;
        currIdx = -1;
    }

    private int FindIndex(string name)
    {
        for (int i = 0; i < window_Buttons.Length; ++i)
        {
            if (window_Buttons[i].name == name)
            {
                return i;
            }
        }
        return -1;
    }

    public void OnOffWindow(string name)
    {
        var idx = FindIndex(name);
        if (idx == -1) return;
        if (window_Buttons[idx].on)
            Close(idx);
        else
        {
            if (currIdx != idx && currIdx != -1)
            {
                Close(currIdx);
            }
            Open(idx);
        }
    }

    public void Close(string name)
    {
        var idx = FindIndex(name);
        if (idx == -1) return;
        Close(idx);
    }

    public bool isOpend(string name)
    {
        return window_Buttons[FindIndex(name)].on;
    }

    public void OpenPanelWithEffect(string name, Color color)
    {
        effectPanel.GetComponent<Image>().color = color;
        window_Buttons[FindIndex(name)].window.GetComponent<Animator>().Play("target Panel Open");
        effectPanel.GetComponent<Animator>().Play("Effect 1");
    }

    public void ShowPanelEffect(Color color)
    {
        effectPanel.GetComponent<Image>().color = color;
        effectPanel.GetComponent<Animator>().Play("Effect 1");
    }
}
