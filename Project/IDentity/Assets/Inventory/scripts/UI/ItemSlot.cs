using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class ItemSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private GameObject description;
    private Image itemImage;
    private Transform descriptionContainer;
    private TextMeshProUGUI itemName;
    private TextMeshProUGUI itemDescription;
    private TextMeshProUGUI countTxt;
    [HideInInspector] public bool haveItem = false;
    [HideInInspector] public string id;

    private void Awake()
    {
        var imgs = GetComponentsInChildren<Image>(true);
        foreach (var img in imgs)
        {
            if (img.name == "Image")
            {
                itemImage = img;
            }

            if (img.name == "Description")
            {
                description = img.gameObject;
            }
        }

        var texts = GetComponentsInChildren<TextMeshProUGUI>(true);
        foreach (var txt in texts)
        {
            if (txt.name == "name") itemName = txt;
            if (txt.name == "description") itemDescription = txt;
            if (txt.name == "Count") countTxt = txt;
        }

        descriptionContainer = GameObject.Find("Descriptions").transform;
        countTxt.gameObject.SetActive(false);
    }

    public void UpdateSlot(ItemData item, int cnt)
    {
        if (cnt > 0) PushItem(item, cnt);
        else PopItem();
    }

    private void PushItem(ItemData item,int cnt)
    {
        if (cnt <= 1)
        {
            countTxt.gameObject.SetActive(true);
            id = item.id;

            // set & show item Image
            itemImage.sprite = item.sprite;
            itemImage.gameObject.SetActive(true);

            // set description UI
            itemName.SetText(item.itemName);
            itemDescription.SetText(item.description);
            this.description.transform.SetParent(descriptionContainer);
            haveItem = true;
        }

        countTxt.SetText(cnt.ToString());
    }

    public void PopItem()
    {
        haveItem = false;
        id = null;
        itemImage.gameObject.SetActive(false);
        countTxt.gameObject.SetActive(false);
        description.transform.SetParent(transform);
        description.SetActive(false);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (haveItem)
            description.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (haveItem)
            description.SetActive(false);
    }
}
