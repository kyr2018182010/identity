using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraumaPusher : MonoBehaviour
{
    private TraumaManager traumaManager;
    private bool pushed = false;

    private void Awake()
    {
        traumaManager = FindObjectOfType<TraumaManager>();
    }

    public void Push()
    {
        if (!pushed)
        {
            pushed = true;
            traumaManager.PushTrauma(true);
            GetComponent<EffectSoundPlayer>().PlayEffectSound(BGMEnum.FindTrauma,false,0f);
        }
    }
}
