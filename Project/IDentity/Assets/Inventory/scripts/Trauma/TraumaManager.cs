using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraumaManager : MonoBehaviour
{
    [SerializeField] private TraumaDatabase database;

    private List<TraumaSlot> slots = new List<TraumaSlot>();
    private int collectCount = 0;

    private void Awake()
    {
        slots.AddRange(FindObjectsOfType<TraumaSlot>());
        slots.Sort((TraumaSlot a, TraumaSlot b) => a.name.CompareTo(b.name));
    }

    public bool PushTrauma(bool effect)
    {
        if(collectCount >= database.Count)
        {
            return false;
        }

        slots[collectCount].PushTrauma(database.GetTraumaData(collectCount++),effect);
        return true;
    }

    public void PopAll()
    {
        slots.ForEach((TraumaSlot slot) => slot.PopTrauma());
        collectCount = 0;
    }

    public int GetCollectCount()
    {
        return collectCount;
    }

    public void LoadTrauma(int count)
    {
        PopAll();
        for (int i = 0; i < count; ++i)
        {
            PushTrauma(false);
        }
    }
}
