using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(order = 4,menuName ="Inventory/Create TraumaDatabase",fileName ="TraumalDatabase")]
public class TraumaDatabase : ScriptableObject
{
    [SerializeField] private List<TraumaData> datas;
    public int Count
    {
        get => datas.Count;
    }


    public TraumaData GetTraumaData(int idx)
    {
        return datas[idx];
    }
}
