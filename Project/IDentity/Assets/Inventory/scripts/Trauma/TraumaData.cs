using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Trauma",menuName = "Inventory/Create Trauma Data",order = 3)]
public class TraumaData : ScriptableObject
{
    public Sprite icon;
    public string id;
    public Sprite content;
}
