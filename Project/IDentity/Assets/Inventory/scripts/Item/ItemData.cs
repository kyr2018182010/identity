using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(order = 0,fileName = "Item Data",menuName ="Inventory/Create ItemData")]
[System.Serializable]
public class ItemData : ScriptableObject
{
    public string id;
    public string itemName;
    public string description;
    public Sprite sprite;
}
