using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    [SerializeField] ItemDatabase itemDatabase;

    private Dictionary<string, int> havingItemAndCount = new Dictionary<string, int>();
    private List<ItemSlot> itemSlots = new List<ItemSlot>();

    private void Awake()
    {
        itemSlots.AddRange(FindObjectsOfType<ItemSlot>());
        itemSlots.Sort((ItemSlot a, ItemSlot b) => a.name.CompareTo(b.name));
        //itemDatabase.SetIds();
    }

    public void PushItem(Item item)
    {
        var itemData = itemDatabase.Find(item.id);
        if (itemData == null) return;

        ItemSlot target;
        if (!havingItemAndCount.ContainsKey(item.id))
        {
            havingItemAndCount.Add(item.id, 0);
            target = itemSlots.Find((ItemSlot slot) => slot.haveItem == false);
        }
        else
        {
            target = itemSlots.Find((ItemSlot slot) => slot.id == item.id);
        }
        target.UpdateSlot(itemData, ++havingItemAndCount[item.id]);
    }

    public void PushItem(string itemId)
    {
        var itemData = itemDatabase.Find(itemId);
        if (itemData == null) return;

        ItemSlot target;
        if (!havingItemAndCount.ContainsKey(itemId))
        {
            havingItemAndCount.Add(itemId, 0);
            target = itemSlots.Find((ItemSlot slot) => slot.haveItem == false);
        }
        else
        {
            target = itemSlots.Find((ItemSlot slot) => slot.id == itemId);
        }
        target.UpdateSlot(itemData, ++havingItemAndCount[itemId]);
    }

    public void PopItem(string item, int count)
    {
        if(havingItemAndCount.ContainsKey(item) && havingItemAndCount[item] >= count)
        {
            havingItemAndCount[item] -= count;
            var cnt = havingItemAndCount[item];
            if (cnt <= 0)
            {
                havingItemAndCount.Remove(item);

            }
            
            // update slot
            var target = itemSlots.FindIndex((ItemSlot slot) => slot.id == item);
            itemSlots[target].UpdateSlot(itemDatabase.Find(item), cnt);
            if (cnt == 0)
            {
                if (target < itemSlots.Count - 1 && itemSlots[target + 1].haveItem)
                {
                    SwapSlot(itemSlots[target], itemSlots[target + 1], itemDatabase.Find(itemSlots[target + 1].id));
                }
            }
        }
    }

    public bool IsPopItems(List<InventorySaveData> list)
    {
        foreach(var data in list)
        {
            if (!(data.count <= havingItemAndCount[data.id])) return false;
        }
        return true;
    }

#if UNITY_EDITOR
    public void SetIds()
    {
        itemDatabase.SetIds();
    }
#endif

    public List<InventorySaveData> GetSaveDatas()
    {
        var datas = new List<InventorySaveData>();
        foreach(var dic in havingItemAndCount)
        {
            var data = new InventorySaveData(dic.Key, dic.Value);
            datas.Add(data); 
        }
        return datas;
    }

    public void LoadInventory(List<InventorySaveData> saveData)
    {
        if(itemSlots.Count <= 0)
        {
            itemSlots.AddRange(FindObjectsOfType<ItemSlot>());
            itemSlots.Sort((ItemSlot a, ItemSlot b) => a.name.CompareTo(b.name));
        }
        else
        {
            ClearInventory();
        }



        foreach(var data in saveData)
        {
            for(int i = 0;i <data.count; ++i)
                PushItem(data.id);
        }
    }

    private void ClearInventory()
    {
        // clear slots
        foreach(var slot in itemSlots)
        {
            slot.UpdateSlot(itemDatabase.Find(slot.id), 0);
        }

        havingItemAndCount.Clear();
    }

    private void SwapSlot(ItemSlot src, ItemSlot dest,ItemData item)
    {
        if (!dest) Debug.LogError("dest is NULl");
        if (!src) Debug.LogError("Src is NULL");
        src.UpdateSlot(item, havingItemAndCount[item.id]);
        dest.UpdateSlot(item, 0);
    }
}
