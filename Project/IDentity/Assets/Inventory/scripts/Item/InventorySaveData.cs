using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct InventorySaveData
{
    public string id;
    public int count;

    public InventorySaveData(string id, int count)
    {
        this.id = id;
        this.count = count;
    }
}
