using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
[CreateAssetMenu(order = 1, fileName = "������ DB", menuName = "Inventory/Create ItemDatabase")]
public class ItemDatabase : ScriptableObject
{
    [SerializeField] private List<ItemData> items;

    public ItemData Find(string id)
    {
        return items.Find((ItemData item) => item.id == id);
    }


    public void SetIds()
    {
        int cnt = 0;
        items.ForEach((ItemData item) => item.id = (++cnt).ToString());
#if UNITY_EDITOR
        AssetDatabase.SaveAssets();
        Debug.Log("Set Item Data Id");
#endif
    }

}
