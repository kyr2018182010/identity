using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryOnwer : MonoBehaviour
{
    private Inventory inventory;
    [HideInInspector] public Item item;
    [HideInInspector] public ItemTarget giveTarget;

    private void Awake()
    {
        inventory = FindObjectOfType<Inventory>();
    }

    private void Update()
    {
        if(item && !item.pushed && Input.GetButtonDown("LeftClick")){
            if (item.TryGetComponent<Collection>(out var collection)) {
                if (collection.Collect())
                {
                    inventory.PushItem(item);
                    item.pushed = true;
                    item = null;
                }
            }
        }
    }

    private void OnTriggerStay(Collider other)      // test. move GiveItems(..) to questParser
    {
        if(other.TryGetComponent<Item>(out var item))
        {
            if(!item.pushed)
                this.item = item;
        }
    }

    public void GiveItems(ItemTarget target)
    {
        Debug.Log("GiveItems");
        if (!target.allCollect)
        {
            Debug.Log("allCollect");

            var list = target.GetItemCounts();
            if (!inventory.IsPopItems(list)) return;

            Debug.Log("IsPopItems next");

            target.GetItemCounts().ForEach((InventorySaveData item) => inventory.PopItem(item.id, item.count));
            ++target.currQuestIdx;
        }
    }
}
