using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ItemTarget : MonoBehaviour
{
    [System.Serializable]
    private struct Item_Count
    {
        public Item item;
        public int count;
    }
    [System.Serializable]
    private struct QuestCollections
    {
        public List<Item_Count> item_Counts;
    }


    public bool allCollect;
    [SerializeField] private List<QuestCollections> questCollections = new List<QuestCollections>();
    public int currQuestIdx = 0;

    public List<InventorySaveData> GetItemCounts()
    {
        if (currQuestIdx >= questCollections.Count) {
            allCollect = true;
            return new List<InventorySaveData>();
        }
        var datas = new List<InventorySaveData>();
        questCollections[currQuestIdx].item_Counts.ForEach((Item_Count t) =>
        {
            datas.Add(new InventorySaveData(t.item.id, t.count));
        });
        return datas;
    }
}
