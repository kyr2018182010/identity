using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField] ItemData item;
    [HideInInspector] public string id;
    [HideInInspector] public bool pushed;

    private void Awake()
    {
        id = item.id;
        Debug.Log(gameObject.name + ":"+id);
    }
}
