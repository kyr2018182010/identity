using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExceptSeamless : MonoBehaviour
{
    public void RemoveSeamlessTargetInChildren()
    {
        var targets = GetComponentsInChildren<SeamlessTarget>();
        Debug.Log(targets.Length);
        foreach(var t in targets)
        {
            Debug.Log("Destroy SeamlessTarget");
            DestroyImmediate(t);
        }
    }
}
