using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using Subtegral.DialogueSystem.Runtime;
using UnityEngine.SceneManagement;


public class SeamlessTarget : MonoBehaviour
{
    public ObjectTypeEnum type;
    [SerializeField]public string id;

    public void SetObjectType()
    {
        var isNonTarget = (
            TryGetComponent<GravityObject>(out var gravity) || 
            TryGetComponent<QuestTile>(out var tile)  || 
            TryGetComponent<Collection>(out var collection) || 
            TryGetComponent<Destination>(out var destination) || 
            TryGetComponent<OnOffTrigger>(out var onOffTrigger) || 
            TryGetComponent<DeadObject>(out var deadObject) ||
            TryGetComponent<QuestDoor>(out var door) ||
            TryGetComponent<SearchLight>(out var searchLight) ||
            TryGetComponent<NPCAI>(out var npc) ||
            TryGetComponent<QuestTower>(out var tower)
            );

        isNonTarget = isNonTarget || (GetComponentInChildren<QuestObj>() != null);

        if (isNonTarget)
        {
            type = ObjectTypeEnum.NonStatic;
            return;
        }

        type = ObjectTypeEnum.Static;
    } 
}
