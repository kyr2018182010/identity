using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Subtegral.DialogueSystem.Runtime;
using Photon.Pun;

public class SeamlessTargetSetter : MonoBehaviour
{
   public List<string> targetPaths;

#if UNITY_EDITOR
    public void SetSeamlessTarget()
    {
        var prefabGuids= AssetDatabase.FindAssets("t:prefab");
        foreach(var guid in prefabGuids)
        {
            var path = AssetDatabase.GUIDToAssetPath(guid);
            if (IsTargetPath(path))
            {
                var go = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                if (go.GetComponentInChildren<Renderer>())
                {
                    // set semless target
                    if (!go.GetComponentInChildren<SeamlessTarget>())
                    {
                        Debug.Log(path);
                        go.transform.root.gameObject.AddComponent<SeamlessTarget>();
                    }
                }
            }
        }


        AssetDatabase.SaveAssets();
        Debug.Log("completed to set seamless target");
    }

    public void SetSemalessTargetId()
    {
        var collectionContainer = GameObject.Find("CollectObjectContainer");
        var collections = FindObjectsOfType<Collection>(true);
        foreach(var col in collections)
        {
            col.transform.SetParent(collectionContainer.transform);
        }

        var destinationContainer = GameObject.Find("DestinationContainer");
        var destinations = FindObjectsOfType<Destination>(true);
        foreach(var dest in destinations)
        {
            dest.transform.SetParent(destinationContainer.transform);
        }

        var except = FindObjectOfType<ExceptSeamless>();
        if (except)
            except.RemoveSeamlessTargetInChildren();

        var objects = FindObjectsOfType<SeamlessTarget>();
        int id = 0;
        foreach(var obj in objects)
        {
            obj.GetComponent<SeamlessTarget>().id = (id++).ToString();
            obj.SetObjectType();
        }

        var gravityObjects = FindObjectsOfType<GravityObject>();
        foreach (var obj in gravityObjects)
        {
            //var gravityObj = obj.GetComponent<PhotonView>() ?? null;
            //if (gravityObj == null)
            Debug.Log(obj.name);
        }

        Debug.Log("complete set ids");
    }

    private bool IsTargetPath(string path)
    {
        foreach(var p in targetPaths)
        {           
            if (path.Contains(p))
                return true;
        }
        return false;
    }
#endif
}
