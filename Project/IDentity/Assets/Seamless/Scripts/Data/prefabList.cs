using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class prefabList : ScriptableObject
{
    public string[] prefabNames;
    public float sectorSize;
}
