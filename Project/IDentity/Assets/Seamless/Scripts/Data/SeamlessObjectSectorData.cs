using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SeamlessObjectSectorData : ScriptableObject
{
    public int x, z;
    public float sectorSize;
    public Vector2 center;
    public TerrData targetTerrain;
    public List<SeamlessObjectData> objectDatas;
}
