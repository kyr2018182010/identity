using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrData : MonoBehaviour
{
    public int x, z;
    public float sizeX, sizeZ;
    [SerializeField] private List<GameObject> linkedTerrains;

    /*[HideInInspector] */public string id;
    /*[HideInInspector]*/ public GameObject origin;
    /*[HideInInspector]*/ public List<GameObject> splitTiles = new List<GameObject>();
    

    private void Awake()
    {
        id = x.ToString() + '_' + z.ToString();
    }

    private void OnValidate()
    {
        id = x.ToString() + '_' + z.ToString();
    } 

    private void OnEnable()
    {
        if (linkedTerrains != null)
        {
            foreach (var t in linkedTerrains)
            {
                t.SetActive(true);
            }
        }
    }

    private void OnDisable()
    {
        if (linkedTerrains != null)
        {
            foreach (var t in linkedTerrains)
            {
                t.SetActive(false);
            }
        }
    }

    public TerrData(TerrData other) 
    {
        x = other.x;
        z = other.z;
        sizeX = other.sizeX;
        sizeZ = other.sizeZ;
        id = other.id;
    }

    public bool CheckOnPlayer(Vector3 playerPosition, float commonSize)
    {
        var center = new Vector2(x * commonSize + commonSize / 2, z * commonSize + commonSize / 2);
        var left = center.x - commonSize / 2;
        var right = center.x + commonSize / 2;
        var foward = center.y + commonSize / 2;
        var back = center.y - commonSize / 2;

        if (playerPosition.x < left)
            return false;
        if (playerPosition.x > right)
            return false;
        if (playerPosition.z > foward)
            return false;
        if (playerPosition.z < back)
            return false;


        return true;
    }

    public void SetActiveTerrain(bool active)
    {
        if (origin)
        {
            SetActiveOrigin(active);
            return;
        }
        else if (splitTiles.Count >0)
        {
            CheckSplitTiles(active);
            return;
        }
        else if (gameObject.activeSelf != active)
        {
            gameObject.SetActive(active);
        }
    }

    private void SetActiveOrigin(bool active)
    {
        if (origin)
        {
            if(active && !origin.activeSelf)
            {
                origin.SetActive(active);
            }
        }

        if(gameObject.activeSelf != active)
            gameObject.SetActive(active);

        if (!active)
            origin.GetComponent<TerrData>().SetActiveTerrain(active);
    }

    private void CheckSplitTiles(bool active)
    {
        if (!active)
        {
            foreach (var tile in splitTiles)
            {
                if (tile.activeSelf)
                {
                    return;
                }
            }
            gameObject.SetActive(false);
        }
        else
        {
            if (gameObject.activeSelf != active)
                gameObject.SetActive(active);
        }
    }
}
