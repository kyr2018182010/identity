using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SeamlessObjectData
{
    public string originPrefabName;
    public string objectName;
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 scale;
    public string sector;       // save to "x_z"
    public string id;
    public int layer;
    public string tag;
    public string material;
}
