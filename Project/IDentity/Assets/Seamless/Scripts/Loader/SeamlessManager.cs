using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SeamlessManager : MonoBehaviour
{
    private Vector2 worldSize;

    private Dictionary<ObjectTypeEnum,List<float>> sectorSizes;
    private List<StaticObjectLoader> objectLoaders;
    private List<NonStaticObjectLoader> questObjectLoaders;

    public float terrSectorSize;
    private TerrainLoader terrainLoader;

    public Transform target;
    public Transform player;

    public string originSceneName;

    private void Awake()
    {
        var saver = GetComponent<SeamlessSaver>();
        var terrDatas = saver.TerrDatas;
        var prefabs = saver.Prefabs;
        var materials = saver.Materials;
        var questTargets = saver.QuestObjects;
        worldSize = saver.worldSize;
        if (saver != null)
        {
            var list = saver.sectorSizes;               // get size data
            objectLoaders = new List<StaticObjectLoader>();
            questObjectLoaders = new List<NonStaticObjectLoader>();
            sectorSizes = new Dictionary<ObjectTypeEnum, List<float>>();
            var staticSize = new List<float>();
            var questObjectSize = new List<float>();
            foreach (var size in list)
            {
                if (size.type == ObjectTypeEnum.Static)
                {
                    staticSize.Add(size.sectorSize);
                    objectLoaders.Add(new StaticObjectLoader());        // add StaticObjectLoader
                    objectLoaders[objectLoaders.Count - 1].originPrefabs = PrefabPerSize(size.sectorSize, prefabs);
                    objectLoaders[objectLoaders.Count - 1].materials = materials;
                }
                else if(size.type == ObjectTypeEnum.NonStatic)
                {
                    questObjectSize.Add(size.sectorSize);
                    questObjectLoaders.Add(new NonStaticObjectLoader());
                    questObjectLoaders[questObjectLoaders.Count - 1].targets = questTargets;
                }
            }

            sectorSizes.Add(ObjectTypeEnum.Static, staticSize);
            sectorSizes.Add(ObjectTypeEnum.NonStatic, questObjectSize);
        }

        questTargets.ForEach((GameObject obj) => obj.SetActive(false));
        terrainLoader = new TerrainLoader();
        terrainLoader.terrains = terrDatas;

        //saver.DestroyAllStaticObjects();
    }

    private void OnEnable()
    {
        if (target)
        {
            // terrain Seamelss Reset
            terrainLoader.Reset(target, terrSectorSize, (int)(worldSize.x / terrSectorSize), (int)(worldSize.y / terrSectorSize));
            StartCoroutine(terrainLoader.CheckCurrentSector());

            // object Loader Reset
            int idx = 0;
            objectLoaders.ForEach((StaticObjectLoader loader) =>
            {
                loader.Reset(target, sectorSizes[ObjectTypeEnum.Static][idx], (int)(worldSize.x / sectorSizes[ObjectTypeEnum.Static][idx]), (int)(worldSize.y / sectorSizes[ObjectTypeEnum.Static][idx]));
                StartCoroutine(loader.CheckCurrentSector());
                ++idx;
            });

            idx = 0;
            questObjectLoaders.ForEach((NonStaticObjectLoader loader) =>
            {
                loader.Reset(target, sectorSizes[ObjectTypeEnum.NonStatic][idx], (int)(worldSize.x / sectorSizes[ObjectTypeEnum.NonStatic][idx]), (int)(worldSize.y / sectorSizes[ObjectTypeEnum.NonStatic][idx]));
                StartCoroutine(loader.CheckCurrentSector());
                ++idx;
            });
        }
    }

    private void OnDisable()
    {
        // terrain Seamelss Quit
        terrainLoader.Quit();
        StopCoroutine(terrainLoader.CheckCurrentSector());

        // object Loader Quit
        foreach (var loader in objectLoaders)
        {
            loader.Quit();
            StopCoroutine(loader.CheckCurrentSector());
        }
    }

    private List<GameObject> PrefabPerSize(float size, List<GameObject> originPrefabs)
    {
        var list = new List<GameObject>();

        var data = Resources.Load<prefabList>($"Seamless/{originSceneName}/{size}/prefabs_{size}");
        var names = data.prefabNames;

        foreach (var name in names)
        {
            var obj = originPrefabs.Find((GameObject a) => a.name == name);
            list.Add(obj);
        }

        list = originPrefabs;
        return list;
    }
}
