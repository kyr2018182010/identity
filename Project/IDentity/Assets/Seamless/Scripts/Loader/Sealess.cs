using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Seamless
{
    public Transform target;
    protected int currX, currZ;
    protected bool isActive = false;

    [SerializeField] [Range(0.1f, 3f)] protected float callTime;
    [SerializeField] protected int sectorCountX = 8;
    [SerializeField] protected int sectorCountZ = 10;
    [SerializeField] protected float sectorSize = 500f;

    protected PlayerMoveDirection CheckDirection(int x, int z)
    {
        if (x < 0 || x >= sectorCountX || z < 0 || z >= sectorCountZ) return PlayerMoveDirection.OutBound;
        if (x == currX - 1  && z == currZ + 1) return PlayerMoveDirection.Left_Front;
        if (x == currX && z== currZ+ 1) return PlayerMoveDirection.Front;
        if (x == currX + 1  && z == currZ + 1) return PlayerMoveDirection.Right_Front;
        if (x == currX - 1 && z == currZ) return PlayerMoveDirection.Left;
        if (x == currX + 1 && z == currZ) return PlayerMoveDirection.Right;
        if (x == currX - 1 && z == currZ- 1) return PlayerMoveDirection.Left_Back;
        if (x == currX && z  == currZ- 1) return PlayerMoveDirection.Back;
        if (x == currX + 1 && z == currZ -1) return PlayerMoveDirection.Right_Back;
        return PlayerMoveDirection.Skip;
    }
    protected void LoadSectors(int x, int z, PlayerMoveDirection dir)
    {
        var left_front = (currX > 0) && (currZ + 1 < sectorCountZ);
        var front = (currZ + 1 < sectorCountZ);
        var right_front = (currX + 1 < sectorCountX) && (currZ + 1 < sectorCountZ);
        var left = (currX > 0);
        var right = (currX + 1 < sectorCountX);
        var left_back = (currX > 0) && (currZ > 0);
        var back = (currZ > 0);
        var right_back = (currX + 1 < sectorCountX) && (currZ > 0);

        if (dir == PlayerMoveDirection.Skip)
        {
            // set false perv terrains
            if (left_front) UnloadSector(currX - 1, currZ + 1);
            if (front) UnloadSector(currX , currZ + 1);
            if (right_front) UnloadSector(currX + 1, currZ + 1);

            if (left) UnloadSector(currX - 1, currZ);
            UnloadSector(currX, currZ);
            if (right) UnloadSector(currX + 1, currZ);

            if (left_back) UnloadSector(currX - 1, currZ - 1);
            if (back) UnloadSector(currX, currZ - 1);
            if (right_back) UnloadSector(currX + 1, currZ - 1);

            left_front = (x > 0) && (z + 1 < sectorCountZ);
            front = (z + 1 < sectorCountZ);
            right_front = (x + 1 < sectorCountX) && (z + 1 < sectorCountZ);
            left = (x > 0);
            right = (x + 1 < sectorCountX);
            left_back = (x > 0) && (z > 0);
            back = (z > 0);
            right_back = (x + 1 < sectorCountX) && (z > 0);

            // set active 
            if (right_front) LoadSector(x+1,z+1);
            if (front) LoadSector(x , z + 1);
            if (left_front) LoadSector(x - 1, z + 1);

            if (right) LoadSector(x + 1, z);
            LoadSector(x, z);
            if (left) LoadSector(x - 1, z );

            if (right_back) LoadSector(x + 1, z - 1);
            if (back) LoadSector(x , z - 1);
            if (left_back) LoadSector(x - 1, z - 1);
        }
        else if (dir == PlayerMoveDirection.Left_Front)
        {
            // set active false : right, right_front, left_back, back, right_back (pivot : curr)
            if (right) UnloadSector(currX + 1, currZ);
            if (right_front) UnloadSector(currX + 1, currZ + 1);
            if (left_back) UnloadSector(currX - 1, currZ - 1);
            if (back) UnloadSector(currX, currZ - 1);
            if (right_back) UnloadSector(currX + 1, currZ - 1);

            left_front = (x > 0) && (z + 1 < sectorCountZ);
            front = (z + 1 < sectorCountZ);
            right_front = (x + 1 < sectorCountX) && (z + 1 < sectorCountZ);
            right = (x + 1 < sectorCountX);
            right_back = (x + 1 < sectorCountX) && (z > 0);

            // set active true : left_front, front, right_front, right, right_back (pivot : next)
            if (left_front) LoadSector(x - 1, z + 1);
            if (front) LoadSector(x, z + 1);
            if (right_front) LoadSector(x + 1, z + 1);
            if (right) LoadSector(x + 1, z);
            if (right_back) LoadSector(x + 1, z - 1);
        }
        else if (dir == PlayerMoveDirection.Front)
        {
            // set active false : left_back, back, right_back (pivot: curr)
            if (left_back) UnloadSector(currX - 1, currZ - 1);
            if (back) UnloadSector(currX, currZ - 1);
            if (right_back) UnloadSector(currX + 1, currZ - 1);

            left_front = (x > 0) && (z + 1 < sectorCountZ);
            front = (z + 1 < sectorCountZ);
            right_front = (x + 1 < sectorCountX) && (z + 1 < sectorCountZ);

            // set active true : left_front, front, right_front (pivot: next)
            if (right_front) LoadSector(x + 1, z + 1);
            if (front) LoadSector(x, z + 1);
            if (left_front) LoadSector(x - 1, z + 1);
        }
        else if (dir == PlayerMoveDirection.Right_Front)
        {
            // set active false : left_front, left, left_back, back, right_back (pivot: curr)
            if (left_front) UnloadSector(currX - 1, currZ + 1);
            if (left) UnloadSector(currX - 1, currZ);
            if (left_back) UnloadSector(currX - 1, currZ - 1);
            if (back) UnloadSector(currX, currZ - 1);
            if (right_back) UnloadSector(currX + 1, currZ - 1);

            left_front = (x > 0) && (z + 1 < sectorCountZ);
            front = (z + 1 < sectorCountZ);
            right_front = (x + 1 < sectorCountX) && (z + 1 < sectorCountZ);
            right = (x + 1 < sectorCountX);
            right_back = (x + 1 < sectorCountX) && (z > 0);

            // set active true : left_front, front, right_front, right, right_back (pivot : next)
            if (right_front) LoadSector(x + 1, z + 1);
            if (front) LoadSector(x, z + 1);
            if (left_front) LoadSector(x - 1, z + 1);
            if (right) LoadSector(x + 1, z);
            if (right_back) LoadSector(x + 1, z - 1);
        }
        else if (dir == PlayerMoveDirection.Left)
        {
            // set active false : right_front, right, right_back (pivot: curr)
            if (right_front) UnloadSector(currX + 1, currZ + 1);
            if (right) UnloadSector(currX + 1, currZ);
            if (right_back) UnloadSector(currX + 1, currZ - 1);

            left_front = (x > 0) && (z + 1 < sectorCountZ);
            left = (x > 0);
            left_back = (x > 0) && (z > 0);

            // set active true : left_front, left, left_back (pivot : next)
            if (left_front) LoadSector(x - 1, z + 1);
            if (left) LoadSector(x - 1, z);
            if (left_back) LoadSector(x - 1, z - 1);
        }
        else if (dir == PlayerMoveDirection.Right)
        {
            // set active false : left_front, left, left_back (pivot : curr)
            if (left_front) UnloadSector(currX - 1, currZ + 1);
            if (left) UnloadSector(currX - 1, currZ);
            if (left_back) UnloadSector(currX - 1, currZ - 1);

            right_front = (x + 1 < sectorCountX) && (z + 1 < sectorCountZ);
            right = (x + 1 < sectorCountX);
            right_back = (x + 1 < sectorCountX) && (z > 0);

            // set active true :  right_front, right, right_back (pivot : next)
            if (right_front) LoadSector(x + 1, z + 1);
            if (right) LoadSector(x + 1, z);
            if (right_back) LoadSector(x + 1, z - 1);
        }
        else if (dir == PlayerMoveDirection.Left_Back)
        {
            // set active false : left_front, front, right_front, right, right_back (pivot: curr)
            if (left_front) UnloadSector(currX - 1, currZ + 1);
            if (front) UnloadSector(currX, currZ + 1);
            if (right_front) UnloadSector(currX + 1, currZ + 1);
            if (right) UnloadSector(currX + 1, currZ);
            if (right_back) UnloadSector(currX + 1, currZ - 1);

            left_front = (x > 0) && (z + 1 < sectorCountZ);
            left = (x > 0);
            left_back = (x > 0) && (z > 0);
            back = (z > 0);
            right_back = (x + 1 < sectorCountX) && (z > 0);

            // set active true : left_front, left, left_back, back, right_back (pivot : next)
            if (left_front) LoadSector(x - 1, z + 1);
            if (left) LoadSector(x - 1, z);
            if (left_back) LoadSector(x - 1, z - 1);
            if (back) LoadSector(x, z - 1);
            if (right_back) LoadSector(x + 1, z - 1);
        }
        else if (dir == PlayerMoveDirection.Back)
        {
            // set active false : left_front, front, right_front (pivot: curr)
            if (left_front) UnloadSector(currX - 1, currZ + 1);
            if (front) UnloadSector(currX, currZ + 1);
            if (right_front) UnloadSector(currX + 1, currZ + 1);

            left_back = (x > 0) && (z > 0);
            back = (z > 0);
            right_back = (x + 1 < sectorCountX) && (z > 0);

            // set active true : left_back, back, right_back (pivot : next)
            if (left_back) LoadSector(x - 1, z - 1);
            if (back) LoadSector(x, z - 1);
            if (right_back) LoadSector(x + 1, z - 1);
        }
        else if (dir == PlayerMoveDirection.Right_Back)
        {
            // set active false : left_front, front, right_front, left, left_back (pivot: curr)
            if (left_front) UnloadSector(currX - 1, currZ + 1);
            if (front) UnloadSector(currX, currZ + 1);
            if (right_front) UnloadSector(currX + 1, currZ + 1);
            if (left) UnloadSector(currX - 1, currZ);
            if (left_back) UnloadSector(currX - 1, currZ - 1);

            right_front = (x + 1 < sectorCountX) && (z + 1 < sectorCountZ);
            right = (x + 1 < sectorCountX);
            left_back = (x > 0) && (z > 0);
            back = (z > 0);
            right_back = (x + 1 < sectorCountX) && (z > 0);

            // set active true : right_front, right, left_back, back, right_back (pivot : next)
            if (right_front) LoadSector(x + 1, z + 1);
            if (right) LoadSector(x + 1, z);
            if (right_back) LoadSector(x + 1, z - 1);
            if (back) LoadSector(x, z - 1);
            if (left_back) LoadSector(x - 1, z - 1);
        }

        currX = x;
        currZ = z;
    }

    protected abstract void LoadSector(int x, int z);
    protected abstract void UnloadSector(int x, int z);
    public abstract IEnumerator CheckCurrentSector();

    public abstract void Reset(Transform tr, float size, int countX, int countZ);
    public abstract void Quit();

}
