using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonStaticObjectLoader : Seamless
{
    public List<GameObject> targets;
    public float worldSizeX = 4000f;
    public float worldSizeZ = 5000f;

    private Dictionary<Vector2, List<GameObject>> sectors;
    
    class Key
    {
        public int x, z;

        public Key(int x, int z)
        {
            this.x = x;
            this.z = z;
        }

        public string toString()
        {
            return $"{x}_{z}";
        }

        public static bool operator ==(Key op1, Key op2)
        {
            return (op1.x == op2.x) && op1.z == op2.z;
        }

        public static bool operator !=(Key op1, Key op2)
        {
            return (op1.x != op2.x) || op1.z != op2.z;
        }

        public override int GetHashCode()
        {
            return new Vector2(x, z).GetHashCode();
        }
    }

    class KeyCompare : IEqualityComparer<Key>
    {
        public bool Equals(Key a, Key b)
        {
            return (a.x == b.x) && (a.z == b.z);
        }

        public int GetHashCode(Key obj)
        {
            return obj.GetHashCode();
        }
    }

    public override IEnumerator CheckCurrentSector()
    {
        while (isActive)
        {
            var x = (int)(target.position.x / sectorSize);
            var z = (int)(target.position.z / sectorSize);

            if (0 <= x && x < sectorCountX && 0 <= z && z < sectorCountZ)
            {

                if (!(currX == x && currZ == z))
                {
                    var dir = CheckDirection(x, z);
                    if (dir != PlayerMoveDirection.OutBound)
                        LoadSectors(x, z, dir);
                }
            }
            yield return new WaitForSeconds(callTime);
        }
    }

    public override void Quit()
    {
        isActive = false;
    }

    public override void Reset(Transform tr, float size, int countX, int countZ)
    {
        isActive = true;

        this.sectorSize = size;
        this.sectorCountX = countX;
        this.sectorCountZ = countZ;
        Sectoring();
        Debug.Log($"{size} , {countX} , {countZ}");

        this.target = tr;
        if (target)
        {
            currX = (int)(target.position.x / sectorSize);
            currZ = (int)(target.position.z / sectorSize);
        }
    }

    protected override void LoadSector(int x, int z)
    {
        var key = new Vector2(x, z);
        if(sectors.ContainsKey(key))
            sectors[key].ForEach((GameObject obj) => obj.SetActive(true));
    }

    protected override void UnloadSector(int x, int z)
    {
        var key = new Vector2(x, z);
        if (sectors.ContainsKey(key))
            sectors[key].ForEach((GameObject obj) => obj.SetActive(false));
    }

    private void Sectoring()
    {
        sectors = new Dictionary<Vector2, List<GameObject>>();
        targets.ForEach((GameObject obj) => {
            var key = new Vector2((int)(obj.transform.position.x / (int)sectorSize), (int)(obj.transform.position.z) / (int)(sectorSize));
            if (!sectors.ContainsKey(key))
            {
                sectors.Add(key, new List<GameObject>());
            }
            sectors[key].Add(obj);
        });

        TestShow();
    }

    private void TestShow()
    {
        foreach(var sec in sectors)
        {
            var msg = $"{sec.Key.x}_{sec.Key.y} : ";
            sec.Value.ForEach((GameObject obj) =>
            {
                msg += $"{obj.name} ";
            });
            Debug.Log(msg);
        }
    }
}
