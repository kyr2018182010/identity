using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class TerrainLoader : Seamless
{
    public List<TerrData> terrains;
    private TerrData[][] terrainMat;

    public override void Reset(Transform tr, float size, int countX, int countZ)
    {
        Debug.Log("Terrain Loade Reset...");
        isActive = true;
        sectorSize = size;
        sectorCountX = countX;
        sectorCountZ = countZ;
        MakeTerrainMatrix();
        foreach (var t in terrains) t.gameObject.SetActive(false);

        target = tr;
        currX = (int)(target.position.x / sectorSize);
        currZ = (int)(target.position.z / sectorSize);

        SkipPlayer();
        //if (!Test) StartCoroutine(CheckCurrentSector());

    }

    public override void Quit()
    {

    }

    private void OnEnable()
    {
        //if (Test) StartCoroutine(CheckCurrentSector());
    }

    private void OnDisable()
    {
        //StopCoroutine(CheckCurrentSector());
    }
    public void ReSetTerrains(Transform pl)
    {
     
    }


    private void MakeTerrainMatrix()
    {
        // fill moutain area
        var biggerTerrains = terrains.FindAll((TerrData a) => a.sizeX > sectorSize);
        foreach (var t in biggerTerrains)
        {
            var cnt = (int)(t.sizeX / sectorSize) * (int)(t.sizeZ / sectorSize);
            var zIdx = 0;
            for (int i = 1; i < cnt; ++i)
            {
                if (i % 3 == 0)
                {
                    zIdx++;
                }
                var gObj = new GameObject("");
                gObj.AddComponent<TerrData>();
                var newTerrData = gObj.GetComponent<TerrData>();
                newTerrData.origin = t.gameObject;
                newTerrData.x = t.x + i % 3;
                newTerrData.z = t.z + zIdx;
                gObj.transform.position = t.transform.position + new Vector3(sectorSize * (i % 3), 0, sectorSize * zIdx);
                terrains.Add(newTerrData);
                newTerrData.id = t.id;
                gObj.name = t.id;
                t.splitTiles.Add(gObj);
                gObj.SetActive(false);
                // gObj.hideFlags = HideFlags.HideAndDontSave;
            }
        }

        // first sort Terrain List by Terrain's Z
        terrains.Sort((TerrData a, TerrData b) => a.z.CompareTo(b.z));

        // make Terrain Matrix
        List<TerrData[]> terrainArrayList = new List<TerrData[]>();
        int idx = 0;
        var tempArr = new TerrData[sectorCountX];
        foreach (var t in terrains)
        {
            tempArr[t.x] = t;
            if (++idx >= sectorCountX)
            {
                idx = 0;
                var arr = new TerrData[sectorCountX];
                tempArr.CopyTo(arr, 0);
                terrainArrayList.Add(arr);
            }
        }
        Debug.Log("Terrain Loader Make Matrix");

        terrainMat = terrainArrayList.ToArray();
    }

    protected override void LoadSector(int x, int z)
    {
        terrainMat[z][x].SetActiveTerrain(true);
        Debug.Log($"{x}_{z} Terrain Load");
    }

    protected override void UnloadSector(int x, int z)
    {
        terrainMat[z][x].SetActiveTerrain(false);
    }


    public override IEnumerator CheckCurrentSector()
    {
        while (isActive)
        {
            // check curr Terrain
            var x = (int)(target.position.x / sectorSize);
            var z = (int)(target.position.z / sectorSize);

            if ((0 <= x && x < sectorCountX && 0 <= z && z < sectorCountZ) 
                && (currX != x || currZ != z))
            {
                var dir = CheckDirection(x, z);
                if (dir != PlayerMoveDirection.OutBound)
                    LoadSectors(x, z, dir);
            }

            yield return new WaitForSeconds(callTime);
        }
    }

    public void SkipPlayer()
    {
        var x = (int)(target.position.x / sectorSize);
        var z = (int)(target.position.z / sectorSize);
        var dir = CheckDirection(x, z);
        if(dir != PlayerMoveDirection.OutBound)
            LoadSectors(x, z,dir);
    }
}
