using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StaticObjectLoader : Seamless
{
    protected struct SeamlessObjectSector
    {
        public SeamlessObjectSectorData data;
        public bool isActive;
        public GameObject root;
    };

    string folderPath = $"Seamless/{SceneManager.GetActiveScene().name}";
    public float worldSizeX = 4000f;
    public float worldSizeZ = 5000f;
    public List<GameObject> originPrefabs;
    public List<Material> materials;

    protected SeamlessObjectSector[][] sectorMat;

    public float SectorSize
    {
        set => sectorSize = value; 
    }


    public override void Reset(Transform tr, float size, int countX, int countZ)
    {
        isActive = true;

        sectorSize = size;
        sectorCountX = countX;
        sectorCountZ = countZ;
        MakeSectorMatrix();

        target = tr;
        if (target)
        {
            currX = (int)(target.position.x / sectorSize);
            currZ = (int)(target.position.z / sectorSize);
        }


    }


    public override void Quit()
    {
        
    }

    protected void MakeSectorMatrix()
    {
        var sectors = new List<SeamlessObjectSector[]>();
        for(int i = 0; i < sectorCountZ; ++i)
        {
            var arr = new SeamlessObjectSector[sectorCountX];
            for(int j = 0; j < sectorCountX; ++j)
            {
                var data = Resources.Load<SeamlessObjectSectorData>($"{folderPath}/{sectorSize}/{j}_{i}");
                arr[j].data = data;
                arr[j].isActive = false;
            }
            sectors.Add(arr);
        }
        sectorMat = sectors.ToArray();
    }

    protected override void LoadSector(int x, int z)
    {

        sectorMat[z][x].isActive = true;
        sectorMat[z][x].root = new GameObject($"{sectorMat[z][x].data.x}_{sectorMat[z][x].data.z}_{sectorSize}");
        if (sectorMat[z][x].data.objectDatas == null)
        {
            return;
        }
        var cache = new List<GameObject>();
        var containComp = new List<GameObject>();
        foreach (var objData in sectorMat[z][x].data.objectDatas)
        {
            var fileName = objData.originPrefabName;

            GameObject target = null;
            if (cache.Count > 0)
            {
                target = cache.Find((GameObject a) => a.name == fileName);
            }
            if(target == null)
            {
                target = originPrefabs.Find((GameObject a) => a.name == fileName);
                if(target)
                    cache.Add(target);
            }
            if (target == null)
                Debug.LogError($"{fileName}: failed");
            else
            {
                var obj = GameObject.Instantiate(target);
                obj.transform.position = objData.position;
                obj.transform.rotation = objData.rotation;
                obj.transform.localScale = objData.scale;
                obj.name = objData.objectName;
                obj.tag = objData.tag;
                obj.layer = objData.layer;

                if(!(target.TryGetComponent<LODGroup>(out var lod) || target.GetComponentInChildren<Renderer>().material.name.Equals(objData.material)))
                    obj.GetComponentInChildren<Renderer>().material = materials.Find((Material mat) => mat.name.Equals(objData.material));
                obj.transform.parent = sectorMat[z][x].root.transform;
                obj.GetComponent<SeamlessTarget>().id = objData.id;
            }
        }
        
    }

    protected override void UnloadSector(int x, int z)
    {
        if (sectorMat[z][x].root) 
            GameObject.Destroy(sectorMat[z][x].root);
        sectorMat[z][x].isActive = false;
    }

    public override IEnumerator CheckCurrentSector()
    {
        while (isActive)
        {
            var x = (int)(target.position.x / sectorSize);
            var z = (int)(target.position.z / sectorSize);
            if (0 <= x && x < (int)(worldSizeX / sectorSize) && 0 <= z && z < (int)(worldSizeZ / sectorSize))
            {

                if (!(currX == x && currZ == z))
                {
                    var dir = CheckDirection(x, z);
                    if(dir != PlayerMoveDirection.OutBound)
                        LoadSectors(x, z, dir);
                }
            }
            yield return new WaitForSeconds(callTime);
        }
    }
}
