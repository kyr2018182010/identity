using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ObjectSaver
{
#if UNITY_EDITOR
    public int objCount = 0;

    public Dictionary<string, SeamlessObjectSectorData> Sectoring(ObjectSizePerSector sps, List<GameObject> staticObjects, float worldX, float worldZ,List<string> list,List<GameObject> prefabs,List<Material> materials,string path)
    {
        var sectorDatas = new Dictionary<string, SeamlessObjectSectorData>();

        // start sectoring
        foreach (var obj in staticObjects)
        {
            var size = obj.GetComponentInChildren<Renderer>().bounds.size.magnitude;
            if (!AssetDatabase.IsValidFolder($"{path}/Objects"))
            {
                AssetDatabase.CreateFolder(path, "Objects");
            }
            if (sps.objectMin <= size && size < sps.objectMax)
            {
                // find sector
                int x, z;
                x = (int)(obj.transform.position.x / sps.sectorSize);
                z = (int)(obj.transform.position.z / sps.sectorSize);
                var key = $"{x}_{z}";
                // create new Object Data
                var data = CreateNewObjectData(obj, key, list,prefabs, materials, $"{path}/Objects");

                if (sectorDatas.ContainsKey(key))
                {
                    if (!sectorDatas[key].objectDatas.Contains(data))
                        sectorDatas[key].objectDatas.Add(data);
                }
                else
                {
                    var sectorData = new SeamlessObjectSectorData();
                    sectorData.x = x;
                    sectorData.z = z;
                    sectorData.center = new Vector2(x * sps.sectorSize + sps.sectorSize / 2, z * sps.sectorSize + sps.sectorSize / 2);
                    sectorData.sectorSize = sps.sectorSize;
                    sectorData.objectDatas = new List<SeamlessObjectData>();
                    sectorData.objectDatas.Add(data);
                    sectorDatas.Add(key, sectorData);
                }
            }
        }

        for (int i = 0; i < (int)(worldZ / sps.sectorSize); ++i)
        {
            for (int j = 0; j < (int)(worldX / sps.sectorSize); ++j)
            {
                var key = $"{j}_{i}";
                if (!sectorDatas.ContainsKey(key))
                {
                    var sectorData = new SeamlessObjectSectorData();
                    sectorData.x = j;
                    sectorData.z = i;
                    sectorData.center = new Vector2(j * sps.sectorSize + sps.sectorSize / 2, i * sps.sectorSize + sps.sectorSize / 2);
                    sectorData.sectorSize = sps.sectorSize;
                    sectorData.objectDatas = new List<SeamlessObjectData>();
                    sectorDatas.Add(key, sectorData);
                }
            }
        }

        return sectorDatas;
    }

    public List<GameObject> GetTargetObjects(ObjectTypeEnum type)
    {
        // find renderer && static objects
        var targets = GameObject.FindObjectsOfType<SeamlessTarget>();
        var staticObjects = new List<GameObject>();
        foreach (var target in targets)
        {
            if (type == target.type)
            {
                staticObjects.Add(target.gameObject);
            }
        }
        Debug.Log($"{type.ToString()} count : {staticObjects.Count}");
        return staticObjects;
    }

    public SeamlessObjectData CreateNewObjectData(GameObject obj, string sector, List<string> nameList, List<GameObject> prefabs,List<Material> materials,string path)
    {
        var data = new SeamlessObjectData();
        data.sector = sector;
        data.objectName = obj.name;
        data.position = obj.transform.position;
        data.rotation = obj.transform.rotation;
        data.scale = obj.transform.lossyScale;
        data.tag = obj.tag;
        data.layer = obj.layer;
        var prefab = PrefabUtility.GetCorrespondingObjectFromSource(obj);       // find original prefab
        if (!prefab)
        {
            PrefabUtility.SaveAsPrefabAsset(obj, $"Assets/Seamless/Resources/Prefabs/{obj.name}.prefab", out var success);
            if (success) Debug.Log($"Object Seamless : save new prefab - {obj.name}.prefab");
            else Debug.LogError($"Object Seamless : fail to save new prefab - {obj.name}.prefab");
            data.originPrefabName = $"Assets/Seamless/Resources/Prefabs/{obj.name}.prefab";
        }
        else
        {
            data.originPrefabName = prefab.name;
            if (!nameList.Contains(prefab.name))
            {
                nameList.Add(prefab.name);
            }
            if (!prefabs.Contains(prefab)) prefabs.Add(prefab);

        }
        data.id = obj.GetComponent<SeamlessTarget>().id;
        if (!obj.TryGetComponent<LODGroup>(out var lod))
        {
            var mat = obj.GetComponentInChildren<Renderer>().sharedMaterial;
            data.material = mat.name;
            if(!materials.Contains(mat))
                materials.Add(mat);
        }
        //var targetMat = obj.GetComponentInChildren<Renderer>().sharedMaterial;
        //var originPass = AssetDatabase.GetAssetPath(targetMat);
        //data.material = AssetDatabase.LoadAssetAtPath<Material>(originPass);

        //AssetDatabase.CreateAsset(data, $"{path}/{data.id}.asset");
        return data;
    }

    public void SaveToAsset(SeamlessObjectSectorData data,string filePath, string fileName)
    {
        var asset = ScriptableObject.CreateInstance<SeamlessObjectSectorData>();
        asset.x = data.x;
        asset.z = data.z;
        asset.objectDatas = data.objectDatas;
        asset.sectorSize = data.sectorSize;
        asset.center = data.center;
        if (data.targetTerrain)
            asset.targetTerrain = data.targetTerrain;
        // save new asset
        AssetDatabase.CreateAsset(asset, $"{filePath}/{fileName}.asset");
    }

#endif
}
