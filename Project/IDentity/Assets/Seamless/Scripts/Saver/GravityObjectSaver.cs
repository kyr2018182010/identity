using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GravityObjectSaver 
{
#if UNITY_EDITOR
    public List<GameObject> GetGravityObjects()
    {
        var gList = GameObject.FindObjectsOfType<GravityObject>();
        var list = new List<GameObject>();
        foreach(var g in gList)
        {
            list.Add(g.gameObject);
        }
        return list;
    }
#endif
}
