using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif


[System.Serializable]
public struct ObjectSizePerSector
{
    public float sectorSize, objectMin, objectMax;
    public ObjectTypeEnum type;
}

public class SeamlessSaver : MonoBehaviour
{
    [SerializeField] List<GameObject> prefabs = new List<GameObject>();
    [SerializeField] List<TerrData> terrDatas = new List<TerrData>();
    [SerializeField] List<GameObject> questObjects = new List<GameObject>();
    [SerializeField] List<Material> materials = new List<Material>();

    public List<GameObject> Prefabs
    {
        get => prefabs;
    }
    public List<TerrData> TerrDatas
    {
        get => terrDatas;
    }
    public List<GameObject> QuestObjects
    {
        get => questObjects;
    }
    public List<Material> Materials
    {
        get => materials;
    }

    public Vector2 worldSize;
    public ObjectSizePerSector[] sectorSizes;
    public bool showDestroyButton;

    [HideInInspector][SerializeField] private List<GameObject> staticObjects;
#if UNITY_EDITOR
    private List<string> prefabNameList;
    private ObjectSaver staticObjectSaver;

    public void Bake()
    {
        // terrain
        terrDatas.Clear();
        terrDatas.AddRange(FindObjectsOfType<TerrData>());
        materials.Clear();
        prefabs.Clear();

        // check exist path
        if (!AssetDatabase.IsValidFolder("Assets/Resources/Seamless"))
            AssetDatabase.CreateFolder("Assets/Resources", "Seamless");

        if (AssetDatabase.IsValidFolder($"Assets/Resources/Seamless/{SceneManager.GetActiveScene().name}"))
            AssetDatabase.DeleteAsset($"Assets/Resources/Seamless/{SceneManager.GetActiveScene().name}");

        if (!AssetDatabase.IsValidFolder($"Assets/Resources/Seamless/{SceneManager.GetActiveScene().name}")) 
            AssetDatabase.CreateFolder("Assets/Resources/Seamless", SceneManager.GetActiveScene().name);
        if (!AssetDatabase.IsValidFolder("Assets/Resources/Seamless/Prefabs"))
            AssetDatabase.CreateFolder("Assets/Resources/Seamless", "Prefabs");

        foreach (var size in sectorSizes)
        {
            var filePath = $"Assets/Resources/Seamless/{SceneManager.GetActiveScene().name}/{size.sectorSize}";

            // create size folder
            //if (AssetDatabase.IsValidFolder(filePath))
            //    FileUtil.DeleteFileOrDirectory(filePath);
            if (!AssetDatabase.IsValidFolder(filePath))
                AssetDatabase.CreateFolder($"Assets/Resources/Seamless/{SceneManager.GetActiveScene().name}", $"{size.sectorSize}");
        }

        // static object
        staticObjectSaver = new ObjectSaver();
        questObjects = staticObjectSaver.GetTargetObjects(ObjectTypeEnum.NonStatic);
        staticObjects = staticObjectSaver.GetTargetObjects(ObjectTypeEnum.Static);
        prefabNameList = new List<string>();
        prefabs.Clear();
        foreach (var size in sectorSizes)
        {
            // init
            prefabNameList.Clear();
            var filePath = $"Assets/Resources/Seamless/{SceneManager.GetActiveScene().name}/{size.sectorSize}";
            var fileName = $"prefabs_{size.sectorSize}";

            var sectorDatas = staticObjectSaver.Sectoring(size, staticObjects,worldSize.x,worldSize.y,prefabNameList,prefabs, materials, filePath);
            SavePrefabNameList(size.sectorSize, filePath,fileName,prefabNameList);
            foreach (var d in sectorDatas)
            {
                staticObjectSaver.SaveToAsset(d.Value,filePath, d.Key);
            }

        }
        AssetDatabase.SaveAssets();
        prefabs.Sort((GameObject a, GameObject b) => a.name.CompareTo(b.name));

        Debug.Log("Seamless Saver : baking was completed!");
    }

    public void onOffStaticObjects(bool active)
    {
        staticObjects.ForEach((GameObject obj) => obj.SetActive(active));
    }

    private void SavePrefabNameList(float size, string filePath,string fileName,List<string> list)
    {
        var asset = ScriptableObject.CreateInstance<prefabList>();
        asset.sectorSize = size;
        asset.prefabNames = list.ToArray();
        AssetDatabase.CreateAsset(asset, $"{filePath}/{fileName}.asset");
    }



    public void RestoreAllStaticObjects()
    {
        // find objectData's folder path
        string path = $"Assets/Resources/Seamless/{SceneManager.GetActiveScene().name}";
        var staticSectorData = ArrayUtility.FindAll(sectorSizes,
            (ObjectSizePerSector a) => a.type == ObjectTypeEnum.Static);
        var folderPaths = new List<string>();
        staticSectorData.ForEach((ObjectSizePerSector sector) =>
            folderPaths.Add($"{path}/{sector.sectorSize}/Objects"));
        // load object data's guids from folder paths
        var guids = new List<string>();
        foreach (var sector in sectorSizes)
        {
            guids.AddRange(AssetDatabase.FindAssets("t:SeamlessObjectData", folderPaths.ToArray()));
        }

        // load object datas from guid
        var allAssets = new List<SeamlessObjectData>();
        guids.ForEach((string guid) =>
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guid);
            //allAssets.Add(AssetDatabase.LoadAssetAtPath<SeamlessObjectData>(assetPath));
        });

        // create new objects
        staticObjects.Clear();
        allAssets.ForEach((SeamlessObjectData data) => CreateObject(data));

        // test
        Debug.Log("complete to restore");
        Debug.Log($"restore object : {allAssets.Count}");
    }

    private void CreateObject(SeamlessObjectData data)
    {
        // find original prefab
        var target = prefabs.Find((GameObject obj) => obj.name == data.originPrefabName);

        var obj = GameObject.Instantiate(target,data.position,data.rotation);
        obj.transform.position = data.position;
        obj.transform.rotation = data.rotation;
        obj.transform.localScale = data.scale;
        obj.name = data.objectName;
        obj.tag = data.tag;
        obj.layer = data.layer;

        staticObjects.Add(obj);

        //if (data.material)
        //    obj.GetComponentInChildren<Renderer>().material = data.material;
        //obj.GetComponent<SeamlessTarget>().id = data.id;
    }
#endif

    public void DestroyAllStaticObjects()
    {
#if UNITY_EDITOR
        staticObjects.ForEach((GameObject obj) => DestroyImmediate(obj));
#else
        staticObjects.ForEach((GameObject obj) => Destroy(obj));
#endif
    }
}

