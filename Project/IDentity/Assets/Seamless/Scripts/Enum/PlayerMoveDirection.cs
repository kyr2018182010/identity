public enum PlayerMoveDirection
{
    Left_Front,
    Front,
    Right_Front,
    Left,
    Right,
    Left_Back,
    Back,
    Right_Back,
    Skip,
    OutBound
}
