﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Subtegral.DialogueSystem.Editor
{
    public class NodeSearchWindow<T, V> : ScriptableObject,ISearchWindowProvider where T : AbstractGraph where V : EditorWindow
    {
        // graph view class
        private T _graphView;

        private V _window;

        private Texture2D _indentationIcon;

        public void Configure(V window,T graphView)
        {
            _window = window;
            _graphView = graphView;
            
            //Transparent 1px indentation icon as a hack
            _indentationIcon = new Texture2D(1,1);
            _indentationIcon.SetPixel(0,0,new Color(0,0,0,0));
            _indentationIcon.Apply();
        }
        
        public List<SearchTreeEntry> CreateSearchTree(SearchWindowContext context)
        {
            List<SearchTreeEntry> tree;

            if (typeof(V) == typeof(QuestGraphView))
            {
                tree = new List<SearchTreeEntry>{
                    new SearchTreeGroupEntry(new GUIContent("Create Node"), 0),
                    new SearchTreeGroupEntry(new GUIContent("Node"), 1),
                    new SearchTreeEntry(new GUIContent("Node", _indentationIcon))
                    {
                        level = 2, userData = new QuestNode()
                    },
                    new SearchTreeEntry(new GUIContent("Comment Block",_indentationIcon))
                    {
                       level = 1,
                       userData = new Group()
                    }
                };
            }
            else
            {
                tree = new List<SearchTreeEntry>{
                    new SearchTreeGroupEntry(new GUIContent("Create Node"), 0),
                    new SearchTreeGroupEntry(new GUIContent("Node"), 1),
                    new SearchTreeEntry(new GUIContent("Node", _indentationIcon))
                    {
                        level = 2, userData = new DialogueNode()
                    },
                    new SearchTreeEntry(new GUIContent("Comment Block",_indentationIcon))
                    {
                       level = 1,
                       userData = new Group()
                    }
                };
            }
               
            return tree;
        }

        public bool OnSelectEntry(SearchTreeEntry SearchTreeEntry, SearchWindowContext context)
        {
            //Editor window-based mouse position
            var mousePosition = _window.rootVisualElement.ChangeCoordinatesTo(_window.rootVisualElement.parent,
                context.screenMousePosition - _window.position.position);
            var graphMousePosition = _graphView.contentViewContainer.WorldToLocal(mousePosition);
            switch (SearchTreeEntry.userData)
            {
                case DialogueNode dialogueNode:
                    _graphView.CreateNewNode("Node",graphMousePosition);
                    return true;
                case Group group:
                    var rect = new Rect(graphMousePosition, _graphView.DefaultCommentBlockSize);
                     _graphView.CreateCommentBlock(rect);
                    return true;
            }
            return false;
        }
    }
}