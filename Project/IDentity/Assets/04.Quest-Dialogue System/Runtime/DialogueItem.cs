using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueItem : MonoBehaviour
{
    private EndingScoreCalc endingScoreCalc;

    private void OnEnable()
    {
        endingScoreCalc = this.transform.parent.GetComponent<EndingScoreCalc>();
    }

    public void Calc()
    {
        if (endingScoreCalc == null)
            return;
        endingScoreCalc.presedButton = this.gameObject;
        endingScoreCalc.scoreCalc();
    }
}
