using Subtegral.DialogueSystem.Runtime;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StoryManager : MonoBehaviour
{
    [SerializeField]
    private List<QuestContainer> questContainers = new List<QuestContainer>();
    private QuestParser questParser;
    public int currentQuestIndex = 0;

    public void StoryManagerInit()
    {
        Debug.Log("StoryManagerInit");

        SkipPlayer skipPlayer = GameObject.FindObjectOfType<SkipPlayer>() ?? null;

        Debug.Log("skipPlayer okay");

        if (skipPlayer != null)
        {
            Debug.Log("skipPlayer is in scene");

            questContainers = skipPlayer.getQuests().ToList();
        }

        questParser = Camera.main.GetComponent<QuestParser>();
        questParser.quest = questContainers[currentQuestIndex];
        questParser.SetQuest();
    }

    public bool NextQuest()
    {
        if (currentQuestIndex == questContainers.Count - 1)
            return false;

        questParser.quest = questContainers[++currentQuestIndex];
        return true;
    }
}
