using Michsky.UI.ModernUIPack;
using Photon.Pun;
using Subtegral.DialogueSystem.Runtime;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Collection : MonoBehaviour
{
    //private GameObject collectionUi;
    private NotificationManager collectionUi;
    public string guid;
    private Vector3 position;
    private bool collectable = false;
    private GameObject canvas;
    private GameObject canvas2;
    private QuestParser questParser;
    private void Start()
    {
        canvas = GameObject.Find("Canvas");

        collectionUi = canvas.transform.Find("CollectionUi").GetComponent<NotificationManager>();
        questParser = Camera.main.GetComponent<QuestParser>();

        position = this.transform.position;
        position.y += 2;
    }

    private void OnDisable()
    {
        collectionUi.CloseNotification();
    }

    public bool Collect()
    {
        if (!collectable) return false;
        this.gameObject.SetActive(false);
        //collectionUi.gameObject.SetActive(false);
        collectionUi.CloseNotification();

        Debug.Log("collect");

        questParser.CheckCollected(guid, this.gameObject, false);
        return true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!other.transform.root.GetComponent<PhotonView>().IsMine)
                return;

            collectionUi.transform.position = position;
            collectionUi.transform.rotation = Camera.main.transform.rotation;
            Debug.Log("collect tigger");
            if(!collectable)
                collectionUi.OpenNotification();
            collectable = true;
            //if (Input.GetButtonDown("LeftClick"))
            //{
            //    this.gameObject.SetActive(false);
            //    collectionUi.gameObject.SetActive(false);

            //    Debug.Log("collect");

            //    Camera camera = Camera.main;
            //    camera.GetComponent<QuestParser>().CheckCollected(guid, this.gameObject);
            //}
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //collectionUi.gameObject.SetActive(false);
        Debug.Log("collect tigger exit");

        collectionUi.CloseNotification();
        collectable = false;
    }
}
