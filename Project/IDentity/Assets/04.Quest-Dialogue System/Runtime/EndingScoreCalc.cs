using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingScoreCalc : MonoBehaviour
{
    public int endingScore = 0;
    public GameObject presedButton;

    public void scoreCalc()
    {
        int childCount = this.transform.childCount;

        if (childCount < 2)
            return;

        for(int i =0;i< childCount; ++i)
        {
            if(this.transform.GetChild(i) == presedButton.transform)
            {
                if(i == 0)
                {
                    endingScore += 2;
                }
                else if (i == 1)
                {
                    endingScore -= 2;
                }
                break;
            }
        }
    }
}
