using Photon.Pun;
using Photon.Realtime;
using Subtegral.DialogueSystem.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    [SerializeField] string PlayerPrefabName0;
    [SerializeField] string PlayerPrefabName1;

    [SerializeField] Transform PlayerPosition;
    [SerializeField] GameObject TitleCanvas;

    public int playerSkillId;

    private TitleUI titleUI;

    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void Awake()
    {
        //Screen.SetResolution(940, 540, false);

        Screen.SetResolution(1920,1080, true);

        PhotonNetwork.ConnectUsingSettings();
        //PhotonNetwork.AutomaticallySyncScene = true;
    }

    private void OnDestroy()
    {
        PhotonNetwork.Disconnect();
    }

    public override void OnConnectedToMaster()
    {
        //PhotonNetwork.JoinOrCreateRoom("Room", new Photon.Realtime.RoomOptions { MaxPlayers = 6 }, null);
        PhotonNetwork.JoinLobby();
    }
    public override void OnJoinedLobby()
    {
        Debug.Log("로비 접속");
        TitleCanvas.SetActive(true);
        titleUI = TitleCanvas.GetComponent<TitleUI>();

        //Invoke("Setting", 1);
        // 로비 접속 완료 콜백 
    }

    public override void OnRoomListUpdate(List<RoomInfo> _roomList)
    {
        // 룸 리스트 초기화 - 현재 생성된 룸들의 정보가 담긴 리스트가 매개변수로 온다. 
        // 로비 내에 룸이 생성되거나 사라질때 자동 호출되는 콜백 
        titleUI.RoomUpdate(_roomList);
        Debug.Log("room update" + _roomList.Count.ToString());
    }

    public override void OnCreatedRoom()
    {
        titleUI.CreatedRoom();
        Debug.Log("룸 생성 완료");
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        titleUI.CreateRoomFailed();
        Debug.Log("룸 생성 실패");
    }


    public override void OnJoinedRoom()
    {
        Debug.Log("join 룸");
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log("룸 join 실패: " + message);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        PhotonNetwork.CurrentRoom.IsVisible = false;
        titleUI.PopUpMatchUI(newPlayer.NickName);
        Debug.Log("다른 플레이어 접속");
    }

    public override void OnLeftRoom()
    {
        Debug.Log("leave 룸");
    }

    public void LoadPlayer()
    {
        Debug.Log("load player");
        PlayerPosition = GameObject.Find("playertransform").transform;

        if(playerSkillId == 0)
        {
            GameObject player = PhotonNetwork.Instantiate(PlayerPrefabName0, PlayerPosition.position, Quaternion.identity);
            FindObjectOfType<DialogueParser>().player = player.GetComponent<PlayerAction>();
            FindObjectOfType<QuestParser>().player = player.GetComponent<PlayerAction>();

            player.GetComponent<PlayerAction>().ID = playerSkillId;
        }
        else
        {
            GameObject player = PhotonNetwork.Instantiate(PlayerPrefabName1, PlayerPosition.position, Quaternion.identity);

            FindObjectOfType<DialogueParser>().player = player.GetComponent<PlayerAction>();
            FindObjectOfType<QuestParser>().player = player.GetComponent<PlayerAction>();

            player.GetComponent<PlayerAction>().ID = playerSkillId;
        }
        //var seamlessTerrain = FindObjectOfType<TerrainSeamless>();
        //seamlessTerrain.ReSetTerrains(player.transform);
    }
}
