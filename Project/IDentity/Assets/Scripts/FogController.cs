using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using Photon.Pun;
using System.Linq;
using Mewlist.MassiveClouds;

public class FogController : MonoBehaviour
{
    [SerializeField]
    private MassiveCloudsPhysicsCloud massiveCloudsPhysicsCloud;
    [SerializeField]
    private float groundHeight = 5;
    private Transform player;

    void OnEnable()
    {
        StartCoroutine("FindPlayer");
    }


    IEnumerator FogUpdate()
    {
        while(true)
        {
            if (player != null)
                massiveCloudsPhysicsCloud.SetFogHeight(player.position.y + groundHeight);
            yield return new WaitForSeconds(4f);
        }
    }

    IEnumerator FindPlayer()
    {
        while (true)
        {
            var players = PhotonView.FindObjectsOfType<PlayerAction>();

            if(players.Length < 1)
                yield return new WaitForSeconds(0.5f);

            for (int i = 0; i < players.Length; ++i)
            {
                if (players[i].GetComponent<PhotonView>().IsMine)
                    player = players[i].transform;
            }
            StartCoroutine("FogUpdate");
            break;
        }
    }
}
