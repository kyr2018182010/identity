using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Protocol
{
    public enum EVENTCODE
    {
        CREATE_OBJECT,
        DELETE_OBJECT,
        CURSOR_ON_UI,
        CURSOR_EXIT_UI,
        SELECTED_UI,
        NPC_CALL,
        NPC_STOP,
        QUEST_START,
        NEXT_DIALOGUE,
        END_DIALOGUE,
        ACCEPTED_QUEST, 
        QUEST_TALK,
        QUSET_COLLECT,
        ACCEPT_MATCH,
        REFUSE_MATCH,
        SCENE_CHANGE,
        CREATE_OBJ_SET_UI_COLOR,
        CREATE_OBJ_SET_UI,
        CREATE_OBJ_UI_TRANSFORM,
        QUEST_ELECTRIC_TURN_ON,
        GRAVITY_OBJ_SET_UI,
        GRAVITY_OBJ_SET_MATERIAL,
        ENDING_SCORE,
        SKILL_SELECT_OK,
        REMOVE_TRAUMA,
    }

    public enum SEND_OPTION
    {
        OTHER,
        ALL,
        MASTER
    }
}
