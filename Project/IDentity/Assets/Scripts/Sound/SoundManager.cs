using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SoundManager : MonoBehaviour
{
    [System.Serializable]
    public struct SoundPair
    {
        public BGMEnum bgmType;
        public AudioClip clip;
        [Range(0f, 1f)] public float volume;
    }
    [SerializeField] private List<SoundPair> sounds;
    [SerializeField] private Slider slider;
    private AudioSource source;
    private BGMEnum currType;
    private float currVolume;
    [Range(0.1f, 1f)] [SerializeField] private float systemVolumeScale = 0.5f;
    public float SystemVolumeScale
    {
        get => systemVolumeScale;
    }

    void Start()
    {
        source = GetComponent<AudioSource>();
        currType = sounds[0].bgmType;
        currVolume = sounds[0].volume;
        source.clip = sounds[0].clip;
        source.volume = sounds[0].volume * systemVolumeScale;
        source.Play();
    }

    public void ChangeBGM(BGMEnum type)
    {
        if (type != BGMEnum.Timer) currType = type;
        var sound = sounds.Find((SoundPair s) => s.bgmType == type);
        currVolume = sound.volume;
        source.volume = sound.volume * systemVolumeScale;
        source.clip = sound.clip;
        source.Play();
    }

    public void ChangeBGM2Origin()
    {
        ChangeBGM(currType);
    }

    public void StopBGM()
    {
        source.Pause();
    }

    public void ReplayBGM()
    {
        source.Play();
    }

    public void DelayStop()
    {
        source.Stop();
    }

    public void PlayOneShot(BGMEnum type, bool bgmStop, float delay)
    {
        var sound = sounds.Find((SoundPair sound) => sound.bgmType == type);
        var clip = sound.clip;
        if (bgmStop)
        {
            source.Stop();
            //source.Play((ulong)Mathf.Ceil(clip.length) + 1);
        }
        StartCoroutine(WaitAndPlay(delay, clip,sound.volume * systemVolumeScale));
    }

    IEnumerator WaitAndPlay(float delay, AudioClip clip,float volume)
    {
        yield return new WaitForSeconds(delay);
        source.PlayOneShot(clip,volume* systemVolumeScale);
    }

    public void Retry()
    {
        currType = sounds[0].bgmType;
        source.clip = sounds[0].clip;
        source.Play();
    }

    public void OnOffVolume()
    {
        source.mute = !source.mute;
    }

    public void ControllVolume()
    {
        systemVolumeScale = slider.value;
        source.volume = currVolume * systemVolumeScale;
    }
}

