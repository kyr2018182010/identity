using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectSoundPlayer : MonoBehaviour
{
    [SerializeField] private AudioClip clip;
    [Range(0f,1f)][SerializeField] private float volume = 1f;
    private AudioSource source;
    private SoundManager soundManager;
    private bool canPlay = true;
    // Start is called before the first frame update
    void Start()
    {
        if(clip)
            source = gameObject.AddComponent<AudioSource>();
        soundManager = FindObjectOfType<SoundManager>();
    }

    public void PlayEffectSound()
    {
        if (!canPlay) return;
        source.PlayOneShot(clip,volume * soundManager.SystemVolumeScale);
        StartCoroutine(WaitClipTime());
    }

    public void PlayEffectSound(BGMEnum type, bool bgmStop, float delay)
    {
        soundManager.PlayOneShot(type,bgmStop,delay);
    }

    IEnumerator WaitClipTime()
    {
        canPlay = false;
        yield return new WaitForSeconds(clip.length);
        canPlay = true;
    }
}
