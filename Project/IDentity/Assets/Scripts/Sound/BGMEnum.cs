using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BGMEnum
{
    Common,
    Boss,
    Timer,
    BossRoar,
    BossHurt,
    BossDead,
    FindTrauma
}
