using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMSwitch : MonoBehaviour
{
    [SerializeField] private SoundManager manager;
    
    private void Awake()
    {
        manager = FindObjectOfType<SoundManager>();
    }

    public void ChangeBGM(BGMEnum type)
    {
        manager.ChangeBGM(type);
    }

    public void ChangeOrigin()
    {
        manager.ChangeBGM2Origin();
    }
}
