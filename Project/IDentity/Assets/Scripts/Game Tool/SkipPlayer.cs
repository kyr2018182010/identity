using Photon.Pun;
using Subtegral.DialogueSystem.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkipPlayer : MonoBehaviour
{
    [HideInInspector] public PlayerAction Player;
    [SerializeField] private Transform[] transforms;
    [SerializeField] private int currTransformIdx = 0;
    [SerializeField] private KeyCode NextKey;
    [SerializeField] private KeyCode PrevKey;
    [SerializeField] private QuestContainer[] questContainers;
    [SerializeField] private int currQuestIdx = 0;
    [SerializeField] private KeyCode NextQuestKey;
    [SerializeField] private KeyCode PrevQuestKey;
    // Update is called once per frame

    private QuestParser questParser;
    private StoryManager storyManager;

    private void Start()
    {
        questParser = Camera.main.transform.GetComponent<QuestParser>();
        storyManager = Camera.main.transform.GetComponent<StoryManager>();
    }
    void Update()
    {
        // 나중에 한 번만 설정하도록 코드 이동
        if (Player == null)
        {
            var players = PhotonView.FindObjectsOfType<PlayerAction>();
            for(int i =0;i<players.Length;++i)
            {
                if (players[i].GetComponent<PhotonView>().IsMine)
                    Player = players[i];
            }
            return;
        }

        if (Input.GetKeyDown(NextKey))
        {
            if (currTransformIdx < transforms.Length - 1)
            {
                Player.SetNewTransform(transforms[++currTransformIdx].position);
                Debug.Log("Next Position : " + currTransformIdx);
            }
        }

        if (Input.GetKeyDown(PrevKey))
        {
            if (currTransformIdx > 0)
            {
                Player.SetNewTransform(transforms[--currTransformIdx].position);
                Debug.Log("Prev Position : " + currTransformIdx);
            }
        }

        if (Input.GetKeyDown(NextQuestKey))
        {
            if (currQuestIdx < transforms.Length - 1)
            {
                storyManager.currentQuestIndex++;

                questParser.quest = questContainers[++currQuestIdx];
                questParser.SetQuest();
                questParser.ResetQuest();
            }
        }

        if (Input.GetKeyDown(PrevQuestKey))
        {
            if (currQuestIdx > 0)
            {
                storyManager.currentQuestIndex--;

                questParser.quest = questContainers[--currQuestIdx];
                questParser.SetQuest();
                questParser.ResetQuest();
            }
        }
    }

    public QuestContainer[] getQuests()
    {
        return questContainers;
    }
}
