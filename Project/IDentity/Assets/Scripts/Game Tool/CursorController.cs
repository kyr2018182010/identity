using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorController : MonoBehaviour
{
    public bool LockCursor = false;
    public KeyCode LockKey = KeyCode.CapsLock;
    void Awake()
    {
        LockGameCursor(LockCursor);
    }

    private void OnValidate()
    {
        LockGameCursor(LockCursor);
    }

    private void OnDestroy()
    {
        LockGameCursor(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(LockKey))
        {
            LockCursor = !LockCursor;
            LockGameCursor(LockCursor);
        }
    }

    static public void LockGameCursor(bool isLock)
    {
        if (isLock)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
