using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour
{
    private Transform player;
    private void OnEnable()
    {
        var players = GameObject.FindObjectsOfType<PlayerAction>();

        for(int i = 0; i < players.Length; ++i)
        {
            if (players[i].photonView.IsMine)
                player = players[i].transform;
        }
    }

    private void LateUpdate()
    {
        Vector3 newPos = player.position;
        newPos.y += 15;
        transform.position = newPos;

        transform.rotation = Quaternion.Euler(90f, player.eulerAngles.y, 0f);
    }
}
