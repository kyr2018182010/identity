using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using Protocol;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class SelectUI : MonoBehaviourPunCallbacks
{

    [SerializeField]
    private string nextScene;

    private bool selectComplete = false;
    private NetworkManager networkManager;

    private void Awake()
    {
        PhotonNetwork.IsMessageQueueRunning = true;
    }
    public override void OnEnable()
    {
        // 네트워크 이벤트 추가
        PhotonNetwork.NetworkingClient.EventReceived += EventReceived;

        //networkManager = GameObject.FindObjectOfType<NetworkManager>();
    }

    public override void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= EventReceived;
        PhotonNetwork.IsMessageQueueRunning = false;
    }

    public void SelectPlayerID0()
    {
        SetSkillID(0);
    }

    public void SelectPlayerID1()
    {
        SetSkillID(1);
    }

    private void SetSkillID(int skillId)
    {
        // skill id 설정
        GameObject.FindObjectOfType<NetworkManager>().playerSkillId = skillId;

        object[] content = new object[] { true };
        SendRaiseEvent(EVENTCODE.SKILL_SELECT_OK, content, SEND_OPTION.OTHER);

        if (selectComplete)
        {
            var selectPlayerId = this.transform.Find("SelectPlayerId");
            selectPlayerId.gameObject.SetActive(false);

            LoadingSceneManager.LoadScene(nextScene);
            return;
        }

        selectComplete = true;
    }

    public void SelectCompletedPlayerID()
    {
        object[] content = new object[] { true };
        SendRaiseEvent(EVENTCODE.SKILL_SELECT_OK, content, SEND_OPTION.OTHER);

        if (selectComplete)
        {
            var selectPlayerId = this.transform.Find("SelectPlayerId");
            selectPlayerId.gameObject.SetActive(false);

            PhotonNetwork.IsMessageQueueRunning = false;
            LoadingSceneManager.LoadScene(nextScene);
            return;
        }
        selectComplete = true;
    }

    public void SendRaiseEvent(EVENTCODE eventcode, object[] content, SEND_OPTION sendoption)
    {
        // debug
        string DebugStr = string.Empty;
        DebugStr = "[SEND__" + eventcode.ToString() + "]";
        for (int i = 0; i < content.Length; ++i)
        {
            DebugStr += "_" + content[i];
        }
        Debug.Log(DebugStr);

        RaiseEventOptions raiseEventOption = new RaiseEventOptions
        {
            Receivers = (ReceiverGroup)sendoption,
        };
        PhotonNetwork.RaiseEvent((byte)eventcode, content, raiseEventOption, SendOptions.SendReliable);
    }


    private void EventReceived(EventData photonEvent)
    {
        int code = photonEvent.Code;
        
        if (code == (int)EVENTCODE.SKILL_SELECT_OK)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("SKILL_SELECT_OK" + datas[0]);

            if(selectComplete)
            {
                var selectPlayerId = this.transform.Find("SelectPlayerId");
                selectPlayerId.gameObject.SetActive(false);

                PhotonNetwork.IsMessageQueueRunning = false;
                LoadingSceneManager.LoadScene(nextScene);
            }
            else
            {
                selectComplete = true;
            }
        }
    }
}
