using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using Protocol;
using ExitGames.Client.Photon;
using UnityEngine.EventSystems;
using Michsky.UI.ModernUIPack;

public class TitleUI : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private string nextScene;

    [SerializeField]
    private TMP_InputField idText;
    [SerializeField]
    private GameObject idButton;
    [SerializeField]
    private GameObject roomListItem;
    [SerializeField]
    private Transform roomListTransform;
    
    //private List<GameObject> Rooms = new List<GameObject>();
    [SerializeField]
    private GameObject[] buttons;
    [SerializeField]
    private ModalWindowManager WaitingMatching;
    [SerializeField]
    private ModalWindowManager WaitingAcceptedMatchUI;
    [SerializeField]
    private ModalWindowManager SuccessMatchUI;
    [SerializeField]
    private ModalWindowManager selectChapterUI;

    [SerializeField]
    private NotificationManager InviteNotification;
    [SerializeField]
    private NotificationManager acceptedMatchNotification;
    [SerializeField]
    private NotificationManager refusedMatchNotification;


    public GameObject choosedRoom;
    private Dictionary<string, RoomInfo> roomList = new Dictionary<string, RoomInfo>();
    private string currentRoomName;

    private void Awake()
    {
        //DontDestroyOnLoad(this.gameObject);

    }
    public override void OnEnable()
    {
        Debug.Log("room count: " + PhotonNetwork.CountOfRooms.ToString());
        Debug.Log("OnEnable Title UI");

        // 네트워크 이벤트 추가
        PhotonNetwork.NetworkingClient.EventReceived += EventReceived;
    }

    public override void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= EventReceived;
    }


    //public override void OnRoomListUpdate(List<RoomInfo> _roomList)
    //{
    //    // 룸 리스트 초기화 - 현재 생성된 룸들의 정보가 담긴 리스트가 매개변수로 온다. 
    //    // 로비 내에 룸이 생성되거나 사라질때 자동 호출되는 콜백 
    //    RoomUpdate(_roomList);
    //    Debug.Log("room update" + _roomList.Count.ToString());
    //}

    public void RoomUpdate(List<RoomInfo> _roomList)
    {
        for (int i = 0; i < _roomList.Count; i++)
        {
            RoomInfo info = _roomList[i];

            if (info.RemovedFromList)
            {
                Destroy(roomListTransform.Find(info.Name).gameObject);
                roomList.Remove(info.Name);
            }
            else
            {
                if (roomList.ContainsKey(info.Name))
                    continue;
                roomList[info.Name] = info;
                InstantiateNewRoom(info.Name);
            }
        }
    }

    public void CheckID()
    {
        for (int i = 0; i < buttons.Length; ++i)
            buttons[i].SetActive(true);

        PhotonNetwork.NickName = idText.text;

        idText.gameObject.SetActive(false);
        idButton.SetActive(false);
    }

    public void StartGame()
    {
        
    }

    public void ExitGame()
    {

    }

    public void EnterOption()
    {

    }

    //button On click func
    public void createRoom()
    {
        currentRoomName = "New Room" + PhotonNetwork.CountOfRooms.ToString();
        PhotonNetwork.CreateRoom(currentRoomName, new RoomOptions { MaxPlayers = 2, IsVisible = true, IsOpen = true });
    }

    public void CreatedRoom()
    {
        InstantiateNewRoom(currentRoomName);
        WaitingMatching.OpenWindow();
        Debug.Log("룸 생성 완료");
    }
    public void CreateRoomFailed()
    {
        currentRoomName = "";
        Debug.Log("룸 생성 실패");
    }

    private void roomListReset()
    {
        foreach (var room in roomList)
            Destroy(roomListTransform.Find(room.Key).gameObject);
        roomList.Clear();
        Debug.Log("프리팹 클리어");
    }
    public void outRoom()
    {
        roomListReset();
        PhotonNetwork.LeaveRoom();
    }

    public void joinRoom()
    {
        Debug.Log(choosedRoom.name);

        if (choosedRoom != null)
        {
            Debug.Log("choose Room " + choosedRoom.name);
            PhotonNetwork.JoinRoom(choosedRoom.name);
            WaitingAcceptedMatchUI.OpenWindow();
        }
        else
            Debug.Log("없엉");

        //object[] content = new object[] { choosePlayer.transform.Find("Text").GetComponent<TextMeshPro>().text };
        //SendRaiseEvent(EVENTCODE.INVITE_PLAYER, content, SEND_OPTION.OTHER);
    }

    public void PopUpMatchUI(string otherPlayerName)
    {
        SuccessMatchUI.titleText = "Match succeeded!";
        SuccessMatchUI.descriptionText = otherPlayerName + "  joined!";
        SuccessMatchUI.UpdateUI();
        SuccessMatchUI.OpenWindow();
    }

    public void AcceptMatch()
    {
        object[] content = new object[] { true };
        SendRaiseEvent(EVENTCODE.ACCEPT_MATCH, content, SEND_OPTION.OTHER);
        selectChapterUI.OpenWindow();
    }

    public void RefuseMatch()
    {
        PhotonNetwork.CurrentRoom.IsVisible = true;
        object[] content = new object[] { false };
        SendRaiseEvent(EVENTCODE.REFUSE_MATCH, content, SEND_OPTION.OTHER);
    }

    public void ContinueGame()
    {

    }

    public void NewGame()
    {
        //PhotonNetwork.LoadLevel("Terrain_JS");
        object[] content = new object[] { nextScene };
        SendRaiseEvent(EVENTCODE.SCENE_CHANGE, content, SEND_OPTION.OTHER);
        LoadingSceneManager.LoadScene(nextScene);
        //PhotonNetwork.IsMessageQueueRunning = false;
    }

    private void InstantiateNewRoom(string roomName)
    {
        var room = Instantiate<GameObject>(roomListItem, roomListTransform);
        room.name = roomName;
        room.GetComponent<ButtonManagerBasic>().buttonText = roomName;
    }

    public void SendRaiseEvent(EVENTCODE eventcode, object[] content, SEND_OPTION sendoption)
    {
        // debug
        string DebugStr = string.Empty;
        DebugStr = "[SEND__" + eventcode.ToString() + "]";
        for (int i = 0; i < content.Length; ++i)
        {
            DebugStr += "_" + content[i];
        }
        Debug.Log(DebugStr);

        RaiseEventOptions raiseEventOption = new RaiseEventOptions
        {
            Receivers = (ReceiverGroup)sendoption,
        };
        PhotonNetwork.RaiseEvent((byte)eventcode, content, raiseEventOption, SendOptions.SendReliable);
    }

    private void EventReceived(EventData photonEvent)
    {
        int code = photonEvent.Code;

        if (code == (int)EVENTCODE.ACCEPT_MATCH)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("RECV__EVENTCODE.INSTANCIATE_ENEMY__" + datas[0]);

            WaitingAcceptedMatchUI.CloseWindow();
            acceptedMatchNotification.OpenNotification();
        }
        else if (code == (int)EVENTCODE.REFUSE_MATCH)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("RECV__EVENTCODE.INSTANCIATE_ENEMY__" + datas[0]);

            WaitingAcceptedMatchUI.CloseWindow();
            refusedMatchNotification.OpenNotification();

            roomListReset();
            PhotonNetwork.LeaveRoom();
        }
        else if (code == (int)EVENTCODE.SCENE_CHANGE)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("RECV__EVENTCODE.INSTANCIATE_ENEMY__" + datas[0]);

            LoadingSceneManager.LoadScene((string)datas[0]);
            PhotonNetwork.IsMessageQueueRunning = false;
        }
    }
}
