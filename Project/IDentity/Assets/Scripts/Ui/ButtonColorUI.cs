using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using Protocol;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonColorUI : MonoBehaviourPunCallbacks, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [SerializeField] private Image uiImage;
    [SerializeField] private Image enterUiImage;

    [SerializeField] private Color enterColor = new Color(1f, 0.8f, 0.8f);
    [SerializeField] private Color otherPlayerEnterColor = new Color(0.7f, 0.7f, 1f);

    [SerializeField] private Color clickColor = new Color(1, 0, 0);
    [SerializeField] private Color otherPlayerClickColor = new Color(0, 0, 1);

    [SerializeField] private Color resetColor = new Color(1, 1, 1);

    public override void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += EventReceived;
    }

    public override void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= EventReceived;
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        if (uiImage.color == clickColor || uiImage.color == otherPlayerClickColor)
            return;

        object[] content = new object[] { this.gameObject.name };
        SendRaiseEvent(EVENTCODE.CURSOR_ON_UI, content, SEND_OPTION.OTHER);

        enterUiImage.color = enterColor;
    }
    
    public void OnPointerExit(PointerEventData eventData)
    {
        if (uiImage.color == clickColor || uiImage.color == otherPlayerClickColor)
            return;

        object[] content = new object[] { this.gameObject.name };
        SendRaiseEvent(EVENTCODE.CURSOR_EXIT_UI, content, SEND_OPTION.OTHER);

        enterUiImage.color = resetColor;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (uiImage.color == clickColor || uiImage.color == otherPlayerClickColor)
            return;

        object[] content = new object[] { this.gameObject.name };
        SendRaiseEvent(EVENTCODE.SELECTED_UI, content, SEND_OPTION.OTHER);

        uiImage.color = clickColor;
    }


    public void SendRaiseEvent(EVENTCODE eventcode, object[] content, SEND_OPTION sendoption)
    {
        // debug
        string DebugStr = string.Empty;
        DebugStr = "[SEND__" + eventcode.ToString() + "]";
        for (int i = 0; i < content.Length; ++i)
        {
            DebugStr += "_" + content[i];
        }
        Debug.Log(DebugStr);

        RaiseEventOptions raiseEventOption = new RaiseEventOptions
        {
            Receivers = (ReceiverGroup)sendoption,
        };
        PhotonNetwork.RaiseEvent((byte)eventcode, content, raiseEventOption, SendOptions.SendReliable);
    }


    private void EventReceived(EventData photonEvent)
    {
        int code = photonEvent.Code;
        if (code == (int)EVENTCODE.CURSOR_ON_UI)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("CURSOR_ON_UI - " + (string)datas[0]);

            if ((string)datas[0] == this.gameObject.name)
                uiImage.color = otherPlayerEnterColor;
        }
        else if (code == (int)EVENTCODE.CURSOR_EXIT_UI)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("CURSOR_EXIT_UI - " + (string)datas[0]);

            if ((string)datas[0] == this.gameObject.name)
                uiImage.color = resetColor;
        }
        else if (code == (int)EVENTCODE.SELECTED_UI)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("SELECTED_UI - " + (string)datas[0]);

            if ((string)datas[0] == this.gameObject.name)
                uiImage.color = otherPlayerClickColor;
        }
    }
}
