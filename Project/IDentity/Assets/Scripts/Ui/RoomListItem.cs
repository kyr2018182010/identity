using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomListItem : MonoBehaviour
{
    private TitleUI titleUI;

    private void OnEnable()
    {
        titleUI = GameObject.Find("titleUICanvas").GetComponent<TitleUI>();
    }

    public void sendRoomGameObject()
    {
        titleUI.choosedRoom = this.gameObject;
    }
}
