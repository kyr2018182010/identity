using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour
{
    [SerializeField]
    private Button settinButton;
    [SerializeField]
    private GameObject settingWindow;
    [SerializeField]
    private RectTransform chatWindowRect;
    public void OnSettingWindow()
    {
        if(settingWindow.activeSelf)
            settingWindow.SetActive(false);
        else
            settingWindow.SetActive(true);
    }

    public void chatWindowSize()
    {
        if(chatWindowRect.sizeDelta.y == 500)
            chatWindowRect.sizeDelta = new Vector2(chatWindowRect.sizeDelta.x, 65);
        else
            chatWindowRect.sizeDelta = new Vector2(chatWindowRect.sizeDelta.x, 500);
    }
}
