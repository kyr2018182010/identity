using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePoint : MonoBehaviour
{
    private SaveAndLoad saver;
    private bool save = false;

    private void Awake()
    {
        saver = FindObjectOfType<SaveAndLoad>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))// * change to PlayerAction 
        {
            if(!save)
                saver.Save(other.transform.position);
            save = true;
        }
    }

    public void Save(Vector3 spawnPoint)        // call by top
    {
        saver.Save(spawnPoint);
    }
}
