using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Subtegral.DialogueSystem.Runtime;

public class SaveAndLoad : MonoBehaviour
{
    private Inventory inventory;
    private TraumaManager traumaManager;
    [HideInInspector]public Vector3 currSavePoint;

    [SerializeField] private GameObject progressLoop;
    private GameObject loop;
    private TextMeshProUGUI text;
    float showTimer;
    const float showTime = 1f;
    
    private void Awake()
    {
        inventory = FindObjectOfType<Inventory>();
        traumaManager = FindObjectOfType<TraumaManager>();
        loop = progressLoop.GetComponentInChildren<Image>(true).gameObject;
        text = progressLoop.GetComponentInChildren<TextMeshProUGUI>(true);
        showTimer = showTime;
    }

    public void Save(Vector3 savePoint)
    {
        text.text = "Saving";
        progressLoop.SetActive(true);
        loop.SetActive(true);
        var request = Resources.LoadAsync<SaveData>("save");
        var data = request.asset as SaveData;
        data.collectedTraumaCount = traumaManager.GetCollectCount();
        data.inventoryDatas = inventory.GetSaveDatas();
        data.savePoint = savePoint;

        QuestParser questParser =  Camera.main.gameObject.GetComponent<QuestParser>();

        data.quest = questParser.quest;

        questParser.GetSaveQuest(out data.acceptedQuests, out data.canAccpetQuests);

        text.text = "Complete!";
        loop.SetActive(false);
        StartCoroutine(ShowCompleteUI());
    }

    public void Load()
    {
        Debug.Log("Data loading...");
        var saveData = Resources.Load<SaveData>("save");
        inventory.LoadInventory(saveData.inventoryDatas);
        traumaManager.LoadTrauma(saveData.collectedTraumaCount);
        currSavePoint = saveData.savePoint;
        var players = FindObjectsOfType<PlayerAction>();
        foreach (var player in players)
        {
            player.SetNewTransform(saveData.savePoint);
        }
        QuestParser questParser = Camera.main.gameObject.GetComponent<QuestParser>();

        questParser.LoadQuest(saveData.quest, saveData.acceptedQuests, saveData.canAccpetQuests);

        // * set gameObjects
    }

    private IEnumerator ShowCompleteUI()
    {
        while(showTimer > 0)
        {
            showTimer -= Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        progressLoop.SetActive(false);

        showTimer = showTime;
    }
}
