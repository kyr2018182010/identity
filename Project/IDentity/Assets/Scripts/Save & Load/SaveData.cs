using Subtegral.DialogueSystem.DataContainers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "save data",menuName = "Save & Load/create save data",order = 5)]
public class SaveData : ScriptableObject
{
    public Vector3 savePoint;
    public List<InventorySaveData> inventoryDatas;
    public int collectedTraumaCount;
    public QuestContainer quest;
    public List<QuestNodeData> acceptedQuests;
    public List<NodeLinkData> canAccpetQuests;
    public string currQuestGuid;
}
