using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using Protocol;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Tilemaps;
using WebSocketSharp;

public class QuestTile : MonoBehaviour
{
    [HideInInspector]public List<PlayerAction> onPlayers = new List<PlayerAction>();
    private FollowQuestNPCAI followNPC;

    //private OffMeshLink offMeshLink;

    private bool coroutine = false;
    [SerializeField]private Vector3 autoMoveVelocity = Vector3.zero;            // only use to auto tile moving

    // auto move
    [SerializeField] [Tooltip("플레이어의 중력 능력에 의해 움직이는 발판이 아닌경우 반드시 체크")] private bool autoMoving = false;
    [SerializeField] [Tooltip("Auto Moving이 true인 경우, 발판의 이동 경로를 설정")] private Transform[] tileTracks;
    int trackIdx = 0;
    int autoTileMoveDirection = 1;         // 1 or -1
    [Range(0f, 10f)] [SerializeField] [Tooltip("발판의 가속도 설정")] private float autoMovingAcc = 1f;
    private bool moving = false;
    [SerializeField] [Tooltip("어떤 트리거에 의해 움직이는 경우 해당 오브젝트 도킹")] private QuestObj moveStartTriggerButton;

    private List<Vector3> tracks = new List<Vector3>();

    private Coroutine autoMovingCoroutine;
    private Coroutine movePlayerCoroutine;
    private Coroutine checkPlayerCoroutine;

    // auto tile will use transform.Translate() not rigidbody
    private void Awake()
    {
        tracks = new List<Vector3>();
        //offMeshLink = GetComponentInChildren<OffMeshLink>();
        if (autoMoving)
        {
            if (tileTracks != null && tileTracks.Length == 0)     Debug.LogError("You have to set auto tile tracks");
            else
            {
                if (tileTracks != null)
                {
                    foreach (var t in tileTracks)
                    {
                        tracks.Add(t.position);
                    }
                }
            }
            if (!moveStartTriggerButton) {
                if (PhotonNetwork.CurrentRoom.masterClientId == PhotonNetwork.LocalPlayer.ActorNumber)
                    StartAutoMoving(); 
            }
        }
    }

    private void OnEnable()
    {
        if (!moveStartTriggerButton)
        {
            if (PhotonNetwork.CurrentRoom.masterClientId == PhotonNetwork.LocalPlayer.ActorNumber)
                StartAutoMoving();

        }
    }

    private void OnDisable()
    {
        if (autoMoving)
        {
            if(autoMovingCoroutine != null)
                StopCoroutine(autoMovingCoroutine);
            autoMovingCoroutine = null;
            moving = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.contacts[0].point.y <= transform.position.y)
            return;
        var player = collision.transform.GetComponentInParent<PlayerAction>();
        if (player != null)     // if collide with player, add that player to List
        {
            if (!FindPlayer(player.ID))
            {
                onPlayers.Add(player);
            }
            player.OnTile(true);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.contacts[0].point.y <= transform.position.y)
            return;
        var player = collision.transform.GetComponentInParent<PlayerAction>();
        if (player != null)     // if collide with player, add that player to List
        {
            if (!FindPlayer(player.ID))
            {
                onPlayers.Add(player);
            }
            player.OnTile(true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent<FollowQuestNPCAI>(out var npc) && CheckOnTile(npc.transform.position,out var hitPoint))
        {
            if (!followNPC && !npc.jumpingToFloor)     // To prevent duplicate coroutine calls
            {
                followNPC = npc;        // set Follow NPC
                followNPC.OnTile(this);       // change follow NPC state to OnTile
                StartCoroutine(FollowNPCMove());
                StartCoroutine(FollowNPCGetDownTile());
                //followNPC.transform.position = offMeshLink.endTransform.position;       // set feet position
                //offMeshLink.activated = false;
            }
        }
    }

    private void Update()
    {
        if(autoMoving && moveStartTriggerButton)
        {
            if (moveStartTriggerButton.isActive && !moving) StartAutoMoving();
            else if (!moveStartTriggerButton.isActive && moving)
            {
                if(autoMovingCoroutine != null)
                    StopCoroutine(autoMovingCoroutine);
                moving = false;
            }
        }
    }

    private void FixedUpdate()
    {
        if(onPlayers.Count > 0 && !coroutine)
        {
            coroutine = true;
            movePlayerCoroutine = StartCoroutine(MovePlayer());
            checkPlayerCoroutine = StartCoroutine(PlayerGetDownTile());
        }

        if (onPlayers.Count < 1)
            coroutine = false;
    }

    private bool IsArrivePoint(Vector3 point)
    {
        if(Vector3.Distance(point,transform.position) <= 0.3f)
        {
            return true;
        }
        return false;
    }

    IEnumerator AutoTileMoving()
    {
        while (true)
        {
            var u = (tracks[trackIdx] - transform.position).normalized;        // u vector
            autoMoveVelocity += (autoMovingAcc * Time.deltaTime) * u;
            transform.position += autoMoveVelocity * Time.deltaTime;

            if (IsArrivePoint(tracks[trackIdx]))   // check arrive track point
            {
                transform.position = tracks[trackIdx];
                if((autoTileMoveDirection == 1 && trackIdx >= tracks.Count - 1) ||     // tile arrive last point 
                    (autoTileMoveDirection == -1 && trackIdx <= 0))                     // tile arrive first point
                {
                    autoTileMoveDirection *= -1;           
                }
                trackIdx += autoTileMoveDirection;      // change next track point
                autoMoveVelocity = Vector3.zero;
                yield return new WaitForSeconds(3f);
            }

            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator MovePlayer()
    {
        while (true)
        {
            if (onPlayers.Count < 1)
            {
                if(movePlayerCoroutine!=null)
                    StopCoroutine(movePlayerCoroutine);
                break;
            }

            var vel = Vector3.zero;
            if (autoMoving) vel = autoMoveVelocity;
            else vel = GetComponent<GravityObject>().GetRigidbodyVelocity();

            foreach (var pl in onPlayers)
            {
                pl.externalVelocity = vel;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator PlayerGetDownTile()
    {
        while (true)
        {
            if (onPlayers.Count < 1)
            {
                if(checkPlayerCoroutine!= null)
                    StopCoroutine(checkPlayerCoroutine);
                break;
            }

            // create temp array
            var players = onPlayers.ToArray<PlayerAction>();

            foreach(var player in players)
            {
                if (!CheckOnTile(player.transform.position,out var hitPoint))       // if player get down tile
                {
                    player.OnTile(false);
                    onPlayers.Remove(player);
                }
            }

            yield return new WaitForFixedUpdate();
            
        }
    }

    IEnumerator FollowNPCGetDownTile()
    {
        while (true)
        {

            if (!CheckOnTile(followNPC.transform.position,out var hitPoint) || !followNPC)
            {
                followNPC = null;
                StopCoroutine(FollowNPCGetDownTile());
                //offMeshLink.activated = true;
            }
            yield return new WaitForSeconds(0.2f);
        }
    }

    IEnumerator FollowNPCMove()
    {
        while (true)
        {
            if (followNPC == null || followNPC.jumpingToFloor)
            {
                StopCoroutine(FollowNPCMove());
                break;
            }


            //followNPC.transform.position = offMeshLink.endTransform.position;
            yield return new WaitForFixedUpdate();
        }
    }

    private bool CheckOnTile(Vector3 feetPos,out Vector3 point)
    {
        if (Physics.Raycast(feetPos + Vector3.up * 0.1f,Vector3.down,out var hit,5f))
        {
            if(hit.transform.TryGetComponent<QuestTile>(out var tile))
            {
                point = hit.point;
                return true;
            }
        }
        point = new Vector3(-100000, -10000, -10000);
        return false;
    }

    public void AllGetDownTile()
    {
        if (onPlayers.Count > 0)
        {
            foreach (var pl in onPlayers)
            {
                pl.OnTile(false);
            }

            onPlayers.Clear();
        }
        if(TryGetComponent<Rigidbody>(out var rigidbody)){
            rigidbody.isKinematic = false;
        }

        if (followNPC)
        {

        }
    }

    public bool FindPlayer(int playerID)
    {
        foreach(var pl in onPlayers)
        {
            if (pl.ID == playerID)
                return true;
        }
        return false;
    }

    public void StartAutoMoving()
    {
        if (autoMoving && !moving) {
            if (autoMovingCoroutine == null)
                autoMovingCoroutine = StartCoroutine(AutoTileMoving());
        }
        moving = true;
    }
}
