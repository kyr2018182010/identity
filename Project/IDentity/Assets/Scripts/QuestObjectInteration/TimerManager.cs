using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
/*
 Manage timer UI and sound
 */

public class TimerManager : MonoBehaviour
{
    [SerializeField] [Tooltip("Timer를 표시하는 UI 오브젝트")] private GameObject timerTarget;
    static private GameObject timerObject;
    private TextMeshProUGUI timerText;
    static private SoundManager sound;
    static private bool onTimer = false;

    static private float timer;

    private void Start()
    {
        timerObject = timerTarget;

        if (timerObject != null)
        {
            timerText = timerTarget.GetComponentInChildren<TextMeshProUGUI>(true);
            if (!timerText)
                Debug.LogError("Timer Manager : no text");
        }

        timerObject.SetActive(false);
        sound = FindObjectOfType<SoundManager>();
    }

    private void Update()
    {
        if (!onTimer) return;
        if (timer > 0)
        {
            timer -= Time.deltaTime;                            // update timer value
            if (timerText) timerText.text = ((int)timer).ToString();           // update timer Text
        }
        else if (timerObject.activeSelf && timer <= 0)
        {
            OffTimer();
        }
    }

    static public void OnTimer(float time)
    {
        if (onTimer) return;
        timer = time;
        timerObject.SetActive(true);
        sound.ChangeBGM(BGMEnum.Timer);
        onTimer = true;
    }

    static public void OffTimer()
    {
        timerObject.SetActive(false);
        sound.ChangeBGM2Origin();
        onTimer = false;
    }

    static public float getTime()
    {
        return timer;
    }

    public void Stop(bool stop)
    {
        if (stop)
            Time.timeScale = 0f;
        else
            Time.timeScale = 1f;
    }
}

