using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestDoor : MonoBehaviour
{
    [SerializeField]
    private List<QuestObj> questObj = new List<QuestObj>();
    [SerializeField]
    private bool mustAllObjAvtice = false;

    private Vector3 yOffset = new Vector3(0, 0.1f, 0);
    private float openAmount = 3f;
    private float changedY;
    private float originPositionY;

    private void Start()
    {
        originPositionY = transform.position.y;
        changedY = originPositionY - openAmount;
    }
    public void IsButtonOk()
    {
        if (mustAllObjAvtice)
        {
            for (int i = 0; i < questObj.Count; ++i)
                if (questObj[i].isActive == false)
                    return;
        }

        StartCoroutine("OpenDoorCoroutine");
    }

    public void CloseDoor()
    {
        if (!mustAllObjAvtice)
        {
            for (int i = 0; i < questObj.Count; ++i)
                if (questObj[i].isActive == true)
                    return;
        }

        StartCoroutine("CloseDoorCoroutine");
    }

    private IEnumerator OpenDoorCoroutine()
    {
        while (this.transform.position.y > changedY)
        {
            this.transform.position -= yOffset;
            yield return new WaitForSeconds(0.001f);
        }
    }

    private IEnumerator CloseDoorCoroutine()
    {
        while (this.transform.position.y < originPositionY)
        {
            this.transform.position += yOffset;
            yield return new WaitForSeconds(0.001f);
        }
    }
}
