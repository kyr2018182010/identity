using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestStone : MonoBehaviour
{
    private Vector3 position;

    private void OnEnable()
    {
        position = this.transform.position;
        Invoke("ResetQuestStone", 30);
    }
    private void OnDisable()
    {
        CancelInvoke("ResetQuestStone");
    }

    private void ResetQuestStone()
    {
        this.gameObject.SetActive(false);
        this.transform.position = position;
    }
}
