using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

[System.Serializable]
public struct TeleportationData
{
    public PlayerAction target;
    public float timer;
}

public class Teleportation : MonoBehaviour
{
    [SerializeField] Transform destination;
    [SerializeField] float watingTime = 5f;

    [SerializeField] private TeleportationData[] targets = new TeleportationData[2];
    [SerializeField] private ParticleSystem vfx;
    private void Start()
    {
        if (TryGetComponent<Collider>(out var coll))
        {
            coll.isTrigger = true;
        }
        else Debug.LogError(gameObject.name + ": Add Collider");


    }

    private void OnEnable()
    {
        vfx.Play();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<PlayerAction>(out var player))
        {
            if (!targets[player.ID].target)
            {
                var targetData = new TeleportationData();
                targetData.target = player;
                targetData.timer = watingTime;
                targets[player.ID] = targetData;
            }
            StartCoroutine(CheckTeleportation(player.ID));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<PlayerAction>(out var player))
        {
            if (targets[player.ID].target)
            {
                StopCoroutine(CheckTeleportation(player.ID));
                targets[player.ID].target = null;
            }
        }
    }

    IEnumerator CheckTeleportation(int idx)
    {
        GetComponent<EffectSoundPlayer>().PlayEffectSound();
        while (targets[idx].target)
        {
            if (targets[idx].timer < 0)
            {
                targets[idx].target.SetNewTransform(destination.position);
                targets[idx].target = null;
                break;
            }
            targets[idx].timer -= Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
    }
}
