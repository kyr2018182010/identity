using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestTower : MonoBehaviour
{
    [SerializeField]
    private GameObject NextTower;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.ToString() == "Player")
        {
            if(TryGetComponent<TraumaPusher>(out var pusehr))
                StartCoroutine(PushTrauma());
            if(TryGetComponent<SavePoint>(out var save))
                StartCoroutine(Save(other.transform.position));
            if (NextTower!= null)
            {
                var nextchildren = NextTower.transform.GetComponentsInChildren<Transform>(true);
                for (int i = 0; i < nextchildren.Length; ++i)
                {
                    nextchildren[i].gameObject.SetActive(true);
                }
            }

            var thisChildren = this.transform.GetComponentsInChildren<Transform>();
            for (int i = 0; i < thisChildren.Length; ++i)
            {
                if (thisChildren[i].name != this.name)
                    thisChildren[i].gameObject.SetActive(false);
            }
        }
    }

    IEnumerator PushTrauma()
    {
        GetComponent<TraumaPusher>().Push();
        yield return new WaitForSeconds(5f);
    }

    IEnumerator Save(Vector3 position)
    {
        GetComponent<SavePoint>().Save(position);
        yield return new WaitForSeconds(10f);
    }
}
