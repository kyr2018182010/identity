using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchLight : MonoBehaviour
{
    [SerializeField] private List<Transform> tracks = new List<Transform>();
    [SerializeField] private float moveAcc;
    [SerializeField] private float rotatingAcc;
    [SerializeField] private float stopTime;
    [SerializeField] float damping = 3f;
    [SerializeField] float maxAngle = 20f;

    private float rotationSync;
    private float rotationAngle;
    private float rotationDir = 1;
    private float workTimer;
    private Transform rotationTarget;
    private int dir = 1;
    private int currIdx = 0;

    private void OnEnable()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;

        if(!rotationTarget)
        {
            rotationTarget = transform.GetChild(0);
        }
        if (Random.Range(1, 2) % 2 == 1) dir *= -1;

        rotationSync = Random.Range(-2f, 2f) * dir * -1;

        stopTime *= Random.Range(0.8f, 5f);
        workTimer = stopTime;
        StartCoroutine(Work());
    }

    private void OnDisable()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;
        StopCoroutine(Work());
    }

    public void Move()
    {
        // turn
        var next = tracks[currIdx].position;
        next.y = 0;
        var curr = transform.position;
        curr.y = 0;
        var u = (next - curr).normalized;
        Quaternion rot = Quaternion.LookRotation(u);
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * damping);

        // move
        transform.Translate(Vector3.forward * moveAcc * Time.deltaTime);

        // check next
        if(Vector3.Distance(next,curr) <= 0.2f)
        {
            currIdx = (currIdx + 1) % tracks.Count;
        }
    }

    private void Rotate()
    {
        transform.rotation = Quaternion.AngleAxis(rotationAngle, Vector3.forward);
        rotationAngle += rotatingAcc * Time.deltaTime * rotationSync * rotationDir;
        if(rotationAngle <= -maxAngle || rotationAngle >= maxAngle)
        {
            rotationDir *= -1;
        }
    }

    IEnumerator Work()
    {
        while (true)
        {
            Move();
            Rotate();
            workTimer -= Time.deltaTime;
            if (workTimer <= 0)
            {
                workTimer = stopTime;
                yield return new WaitForSeconds(3f);
            }
            yield return new WaitForFixedUpdate();
        }
    }

    public void OnCollisionInChild(Collider other)
    {
        if(other.TryGetComponent<PlayerAction>(out var player))
        {
            var u = (player.transform.position- transform.position).normalized;
            var dist = Vector3.Distance(transform.position, player.transform.position);
            Debug.DrawRay(transform.position, u * dist);
            if (Physics.BoxCast(transform.position, transform.lossyScale/2,u,out var hit,Quaternion.identity,dist))
            {
                if (!hit.transform.TryGetComponent<PlayerAction>(out var p)) return;
            }
            GameStateManager.GameOver();

        }
    }
}
