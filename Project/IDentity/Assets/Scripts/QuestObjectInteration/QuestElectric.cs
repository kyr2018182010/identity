using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using Protocol;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class QuestElectric : QuestObj
{
    [SerializeField]
    private List<QuestDoor> questDoors = new List<QuestDoor>();
    [SerializeField]
    private float time = 240f;
    [SerializeField]
    private VisualEffect effect;
    [SerializeField]
    private float turnOnTime = 3f;
    [SerializeField]
    private GameObject lineEffect;

    private float lessTurnOnTime;
    private float timeOffset;
    private float lessTime = 240f;
    private float effectSize;
    private float defaultEffectSize;

    public override void OnEnable()
    {
        // 네트워크 이벤트 추가
        PhotonNetwork.NetworkingClient.EventReceived += EventReceived;
    }

    public override void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += EventReceived;
    }

    private void Start()
    {
        defaultEffectSize = effect.GetFloat("time");
        lessTime = time;       
        lessTurnOnTime = turnOnTime;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.root.TryGetComponent<PlayerAction>(out var player))
        {
            TurnOnTimer();
        }
    }

    [PunRPC]
    private void TurnOnTimer()
    {
        float t = Time.deltaTime;
        lessTurnOnTime -= t;
        //Debug.Log(lessTurnOnTime);

        effectSize = effect.GetFloat("time");
        timeOffset = effectSize / (lessTurnOnTime / t);
        effect.SetFloat("time", effectSize -= timeOffset);
        GetComponent<EffectSoundPlayer>().PlayEffectSound();

        if (lessTurnOnTime < 0)
        {
            object[] content = new object[] { photonView.ViewID };
            SendRaiseEvent(EVENTCODE.QUEST_ELECTRIC_TURN_ON, content, SEND_OPTION.OTHER);

            gameObject.GetComponent<BoxCollider>().enabled = false;
            lineEffect.SetActive(true);
            StartCoroutine("Timer");
            isActive = true;
            for (int i = 0; i < questDoors.Count; ++i)
                questDoors[i].IsButtonOk();
        }
    }

    private IEnumerator Timer()
    {
        while (lessTime > 0)
        {
            float t = Time.deltaTime;
            lessTime -= t;
            yield return new WaitForSeconds(t);
        }
        effectSize = defaultEffectSize;
        lessTime = time;
        lessTurnOnTime = turnOnTime;
        isActive = false;
        for (int i = 0; i < questDoors.Count; ++i)
            questDoors[i].CloseDoor();

        effect.SetFloat("time", effectSize);
        gameObject.GetComponent<BoxCollider>().enabled = true;
        lineEffect.SetActive(false);
    }


    public void SendRaiseEvent(EVENTCODE eventcode, object[] content, SEND_OPTION sendoption)
    {
        // debug
        string DebugStr = string.Empty;
        DebugStr = "[SEND__" + eventcode.ToString() + "]";
        for (int i = 0; i < content.Length; ++i)
        {
            DebugStr += "_" + content[i];
        }
        Debug.Log(DebugStr);

        RaiseEventOptions raiseEventOption = new RaiseEventOptions
        {
            Receivers = (ReceiverGroup)sendoption,
        };
        PhotonNetwork.RaiseEvent((byte)eventcode, content, raiseEventOption, SendOptions.SendReliable);
    }

    private void EventReceived(EventData photonEvent)
    {
        int code = photonEvent.Code;

        if (code == (int)EVENTCODE.QUEST_ELECTRIC_TURN_ON)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("RECV__EVENTCODE.INSTANCIATE_ENEMY__" + (int)datas[0]);
            Debug.Log((int)datas[0]);
            Debug.Log("photon view ID - " + photonView.ViewID);

            if ((int)datas[0] == photonView.ViewID)
            {
                gameObject.GetComponent<BoxCollider>().enabled = false;
                lineEffect.SetActive(true);
                StartCoroutine("Timer");
                isActive = true;
                for (int i = 0; i < questDoors.Count; ++i)
                    questDoors[i].IsButtonOk();
            }
        }
    }
}

