using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
   - Object that kills the player
*/

[System.Serializable]
public enum DeadAction
{
    Fall,
    None
}

public class DeadObject : MonoBehaviour
{
    [SerializeField] DeadAction deadAction;
    [SerializeField] [Tooltip("Dead Action이 Fall일 경우에 설정")] private float fallAcc = 0f;
    [SerializeField] [Tooltip("Dead Trigger가 되는 오브젝트")] private QuestObj triggerObject;

    private Rigidbody rigidbody;
    private Transform resetTransform;

    private bool kill = false;
    void Start()
    {
        resetTransform = transform;

        if (deadAction == DeadAction.Fall)
        {
            // set RigidBody
            if (TryGetComponent<Rigidbody>(out var obj)) rigidbody = obj;
            else rigidbody = gameObject.AddComponent<Rigidbody>();

            rigidbody.useGravity = true;
            rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            rigidbody.isKinematic = true;             // this will change to false when Call DeadAction()
        }

        kill = false;
    }

    private void Update()
    {
        if (triggerObject.isActive && !kill)
        {
            DeadActionStart();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<PlayerAction>(out var player))        // kill player
        {
            kill = true;
            GameStateManager.GameOver();
            gameObject.layer = 3;
            if (deadAction == DeadAction.Fall)
                GetComponent<Collider>().isTrigger = false;
        }



        if (other.CompareTag("Floor"))      // Fall Object on Ground
        {
            if (deadAction == DeadAction.Fall)
            {
                rigidbody.velocity = Vector3.zero;
                rigidbody.isKinematic = true;
                StopCoroutine(Fall());
            }
        }

    }


    private void Reset()
    {
        StopAllCoroutines();
        kill = false;
        rigidbody.isKinematic = true;
        rigidbody.velocity = Vector3.zero;

        transform.SetPositionAndRotation(resetTransform.position, resetTransform.rotation);     // reset transform

        GetComponent<Collider>().isTrigger = true;
        gameObject.layer = 0;           // set layer to Default
    }

    IEnumerator Fall()
    {
        while (!kill)
        {
            rigidbody.velocity += Vector3.down * fallAcc * Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
    }

    public void DeadActionStart()
    {
        if (deadAction == DeadAction.Fall)
        {
            StartCoroutine(Fall());
            rigidbody.isKinematic = false;
        }
    }
}
