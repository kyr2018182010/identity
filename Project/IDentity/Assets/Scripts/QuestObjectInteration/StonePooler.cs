using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StonePooler : MonoBehaviour
{
    [SerializeField]
    private GameObject stone;

    private List<GameObject> stonePool = new List<GameObject>();
    private int currIndex = 0;
    private int initCount = 20;
    private Coroutine stoneFallCoroutine;
    // Start is called before the first frame update

    private void Awake()
    {
        Vector3 boundSize = this.GetComponent<Collider>().bounds.size;
        Vector3 boundExtents = this.GetComponent<Collider>().bounds.extents;
        for (int i = 0; i < initCount; i++)
        {
            Vector3 randomPosition = this.transform.position - boundExtents + (Random.value + 0.001f) * boundSize;
            stonePool.Add(Instantiate<GameObject>(stone, randomPosition, Quaternion.identity, this.transform));
            stonePool[i].SetActive(false);
        }
    }

    private void OnEnable()
    {
        stoneFallCoroutine = StartCoroutine("stoneFall");
    }

    private void OnDisable()
    {
        StopCoroutine(stoneFallCoroutine);
        for (int i = 0; i < initCount; ++i)
            stonePool[i].SetActive(false);
    }

    IEnumerator stoneFall()
    {
        while (true)
        {
            if (currIndex == initCount)
                currIndex = 0;
            stonePool[currIndex++].SetActive(true);
            yield return new WaitForSeconds(3f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.ToString() == "Player")
        {
            StopCoroutine(stoneFallCoroutine);
            for (int i = 0; i < initCount; ++i)
                stonePool[i].SetActive(false);
        }
    }
}
