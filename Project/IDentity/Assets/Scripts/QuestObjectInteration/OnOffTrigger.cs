using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
- Used to activate/deactivate an object.
- Add to the trigger object.
- Must be used with objects that are QuestObj.
 */

public class OnOffTrigger : MonoBehaviour
{
    [SerializeField] [Tooltip("활성화/비활성화 할 오브젝트를 연결시켜줍니다.")] private GameObject onOffTarget;
    [SerializeField] [Tooltip("trigger가 활성화 되었을 때 target의 상태")] private bool active = true;
    [SerializeField] [Tooltip("일정 시간동안만 오브젝트를 활성화 시킨다면 true로 설정해주세요.")] private bool timerQuest;
    [SerializeField] [Tooltip("Timer Quest가 true일 경우, 활성화 시킬 시간을 설정해주세요.")] private float time;
    private QuestObj triggerObj;

    private void Start()
    {
        triggerObj = GetComponentInChildren<QuestObj>();
        onOffTarget.SetActive(!active);
        if (!triggerObj) Debug.LogError(gameObject.name + ": You have to add QuestObj conponent");
    }

    private void Update()
    {
        if (triggerObj == null || onOffTarget == null) return;
        if (triggerObj.isActive == !onOffTarget.activeSelf && active) 
            OnOffTarget(triggerObj.isActive);
        else if (!active && triggerObj.isActive == onOffTarget.activeSelf)
            OnOffTarget(!triggerObj.isActive);
    }

    public void OnOffTarget(bool on)
    {
        onOffTarget.SetActive(on);
    }
}
