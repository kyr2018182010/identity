using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestButton : QuestObj
{
    private Vector3 yOffset = new Vector3(0, 0.005f, 0);
    private float pushAmount = 0.5f;
    private float changedY;

    [SerializeField]
    private List<QuestDoor> questDoors = new List<QuestDoor>();

    [SerializeField]
    private GravityLevel gravityLevel = GravityLevel.Light;
    Coroutine soundCoroutine;

    private void Start()
    {
        changedY = transform.position.y - pushAmount;
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (!photonView.IsMine)
            return;

        // 플레이어일 때
        if (collision.transform.root.TryGetComponent<PlayerAction>(out var player))
        {
            if (player.GravityLevel <= gravityLevel)
            {
                photonView.RPC("Push", RpcTarget.Others);
                Push();
            }
            return;
        }
        // Gravity Object 일 때
        if (collision.gameObject.TryGetComponent<GravityObject>(out var gravity))
        {
            if (gravity.Level < (int)gravityLevel)
            {
                photonView.RPC("Push", RpcTarget.Others);
                Push();
            }
        }
    }

    [PunRPC]
    private void Push()
    {
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        if (soundCoroutine == null)
            soundCoroutine = StartCoroutine(PlaySound());
        StartCoroutine("PushButton");
        isActive = true;
        if (questDoors.Count > 0)
        {
            for (int i = 0; i < questDoors.Count; ++i)
                questDoors[i].IsButtonOk();
        }
    }

    private IEnumerator PushButton()
    {
        while (this.transform.position.y > changedY)
        {
            this.transform.position -= yOffset;
            yield return new WaitForSeconds(0.001f);
        }
        gameObject.GetComponent<Rigidbody>().isKinematic = false;
    }

    private IEnumerator PlaySound()
    {
        GetComponent<EffectSoundPlayer>().PlayEffectSound();
        yield return new WaitForSeconds(5f);
        soundCoroutine = null;
    }

    public void Retry()
    {
        GetComponent<Rigidbody>().isKinematic = false;
        isActive = false;
        transform.position += Vector3.up * pushAmount;
    }
}
