using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectCollision : MonoBehaviour
{
    private SearchLight parent;

    private void Start()
    {
        parent = GetComponentInParent<SearchLight>();
    }

    private void OnTriggerStay(Collider other)
    {
        parent.OnCollisionInChild(other);
    }
}
