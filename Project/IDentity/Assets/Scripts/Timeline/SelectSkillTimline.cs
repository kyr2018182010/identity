using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class SelectSkillTimline : MonoBehaviour
{
    [SerializeField]
    private PlayableDirector openingPlayableDirector;
    [SerializeField]
    private PlayableDirector selectSkillPlayableDirector;
    [SerializeField]
    private TimelineAsset openingTimeline;
    [SerializeField]
    private TimelineAsset selectSkillTimeline;

    [SerializeField]
    private GameObject selectSkillUI;

    void Awake()
    {
        openingPlayableDirector.Play(openingTimeline);
        selectSkillUI.SetActive(false);
    }

    void OnEnable()
    {
        openingPlayableDirector.stopped += OnPlayableDirectorStopped;
        selectSkillPlayableDirector.stopped += OnPlayableDirectorStopped;
    }

    void OnPlayableDirectorStopped(PlayableDirector aDirector)
    {
        Debug.Log("PlayableDirector named " + aDirector.name + " is now stopped.");
        if (openingPlayableDirector == aDirector)
        {
            selectSkillPlayableDirector.extrapolationMode = DirectorWrapMode.Loop;
            selectSkillPlayableDirector.Play(selectSkillTimeline);
            selectSkillUI.SetActive(true);
        }
    }

    void OnDisable()
    {
        openingPlayableDirector.stopped -= OnPlayableDirectorStopped;
        selectSkillPlayableDirector.stopped -= OnPlayableDirectorStopped;
    }

    public void skip()
    {
        openingPlayableDirector.Stop();
    }
}
