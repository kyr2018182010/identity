using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class EndingTimeline : MonoBehaviour
{
    [SerializeField]
    private PlayableDirector endingPlayableDirector;
    [SerializeField]
    private TimelineAsset endingTimeline;

    private void OnEnable()
    {
        endingPlayableDirector.stopped += OnPlayableDirectorStopped;
    }

    void OnPlayableDirectorStopped(PlayableDirector aDirector)
    {
        if (endingPlayableDirector == aDirector)
        {
            Destroy(FindObjectOfType<NetworkManager>().gameObject);
            LoadingSceneManager.LoadScene("TitleScene");
        }
    }
}
