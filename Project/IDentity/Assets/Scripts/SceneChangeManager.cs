using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangeManager : MonoBehaviour
{
    private NetworkManager networkManager;

    [SerializeField]
    private string loadScene = "ServerTest";

    [SerializeField]
    private List<GameObject> setObjAfterServerSetting = new List<GameObject>();

    private void Awake()
    {
        //PhotonNetwork.IsMessageQueueRunning = true;
    }
    void OnEnable()
    {
        Debug.Log("SceneChangeManager");
        PhotonNetwork.IsMessageQueueRunning = true;
        CheckSceneLoaded();
    }

    // 체인을 걸어서 이 함수는 매 씬마다 호출된다.
    void CheckSceneLoaded()
    {
        Debug.Log("OnSceneLoaded: " + SceneManager.GetActiveScene().name);
        if(SceneManager.GetActiveScene().name == loadScene)
        {
            networkManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
            networkManager.LoadPlayer();

            for(int i =0;i< setObjAfterServerSetting.Count; ++i)
            {
                setObjAfterServerSetting[i].SetActive(true);
            }
            StartCoroutine("CheckPlayerinit");
        }
    }

    private IEnumerator CheckPlayerinit()
    {
        Debug.Log("CheckPlayerinit");

        while (true)
        {
            var players = PhotonView.FindObjectsOfType<PlayerAction>();
            Debug.Log("players.Length - " + players.Length.ToString());
            if (players.Length < 2)
                yield return new WaitForSeconds(0.3f);
            else
            {
                for (int i = 0; i < players.Length; ++i)
                {
                    Debug.Log("player" + i.ToString());
                    players[i].gameObject.name = "player" + i.ToString();
                }
                Debug.Log("SceneChangeManager - strory manager init");
                Camera.main.GetComponent<StoryManager>().StoryManagerInit();
                break;
            }
        }
    }
}
