using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using Protocol;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraumaDestroyer : MonoBehaviourPunCallbacks
{
    BossQuest bossQuest;

    private void Awake()
    {
        bossQuest = FindObjectOfType<BossQuest>();
    }

    public override void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += EventReceived;
    }

    public override void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= EventReceived;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (!bossQuest.start) return;

        if(other.TryGetComponent<Trauma>(out var trauma))
        {
            if (trauma.isActiveAndEnabled)
            {
                object[] content = new object[] { trauma.id, trauma.GetComponent<PhotonView>().ViewID };
                SendRaiseEvent(EVENTCODE.REMOVE_TRAUMA, content, SEND_OPTION.OTHER);

                trauma.gameObject.SetActive(false);
                bossQuest.DestroyTrauma(trauma.id);
                GetComponent<EffectSoundPlayer>().PlayEffectSound();
                GetComponent<EffectSoundPlayer>().PlayEffectSound(BGMEnum.BossHurt, false, 0f);
            }
        }
    }

    public void SendRaiseEvent(EVENTCODE eventcode, object[] content, SEND_OPTION sendoption)
    {
        // debug
        string DebugStr = string.Empty;
        DebugStr = "[SEND__" + eventcode.ToString() + "]";
        for (int i = 0; i < content.Length; ++i)
        {
            DebugStr += "_" + content[i];
        }
        Debug.Log(DebugStr);

        RaiseEventOptions raiseEventOption = new RaiseEventOptions
        {
            Receivers = (ReceiverGroup)sendoption,
        };
        PhotonNetwork.RaiseEvent((byte)eventcode, content, raiseEventOption, SendOptions.SendReliable);
    }

    private void EventReceived(EventData photonEvent)
    {
        int code = photonEvent.Code;

        if (code == (int)EVENTCODE.REMOVE_TRAUMA)
        {
            object[] datas = (object[])photonEvent.CustomData;
            Debug.Log("REMOVE_TRAUMA - " + datas[0]);

            PhotonView.Find((int)datas[1]).gameObject.SetActive(false);
            bossQuest.DestroyTrauma((int)datas[0]);
            bossQuest.CheckEnd();
        }
    }
}
