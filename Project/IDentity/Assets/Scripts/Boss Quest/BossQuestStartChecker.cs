using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossQuestStartChecker : MonoBehaviour
{
    private TraumaManager traumaManager;
    private BossQuest bossQuest;
 
    private void Awake()
    {
        traumaManager = FindObjectOfType<TraumaManager>();
        bossQuest = FindObjectOfType<BossQuest>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<PlayerAction>(out var player))       
        {
            traumaManager.PopAll();
            bossQuest.StartQuest();
        }
    }
}
