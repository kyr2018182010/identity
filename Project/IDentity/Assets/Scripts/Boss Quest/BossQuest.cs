using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BossQuest : MonoBehaviour
{
    private List<int> destroyedTraumas = new List<int>();
    [SerializeField] private List<Trauma> traumas = new List<Trauma>();
    private BossAI boss;
    [SerializeField] private GameObject bossStartPanel;
    [SerializeField] private InventoryUI inventoryUI;
    [SerializeField] private SoundManager sound;

    private Coroutine trackingBossAttackCount;

    public bool start = false;

    private void Awake()
    {
        int id = 0;
        traumas.ForEach((Trauma t) =>
        {
            t.gameObject.SetActive(false);
            t.id = ++id;
        });
        boss = FindObjectOfType<BossAI>();
        sound = FindObjectOfType<SoundManager>();
    }

    private void Start()
    {
        bossStartPanel.SetActive(false);
    }

    public void StartQuest()
    {
        if (start) return;
        traumas.ForEach((Trauma t) => t.gameObject.SetActive(true));
        boss.QuestStart();
        Debug.Log("start quest");
        start = true;
        //bossStartPanel.SetActive(true);
        //inventoryUI.OpenPanelWithEffect(bossStartPanel.name, Color.black);
        //StartCoroutine(ClosePanel());
        boss.Roar(true, 0f);
        sound.ChangeBGM(BGMEnum.Boss);
        trackingBossAttackCount = StartCoroutine(TrackingBossAttackCount());
    }

    public void DestroyTrauma(int id)
    {
        if (destroyedTraumas.Contains(id)) return;
        destroyedTraumas.Add(id);

        CheckEnd();
    }

    public void CheckEnd()
    {
        Debug.Log(destroyedTraumas.Count);
        if ((traumas.FindAll((Trauma a) => !a.gameObject.activeSelf)).Count >= 4)
            EndQuest();
        //if (destroyedTraumas.Count >= traumas.Count)
        //{
        //    EndQuest();
        //}
    }

    private void EndQuest()
    {
        boss.Dead();
        StopCoroutine(trackingBossAttackCount);
        //FindObjectOfType<GameEndPoint>().end = true;
        //start = false;
        StartCoroutine(WaitingBossAnimationEnd());
    }

    IEnumerator WaitingBossAnimationEnd()
    {
        yield return new WaitForSeconds(3f);
        FindObjectOfType<GameStateManager>().GameEnd();
    }

    private IEnumerator TrackingBossAttackCount()
    {
        while (boss.attackCount < 5)
        {
            yield return new WaitForSeconds(0.5f);
        }

        // GameOver & Reset
        boss.Dead();
        GameStateManager.GameOver();
    }

    public void Retry()
    {
        if (start)
        {
            boss.Retry();
            traumas.ForEach((Trauma trauma) => trauma.Retry());
        }
        start = false;
    }

    IEnumerator ClosePanel()
    {
        yield return new WaitForSeconds(1.5f);
        inventoryUI.Close(bossStartPanel.name);
        sound.ChangeBGM(BGMEnum.Boss);
        StopCoroutine(ClosePanel());
    }
}

