using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trauma : MonoBehaviour
{
    [SerializeField] private float rotSpeed = 15f;
    public int id;
    private Transform resetTr;
    private Transform rotTarget;

    private void Awake()
    {
        resetTr = transform;
        rotTarget = GetComponentInChildren<Renderer>().transform;
    }

    void FixedUpdate()
    {
        rotTarget.Rotate(Vector3.up * rotSpeed * Time.deltaTime);   
    }

    public void Retry()
    {
        transform.position = resetTr.position;
        transform.rotation = resetTr.rotation;
    }
}
